package com.vn.akidokids.ui.main.Category.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.DataArticleCategory
import com.vn.akidokids.util.FORMAT_DATE_APEARANCE
import com.vn.akidokids.util.FORMAT_DATE_API
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.loadImage
import com.vn.akidokids.util.extension.toDateString
import kotlinx.android.synthetic.main.item_article.view.*

class ArticleAdapter(private var listArticle:MutableList<DataArticleCategory>, private val delegate: ArticleDelegate):RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val VIEW_TYPE_ITEM = 0
    val VIEW_TYPE_LOADING = 1

    var isLoading:Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
           return ArticleViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_article, parent, false))
        }

        return ArticleViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_loading, parent, false))

    }

    override fun getItemViewType(position: Int): Int {
        if ( this.listArticle.count() > position)
         return VIEW_TYPE_ITEM

        return VIEW_TYPE_LOADING
    }

    override fun getItemCount(): Int {
        if (this.isLoading)
            return listArticle.count() + 1

        return listArticle.count()
    }

    internal fun addCatergoryToList(articles: List<DataArticleCategory>, isLoading:Boolean, isRefresh:Boolean) {
        this.isLoading = isLoading
        if (isRefresh) {
            this.listArticle.clear()
        }
        this.listArticle.addAll(articles)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (this.getItemViewType(position)) {
            VIEW_TYPE_ITEM -> {
                val holder = holder as? ArticleAdapter.ArticleViewHolder
                holder?.let {
                    it.clear()
                    it.onBind(position)
                    it.itemClick(position = position)
                }
            }

            VIEW_TYPE_LOADING -> {
                val menuHolder = holder as? LoadingViewHolder
            }

            else -> return
        }
    }


    inner class ArticleViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun itemClick(position: Int) {
            itemView.setOnClickListener {
                if (position >= listArticle.count()) {
                    delegate.clickArticle(0)
                    return@setOnClickListener
                }

                delegate.clickArticle(listArticle[position]?.id ?: 0)
            }
        }

        fun clear() {
            itemView.imgThumbnail.setImageDrawable(null)
            itemView.lblTitle.text = ""
            itemView.lblDescription.text = ""
            itemView.lblDate.text = ""
            itemView.lbCategory.text = ""
        }

        fun onBind(position: Int) {
            listArticle[position].title?.let {
                itemView.lblTitle.text = it
            }

            listArticle[position].description?.let {
                itemView.lblDescription.text = it
            }

            listArticle[position].createdDate?.let {
                val date = it.convertStringToDate(FORMAT_DATE_API)
                itemView.lblDate.text = date?.toDateString(FORMAT_DATE_APEARANCE)
            }

            listArticle[position].img?.let {
                itemView.imgThumbnail.loadImage(it)
            }

            listArticle[position].cateName?.let {
                itemView.lbCategory.text = it
            }
        }
    }

    inner class LoadingViewHolder(view:View):RecyclerView.ViewHolder(view) {

    }
}

interface ArticleDelegate {
    fun clickArticle(pID:Long)
}