package com.vn.akidokids.ui.Course.LessonCourse

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.Course.LessonCourse.view.LessonCourseAdapter
import com.vn.akidokids.ui.Course.LessonCourse.view.LessonCourseFragment
import dagger.Module
import dagger.Provides

@Module
class LessonCourseModule {
    @Provides
    internal fun provideLinearLayout(fragment: LessonCourseFragment):LinearLayoutManager = LinearLayoutManager(fragment.activity)

    @Provides
    internal fun provideAdapter(fragment: LessonCourseFragment):LessonCourseAdapter = LessonCourseAdapter(arrayListOf(), fragment)
}