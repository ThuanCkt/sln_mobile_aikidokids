package com.vn.akidokids.ui.ChangeInformation.view

import android.Manifest
import android.R
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import com.justinnguyenme.base64image.Base64Image
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.vn.akidokids.ui.ChangeInformation.interactor.ChangeInformationInteractor
import com.vn.akidokids.ui.ChangeInformation.presenter.ChangeInformationPresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.*
import com.vn.akidokids.util.extension.hideKeyboard
import com.vn.akidokids.util.extension.loadImage
import com.vn.akidokids.util.extension.toDateString
import com.vn.akidokids.util.extension.toEditable
import kotlinx.android.synthetic.main.activity_change_information.*
import kotlinx.android.synthetic.main.activity_history_point.mToolbar
import kotlinx.android.synthetic.main.activity_register_cause.*
import java.util.*
import javax.inject.Inject

class ChangeInformationActivity : BaseActivity(), ChangeInformationView,
    DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener,
    MultiplePermissionsListener {

    private val GALLERY = 1
    private val CAMERA = 1888

    private var bitmap: Bitmap? = null

    private var base64Image:String = EMPTY_STRING
    @Inject
    lateinit var presenter: ChangeInformationPresenter<ChangeInformationView, ChangeInformationInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        setContentView(com.vn.akidokids.R.layout.activity_change_information)
        requestMultiplePermissions()
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }
         base64Image = Utils.getAvartar()
        if (!base64Image.isEmpty()) {
            Base64Image.instance.decode(base64Image, { it ->
                it?.let {
                    bitmap = it
                }
            })
        }

        presenter.callInformationUser()

        btnSelectDateOfBirth.setOnClickListener {
            clickDatePickerSelect()
        }

        btnChange.setOnClickListener {
            if (validData()) {
                showMessageToast("Bạn chưa nhập đủ dữ liệu.")
                return@setOnClickListener
            }
            var sex = 1
            if (!radioMale.isChecked) {
                sex = 2
            }
            if (base64Image.isNullOrEmpty()) {
                base64Image = Utils.getAvartar()
            }
            presenter.updateData(
                fullName = txtFullName.text.toString(),
                nameDisplay = txtDisplayName.text.toString(),
                sex = sex,
                address = txtAddress.text.toString(),
                phone = txtPhone.text.toString(),
                email = txtEmail.text.toString(),
                avatar = base64Image
            )

        }

        btnChangeAvatar.setOnClickListener {
            showPictureDialog()
        }

        spnProvince.onItemSelectedListener = this
    }


    override fun onStart() {
        super.onStart()


    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    fun clickDatePickerSelect() {
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this, this, mYear, mMonth, mDay)
        datePickerDialog.datePicker.maxDate = Date().time
        datePickerDialog.show()
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        val cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, p1)
        cal.set(Calendar.MONTH, p2)
        cal.set(Calendar.DAY_OF_MONTH, p3)
        presenter.setDateOfBirth(date = cal.time)
        lblBirtdayDisplay.text = cal.time.toDateString(FORMAT_DATE_APEARANCE)
    }

    //delegate view
    override fun succesUserInfor() {
        presenter.getUserData()?.let {
            if (bitmap != null) {
                imgAvatar.setImageBitmap(bitmap)
            } else {
                imgAvatar.loadImage(it.avatar ?: EMPTY_STRING)
            }
            txtFullName.text = it.fullname?.toEditable()
            txtDisplayName.text = it.displayname?.toEditable()
            txtDisplayName.text = it.displayname?.toEditable()
            lblBirtdayDisplay.text = it.birthdaytext?.toEditable()
            txtAddress.text = it.address?.toEditable()
            txtPhone.text = it.phoneNumber?.toEditable()
            txtEmail.text = it.email?.toEditable()
            if (it.sex == 1.toLong()) {
                radioMale.isChecked = true
                radioFeMale.isChecked = false
            } else {
                radioFeMale.isChecked = true
                radioMale.isChecked = false
            }

            lblBirtdayDisplay.text = it.birthdaytext
        }
    }

    override fun successProvince() {
        val list = presenter.getListNameProvince()
        if (list.count() <= 1) {
            spnQuanlityStudent.isEnabled = false
        }
        val adapter = ArrayAdapter(this, R.layout.simple_spinner_dropdown_item, list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnProvince.setAdapter(adapter)
        spnProvince.setSelection(presenter.getIndexDefaultSelectProvince())
    }

    override fun failCallUserInfor(message: String) {
        showMessageToast(message)
    }

    override fun successUpdateData() {
        showMessageToast("Cập nhật thành công")
        EventBus.post(TypeEvenBus.ReloadDataUse("okla"))
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        presenter.provinceSelect(p2)
    }

    fun validData(): Boolean {
        when {
            txtFullName.text.isEmpty() -> {
                return true
            }
            txtDisplayName.text.isEmpty() -> {
                return true
            }
            txtAddress.text.isEmpty() -> {
                return true
            }
            txtPhone.text.isEmpty() -> {
                return true
            }
            txtEmail.text.isEmpty() -> {
                return true
            }
            else -> {
                return false
            }
        }
        return false
    }

    private fun showPictureDialog() {
        this.hideKeyboard()
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Chọn hình thức lấy ảnh")
        val pictureDialogItems = arrayOf("Lấy ảnh từ thư viện ảnh", "Chụp ảnh từ camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
       val intent =  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GALLERY) {
            val imageUri = data?.data

            val selectedPhotoUri = data?.data
            try {
                selectedPhotoUri?.let {
                    if(Build.VERSION.SDK_INT < 28) {
                        val bitmap = MediaStore.Images.Media.getBitmap(
                            this.contentResolver,
                            selectedPhotoUri
                        )
                        imgAvatar.setImageBitmap(bitmap)
                        val aspectRatio =
                            bitmap.getWidth() / bitmap.getHeight().toFloat()
                        val width = 360
                        val height = Math.round(width / aspectRatio)

                        this.bitmap = Bitmap.createScaledBitmap(
                            bitmap, width, height, false
                        )
                        base64Image = ImageUtil.convert(this.bitmap)
                    } else {
                        val source = ImageDecoder.createSource(this.contentResolver, selectedPhotoUri)
                        val bitmap = ImageDecoder.decodeBitmap(source)
                        imgAvatar.setImageBitmap(bitmap)
                        val aspectRatio =
                            bitmap.getWidth() / bitmap.getHeight().toFloat()
                        val width = 360
                        val height = Math.round(width / aspectRatio)

                        this.bitmap = Bitmap.createScaledBitmap(
                            bitmap, width, height, false
                        )
                        base64Image = ImageUtil.convert(this.bitmap)
                    }
                    presenter.callInformationUser()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (requestCode == CAMERA) {

            bitmap = data?.getExtras()?.get("data") as? Bitmap
            if (bitmap == null) {
                return
            }
            Base64Image.instance.encode(bitmap!!) { base64 ->
                base64?.let {
                    base64Image = it
                    presenter.setDataCamera(it)
                }
            }

        }
    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
    }

    override fun onPermissionRationaleShouldBeShown(
        permissions: MutableList<PermissionRequest>?,
        token: PermissionToken?
    ) {
        token?.continuePermissionRequest();
    }

    fun requestMultiplePermissions() {
        val permissions = listOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        Dexter.withActivity(this)
            .withPermissions(permissions)
            .withListener(this)
            .check()
    }


}
