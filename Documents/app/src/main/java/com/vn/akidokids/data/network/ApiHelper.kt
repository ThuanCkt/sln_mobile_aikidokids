package com.vn.akidokids.data.network

import com.vn.akidokids.data.network.model.*
import com.vn.akidokids.data.network.request.*
import io.reactivex.Observable

/**
 * Created by jyotidubey on 04/01/18.
 */
interface ApiHelper {

    //Login and register
    fun performServerLogin(request: LoginRequest.ServerLoginRequest): Observable<LoginResponse>

    fun performServerRegister(request: RegisterRequest): Observable<LoginResponse>
    fun performServerForgotPassword(request: ForgotPasswordRequest): Observable<ForgotPasswordResponse>

    //Category and article
    fun performServerListCategory(): Observable<CategoryResponse>

    fun performServerListArticleFollowCategory(request: CategoryRequest.ServerListArticleFollowCategory): Observable<ArticleCategoryResponse>
    fun performServerDetailArticle(request: CategoryRequest.ServerDetailArticle): Observable<DetailArticleResponse>

    //Course
    fun performServerListCourse(): Observable<ListCourseResponse>

    fun performServerDetailCourse(request: CourseRequest.DetailCourseRequest): Observable<DetailCourseResponse>
    fun performServerQuantityStudent(request: CourseRequest.QuantityStudentRequest): Observable<QuantityStudentResponse>
    fun performServerReferUser(request: CourseRequest.ReferUserRequest): Observable<UserReferResponse>
    fun performServerRegistCourse(request: CourseRequest.BookingCourse): Observable<BookingCourseResponse>
    fun performServerMethodPayment(): Observable<MethodPaymentResponse>

    //User
    fun performServerUserInfor(): Observable<UserInforResponse>
    fun performServerChangePassword(request: UserRequest.ChangePasswordRequest): Observable<LoginResponse>
    fun performServerUserInforById(request: UserRequest.UserIdRequest): Observable<ResponseUserId>
    fun performServerProvince(): Observable<ProvinceResponse>
    fun performServerUserInforUpdate(request: UserRequest.ChangeInforRequest): Observable<ResponseUserId>
    fun performServerUserAvatarUpdate(request: UserRequest.ChangeAvartarRequest): Observable<UpdateAvatarReponse>

    //Class
    fun performServerInforClass(request: ClassRequest.ClassInforRequest): Observable<ClassInforResponse>
    fun performServerArticleClass(request: ClassRequest.ArticleClassRequest): Observable<PostArticleClassResponse>
    fun performServerGalleyClass(request: ClassRequest.GalleyClassRequest): Observable<AlbumClassResponse>
    fun performServerDetailArticleClass(request: ClassRequest.DetailClassRequest): Observable<DetailArticleClassResponse>

    //Term and about
    fun performServerTermAndServer(): Observable<TermAndAboutResponse>

    //History point
    fun performServerHistoryPoint(request: HistoryTransactionRequest.ServerListHistoryTransactionRequest): Observable<HistoryTransactionResponse>
    fun performServerDeleteHistoryPoint(request:HistoryTransactionRequest.DeleteTransaction): Observable<DeteleHistoryResponse>

    //History point
    fun performServerMessage(request:MessageTransactionRequest.ServerListMessageTransactionRequest): Observable<MessageTransactionResponse>
    fun performServerDeleteMessage(request:MessageTransactionRequest.DeleteTransaction): Observable<DeteleHistoryResponse>
}