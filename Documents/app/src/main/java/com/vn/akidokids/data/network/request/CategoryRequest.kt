package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CategoryRequest {
  data class ServerListArticleFollowCategory internal constructor(
      @Expose
      @SerializedName("cid")
      val cid:Int,

      @Expose
      @SerializedName("page")
      val page:Int,

      @Expose
      @SerializedName("pageSize")
      val pageSize:Int
  )

    data class ServerDetailArticle internal constructor(
        @Expose
        @SerializedName("idArticle")
        val idArticle:Long
    )
}