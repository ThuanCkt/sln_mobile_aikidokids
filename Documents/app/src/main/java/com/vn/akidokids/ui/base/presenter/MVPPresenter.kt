package com.vn.akidokids.ui.base.presenter

import com.vn.akidokids.ui.base.interactor.MVPInteractor
import com.vn.akidokids.ui.base.view.MVPView

interface MVPPresenter<V : MVPView, I : MVPInteractor> {
    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?
}