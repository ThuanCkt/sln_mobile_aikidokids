package com.vn.akidokids.ui.splash.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class SplashInteractorImplement @Inject constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper): BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), SplashInteractor {
    override fun seedQuestions(): Observable<Boolean> {
        return Observable.just(false);
    }

    override fun seedOptions(): Observable<Boolean> {
        return Observable.just(true);
    }
}