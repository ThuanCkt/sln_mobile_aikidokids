package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MethodPayment
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.Utils
import kotlinx.android.synthetic.main.item_method_banking.view.*

class MethodBankingAdapter:RecyclerView.Adapter<MethodBankingAdapter.MethodBankingHolder>() {

    private var banks: List<MethodPayment> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MethodBankingHolder {
        return MethodBankingHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_method_banking,parent, false))
    }

    fun setListBank(banks: List<MethodPayment>) {
        this.banks = banks
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return banks.count();
    }

    override fun onBindViewHolder(holder: MethodBankingHolder, position: Int) {
        holder.clear()
        holder.bindView(position)
    }

    inner class MethodBankingHolder(view: View):RecyclerView.ViewHolder(view) {
        fun clear() {
            itemView.lbNameBank.text = EMPTY_STRING
            itemView.lbFullName.text = EMPTY_STRING
            itemView.lbBankNumber.text = EMPTY_STRING
            itemView.lbBranch.text = EMPTY_STRING
        }

        fun bindView(position: Int) {
            banks[position]?.let {
                itemView.lbNameBank.text = "${position + 1}, ${it.name}"
                itemView.lbBankNumber.text ="Số tài khoản: ${it.accountNumber}"
                itemView.lbFullName.text = "Tên tài khoản: ${it.accountOwner}"
                itemView.lbBranch.text = "Chi nhánh: ${it.branchName}"
            }

            itemView.setOnClickListener {
                Utils.setClipboard(AikidoKidsApplication.context,  banks[position]?.accountNumber ?: EMPTY_STRING)
                Toast.makeText(AikidoKidsApplication.context, AikidoKidsApplication.getAppResources().getString(R.string.copy_completed), Toast.LENGTH_LONG).show()
            }
        }
    }
}