package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetailArticleResponse (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String,

    @Expose
    @SerializedName("result")
    var result: ResultDetailArticle
)

data class ResultDetailArticle (
    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("lstakdarticlelink")
    var lstakdarticlelink: List<Lstakdarticlelink>? = null,

    @Expose
    @SerializedName("lstakdarticlerelated")
    var lstakdarticlerelated: List<ResultDetailArticle>? = null,

    @Expose
    @SerializedName("id")
    var id: Long,

    @Expose
    @SerializedName("gid")
    var gid: Long,

    @Expose
    @SerializedName("title")
    var title: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("img")
    var img: String? = null,

    @Expose
    @SerializedName("img_thumbnail")
    var imgThumbnail: String? = null,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("contents_html")
    var contentsHTML: String? = null,

    @Expose
    @SerializedName("contents")
    var contents: Any? = null,

    @Expose
    @SerializedName("acc_signature")
    var accSignature: Any? = null,

    @Expose
    @SerializedName("slogan")
    var slogan: Any? = null,

    @Expose
    @SerializedName("is_hot")
    var isHot: Boolean,

    @Expose
    @SerializedName("is_cool")
    var isCool: Boolean,

    @Expose
    @SerializedName("is_popular")
    var isPopular: Boolean,

    @Expose
    @SerializedName("cate_name")
    var cateName: String,

    @Expose
    @SerializedName("cate_slug")
    var cateSlug: String,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String,

    @Expose
    @SerializedName("created_by")
    var createdBy: String,

    @Expose
    @SerializedName("iscreated_logok")
    var createdLog: Any? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    var mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    var userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    var modeaction: Long,

    @Expose
    @SerializedName("langid")
    var langid: Long,

    @Expose
    @SerializedName("langcode")
    var langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    var langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    var lstlang: Any? = null
)

data class Lstakdarticlelink (
    @Expose
    @SerializedName("id")
    var id: Long,

    @Expose
    @SerializedName("title")
    var title: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("cate_name")
    var cateName: Any? = null,

    @Expose
    @SerializedName("cate_slug")
    var cateSlug: Any? = null
)
