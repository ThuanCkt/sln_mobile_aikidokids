package com.vn.akidokids.ui.main.ListCourse.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.data.network.model.DataCourse
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.ui.main.ListCourse.interacror.ListCourseInteractor
import com.vn.akidokids.ui.main.ListCourse.view.ListCourseView
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ListCoursePresenterImplement<V:ListCourseView, I:ListCourseInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable):
BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), ListCoursePresenter<V, I>{

    private var listCourse = mutableListOf<DataCourse>()

    override fun onAttach(view: V?) {
        super.onAttach(view)

    }

    override fun callListCourse() {
        interactor?.let {
            compositeDisposable.add(
                it.getListCourse().compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({response ->
                        if (response.result == null) {
                            return@subscribe
                        }

                        if (response.result?.msgcode != null) {
                            handleError(response.result?.msgcode ?: EMPTY_STRING)
                        }

                        if (response.result?.isshow == true) {
                            return@subscribe
                        }

                        response.result?.data?.let {
                            listCourse.addAll(it)
                            getView()?.sucessCallApi()
                        }
                    }, {
                            val err = it as? ANError
                            if(err?.errorCode == 401 || err?.errorCode == 403) {
                                this.handleError("401")
                            }
                        })
            )
        }
    }

    override fun getListCourse(): List<DataCourse> {
        return this.listCourse
    }

    override fun pullToRefresh() {
        this.listCourse.clear()
        callListCourse()
    }
}