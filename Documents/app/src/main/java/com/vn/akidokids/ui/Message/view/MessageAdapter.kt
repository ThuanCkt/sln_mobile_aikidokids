package com.vn.akidokids.ui.Message.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MessageTransactionData
import kotlinx.android.synthetic.main.item_message.view.*

class MessageAdapter(
    private var listHistory: MutableList<MessageTransactionData>,
    val delegate: MessageAdapter.MessageAdapterAdapterInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_TYPE_ITEM = 0
    val VIEW_TYPE_LOADING = 1

    var isLoading:Boolean = false

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            return MessageHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_message,
                    parent,
                    false
                )
            )
        }

        return MessageHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_loading, parent, false))

    }

    override fun getItemViewType(position: Int): Int {
        if ( this.listHistory.count() > position)
            return VIEW_TYPE_ITEM

        return VIEW_TYPE_LOADING
    }

    fun setListHistory(listHistory: List<MessageTransactionData>, isLoading:Boolean) {
        this.isLoading = isLoading
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    fun setListHistory(listHistory: List<MessageTransactionData>) {
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        if (this.isLoading)
            return listHistory.count() + 1

        return listHistory.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (this.getItemViewType(position)) {
            VIEW_TYPE_ITEM -> {
                val holder = holder as? MessageAdapter.MessageHolder
                holder?.let {
                    it.bindData(position)
                }
            }

            VIEW_TYPE_LOADING -> {
                holder as? MessageAdapter.LoadingViewHolder
            }

            else -> return
        }
    }

    inner class MessageHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindData(position: Int) {
            listHistory[position].msg?.let {
                itemView.lbContent.text = it
            }

            listHistory[position].msg_time?.let {
                itemView.lbDateDisplay.text = it
            }

            itemView.setOnClickListener {
                delegate.onClickItem(position)
            }
        }
    }

    inner class LoadingViewHolder(view:View):RecyclerView.ViewHolder(view) {

    }

    interface MessageAdapterAdapterInterface {
        fun onClickItem(index: Int)
    }
}