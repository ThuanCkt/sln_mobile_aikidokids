package com.vn.akidokids.ui.HistoryPoint

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.HistoryPoint.interactor.HistoryPointInteractor
import com.vn.akidokids.ui.HistoryPoint.interactor.HistoryPointInteractorImplement
import com.vn.akidokids.ui.HistoryPoint.presenter.HistoryPointPresenter
import com.vn.akidokids.ui.HistoryPoint.presenter.HistoryPointPresenterImplement
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPoinAdapter
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPointActivity
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPointView
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class HistoryPointModule {

    @Provides
    fun provideInteractor(interactor: HistoryPointInteractorImplement): HistoryPointInteractor =
        interactor

    @Provides
    fun providePresenter(presenter: HistoryPointPresenterImplement<HistoryPointView, HistoryPointInteractor>): HistoryPointPresenter<HistoryPointView, HistoryPointInteractor> =
        presenter

    @Provides
    fun provideAdapter(delegate:HistoryPointActivity):HistoryPoinAdapter = HistoryPoinAdapter(
        ArrayList(), delegate)

    @Provides
    fun provideLinearLayoutManage(context:HistoryPointActivity) = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
}