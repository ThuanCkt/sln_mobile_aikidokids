package com.vn.akidokids.ui.Course.LessonCourse.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.AlbumClassData
import com.vn.akidokids.util.extension.loadImage
import kotlinx.android.synthetic.main.item_galley_class.view.*

class GalleyClassAdapter(private val delegate: GalleyClassAdapter.GalleyClassAdapterInterface):RecyclerView.Adapter<GalleyClassAdapter.LessonCourseHolder>() {
    private var listAlbum:List<AlbumClassData> = arrayListOf()

    override fun onBindViewHolder(holder: LessonCourseHolder, position: Int) {
        holder.bindData(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonCourseHolder {
        return LessonCourseHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_galley_class,
                parent,
                false
            )
        )
    }

    fun setListAlbum(listAlbum:List<AlbumClassData>) {
        this.listAlbum = listAlbum
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listAlbum.count()
    }


    inner class LessonCourseHolder(view:View):RecyclerView.ViewHolder(view){
        fun bindData(position:Int) {
            listAlbum[position].img?.let {
                itemView.imgThumbnail.loadImage(it)
            }

            itemView.setOnClickListener {
                delegate.itemGalleyClick(position)
            }
        }
    }

    interface GalleyClassAdapterInterface {
        fun itemGalleyClick(position: Int)
    }
}