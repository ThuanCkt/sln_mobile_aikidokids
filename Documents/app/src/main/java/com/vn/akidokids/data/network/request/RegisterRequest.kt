package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegisterRequest internal constructor(
    @Expose
    @SerializedName("username") internal val username: String,
    @Expose
    @SerializedName("password") internal val password: String,
    @Expose
    @SerializedName("deviceID") internal val deviceID: String,
    @Expose
    @SerializedName("fullname") internal val fullname: String,
    @Expose
    @SerializedName("tokenAPNs") internal val tokenAPNs:String,
    @Expose
    @SerializedName("deviceName") internal val deviceName:String
)