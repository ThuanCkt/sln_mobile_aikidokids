package com.vn.akidokids.ui.main.Category.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.ArticleCategoryResponse
import com.vn.akidokids.data.network.request.CategoryRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class CategoryInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) :
    BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), CategoryInteractor {

    override fun getListCategory(
        cid: Int,
        page: Int,
        pageSize: Int
    ): Observable<ArticleCategoryResponse> {
        return apiHelper.performServerListArticleFollowCategory(
            CategoryRequest.ServerListArticleFollowCategory(
                cid = cid,
                page = page,
                pageSize = pageSize
            )
        )
    }
}