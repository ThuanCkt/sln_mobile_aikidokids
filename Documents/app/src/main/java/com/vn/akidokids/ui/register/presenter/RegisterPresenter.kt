package com.vn.akidokids.ui.register.presenter

import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.ui.register.interactor.RegisterInteractor
import com.vn.akidokids.ui.register.view.RegisterView

interface RegisterPresenter<V: RegisterView, I:RegisterInteractor>: MVPPresenter<V, I> {
    fun registerAccount(username:String, fullname:String, password:String, rePassword:String, isAgree:Boolean)
}