package com.vn.akidokids.data.preferences

import com.vn.akidokids.data.network.model.UserEntity
import com.vn.akidokids.util.AppConstants

/**
 * Created by jyotidubey on 04/01/18.
 */
interface PreferenceHelper {

    fun getCurrentUserLoggedInMode(): Int

    fun setCurrentUserLoggedInMode(mode: AppConstants.LoggedInMode)

    fun getCurrentUserId(): Long?

    fun setCurrentUserId(userId: String?)

    fun getCurrentUserName(): String

    fun setCurrentUserName(userName: String?)

    fun getCurrentUserEmail(): String?

    fun setCurrentUserEmail(email: String?)

    fun getAccessToken(): String?

    fun setAccessToken(accessToken: String?)

    fun setIsRemember(isRemember:Boolean)

    fun getIsRemember():Boolean

    fun setUserEntity(user: UserEntity?)

    fun getUserEntity():UserEntity

    fun setLinkImage(link: String)
}