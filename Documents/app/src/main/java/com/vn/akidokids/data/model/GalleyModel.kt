package com.vn.akidokids.data.model

import android.os.Parcelable
import com.vn.akidokids.data.network.model.AlbumClassData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GalleyModel(var index:Int, var listImages:List<AlbumClassData>? = null): Parcelable {
}