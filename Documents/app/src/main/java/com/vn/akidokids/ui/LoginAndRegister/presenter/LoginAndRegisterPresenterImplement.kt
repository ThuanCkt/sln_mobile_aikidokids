package com.vn.akidokids.ui.LoginAndRegister.presenter

import com.vn.akidokids.ui.LoginAndRegister.interactor.LoginAndRegisterInteractor
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginAndRegisterPresenterImplement<V : LoginAndRegisterView, I : LoginAndRegisterInteractor>
@Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(interactor, schedulerProvider, disposable),
    LoginAndRegisterPresenter<V, I> {


}