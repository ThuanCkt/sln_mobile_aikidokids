package com.vn.akidokids.ui.Course.Course

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.vn.akidokids.ui.Course.DetaiCourse.DetailCourseFragment
import com.vn.akidokids.ui.Course.LessonCourse.view.LessonCourseFragment
import com.vn.akidokids.ui.Course.ListTeacher.view.TeacherFragment

class CoursePagerAdapter (fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    val detailCourseFragment = DetailCourseFragment.newInstance()
    val lessonCourseFragment = LessonCourseFragment.newInstance()
    val teacherFragment = TeacherFragment.newInstance()

    private var tabCount = 0

    override fun getCount(): Int {
        return tabCount
    }

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> detailCourseFragment
            1 -> lessonCourseFragment
            2 -> teacherFragment
            else -> null
        }
    }

    internal fun setCount(count: Int) {
        this.tabCount = count
    }

}