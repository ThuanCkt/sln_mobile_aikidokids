package com.vn.akidokids.ui.ChangePassword.view

import com.vn.akidokids.ui.base.view.MVPView

interface ChangePasswordView:MVPView {

    fun validChangePassword(string:String)
    fun changeSuccess()
}