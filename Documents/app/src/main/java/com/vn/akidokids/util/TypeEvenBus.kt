package com.vn.akidokids.util

import android.graphics.Bitmap

class TypeEvenBus {

    data class ReloadDataUse(val text:String)
    data class ChooseAvatar(val bitmap: Bitmap)
    data class LogOut(val text:String? = null)
    data class LoginSuccess(val text:String)
}