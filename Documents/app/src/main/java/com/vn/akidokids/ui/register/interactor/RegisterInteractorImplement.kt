package com.vn.akidokids.ui.register.interactor


import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.LoginResponse
import com.vn.akidokids.data.network.model.ResultUser
import com.vn.akidokids.data.network.request.RegisterRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import com.vn.akidokids.util.AppConstants
import com.vn.akidokids.util.Utils
import io.reactivex.Observable
import javax.inject.Inject

class RegisterInteractorImplement @Inject constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper): BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),RegisterInteractor  {

    override fun registerAccount(
        username: String,
        password: String,
        fullname: String
    ): Observable<LoginResponse> {
        return apiHelper.performServerRegister(RegisterRequest(username = username, password = password, deviceName = Utils.getNameDevice(), deviceID = Utils.getModelDevice(),tokenAPNs = "", fullname = fullname))
    }

    override fun updateUserInSharedPref(userResult: ResultUser, loggedInMode: AppConstants.LoggedInMode) =
        preferenceHelper.let {
            it.setCurrentUserId(userResult?.user?.id)
            it.setAccessToken(userResult?.token)
            it.setCurrentUserEmail(userResult?.user?.email)
            it.setCurrentUserName(userResult?.user?.username)
            it.setCurrentUserLoggedInMode(loggedInMode)
            it.setIsRemember(false)
        }
}