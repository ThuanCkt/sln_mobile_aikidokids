package com.vn.akidokids.ui.HistoryPoint.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.HistoryTransactionData
import com.vn.akidokids.data.network.request.HistoryTransactionRequest
import com.vn.akidokids.ui.HistoryPoint.interactor.HistoryPointInteractor
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPointView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.util.Utils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HistoryPointPresenterImplement<V : HistoryPointView, I : HistoryPointInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = disposable
), HistoryPointPresenter<V, I> {

    var page: Long = 1
    var sizePage: Int = 6
    var isLoadMore: Boolean = false
    var isShowLoading: Boolean = true
    var listHistory: MutableList<HistoryTransactionData> = arrayListOf()

    override fun callListHistoryTransaction() {
        interactor?.let {
            if (this.isShowLoading) {
                this.getView()?.showProgress()
            }
            compositeDisposable.add(
                it.getListHistory(page = this.page.toInt(), sizePage = this.sizePage).compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ response ->
                        this.isShowLoading = true
                        getView()?.hideProgress()
                        if (response.result == null) {
                            this.getView()?.failCallApi(
                                message = AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response.result?.msgcode != null) {
                            handleError(response.result?.msgcode ?: EMPTY_STRING)
                        }

                        if (response.result?.isshow == true) {
                            this.getView()
                                ?.failCallApi(message = response.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.isLoadMore = false
                        this.page = response.result?.page?.currentpage ?: 1
                        response.result?.let { it ->
                            listHistory.addAll(it.data ?: ArrayList())
                            getView()?.shouldShowMore(listHistory.count() > 0)
                            this.getView()?.successGetListHistoryData(isLoadMore = it.page?.isloadmore ?: false)
                        }
                    }, {
                        this.getView()?.hideProgress()
                        this.isShowLoading = true;
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                        this.getView()?.failCallApi(
                            message = AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet
                            )
                        )
                    })
            )
        }
    }

    override fun deleteHistory(position: Int) {
        if(position >= this.listHistory.count() && position != -1) {
            return
        }
        var idHistory = "delete_all"
        if (position != -1) {
            idHistory = this.listHistory[position].tranID ?: EMPTY_STRING
        }

        val data = HistoryTransactionRequest.DeleteTransaction(
            userId = Utils.getUserId(),
            listId = idHistory
        )
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.deleteHistoryPoint(data).compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            this.getView()?.failCallApi(
                                message = AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        this.getView()?.failCallApi(message = response.result?.msg ?: EMPTY_STRING)
                        if(response.isok == true) {
                            if(response.isok == true) {
                                if (position == - 1) {
                                    this.listHistory.clear()
                                } else {
                                    this.listHistory.removeAt(position)
                                }
                                this.getView()?.deleteSuccess()
                            }
                        }
                        return@subscribe

                    }, { error ->
                        this.getView()?.hideProgress()
                        this.getView()?.failCallApi(
                            message = AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet
                            )
                        )
                    })
            )
        }
    }

    override fun getListHistoryPoint(): List<HistoryTransactionData> {
        return listHistory ?: arrayListOf()
    }

    override fun loadMore() {
        if (this.isLoadMore) {
            return
        }

        this.isShowLoading = false

        this.isLoadMore = true
        this.page += 1

        this.callListHistoryTransaction()
    }

    override fun pullToRefresh() {
        page = 1
        this.isShowLoading = false
        this.listHistory.clear()
        this.callListHistoryTransaction()
    }
}