package com.vn.akidokids.ui.Class.ClassActivity.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MemberInClass
import com.vn.akidokids.util.extension.loadImage
import kotlinx.android.synthetic.main.member_in_class_item.view.*

class MemberClassAdapter:RecyclerView.Adapter<MemberClassAdapter.MemberClassHolder>() {
    private var listMemeber:List<MemberInClass> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberClassHolder {
        return MemberClassHolder(LayoutInflater.from(parent.context).inflate(R.layout.member_in_class_item, parent, false))
    }

    override fun getItemCount(): Int {
        return listMemeber.count()
    }

    fun setListMember(listMemeber:List<MemberInClass>) {
        this.listMemeber = listMemeber
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: MemberClassHolder, position: Int) {
        holder.bindData(position)
    }

    inner class MemberClassHolder(view: View):RecyclerView.ViewHolder(view) {
        fun bindData(position: Int) {
            listMemeber[position].avatar?.let{
                itemView.imgAvatar.loadImage(it)
            }
        }
    }
}