package com.vn.akidokids.ui.register.presenter

import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ResultUser
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.ui.register.interactor.RegisterInteractor
import com.vn.akidokids.ui.register.view.RegisterView
import com.vn.akidokids.util.AppConstants
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RegisterPresenterImplement<V: RegisterView, I:RegisterInteractor>
@Inject internal constructor(interactor: I, schedulerProvide:SchedulerProvider, disposable: CompositeDisposable)
    :BasePresenter<V, I>(interactor, schedulerProvide, disposable), RegisterPresenter<V, I> {


    override fun registerAccount(
        username: String,
        fullname: String,
        password: String,
        rePassword: String,
        isAgree: Boolean
    ) {
        when {
            username.isEmpty() -> this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(
                R.string.username_not_empty))
            fullname.isEmpty() -> this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.fullname_not_empty))
            password.isEmpty() -> this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.password_not_empty))
            rePassword.isEmpty() -> this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.re_password_not_empty))
            rePassword != password -> this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.password_not_same_re_password))
            !isAgree -> this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.you_not_agree_term))
            else -> {
                this.getView()?.let {
                    it.showProgress()
                }

                this.interactor?.let {
                    it.registerAccount(username = username, password = password, fullname = fullname)
                        .compose(schedulerProvider.ioToMainObservableScheduler())
                        .subscribe( { loginResponse ->
                            getView()?.hideProgress()
                            if (loginResponse.result == null) {
                                this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.account_exits))
                                return@subscribe
                            }

                            if(loginResponse.result?.isshow == true) {
                                this.getView()?.messageError(loginResponse.result?.msg ?: "" )
                                return@subscribe
                            }

                            this.updateUserInSharedPref(loginResponse.result!!, AppConstants.LoggedInMode.LOGGED_IN_MODE_SERVER)
                            this.getView()?.openMainActivity()
                        }, { err ->
                            getView()?.hideProgress()
                            this.getView()?.messageError(AikidoKidsApplication.getAppResources().getString(R.string.account_exits))
                        })
                }
            }
        }
    }

    private fun updateUserInSharedPref(
        resultUser: ResultUser,
        loggedInMode: AppConstants.LoggedInMode) =
        interactor?.updateUserInSharedPref(resultUser, loggedInMode)
}