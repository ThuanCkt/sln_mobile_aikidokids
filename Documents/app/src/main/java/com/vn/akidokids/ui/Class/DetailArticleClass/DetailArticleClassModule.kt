package com.vn.akidokids.ui.Class.DetailArticleClass

import com.vn.akidokids.ui.Class.DetailArticleClass.interactor.DetailArticleClassInteractor
import com.vn.akidokids.ui.Class.DetailArticleClass.interactor.DetailArticleClassInteractorImplement
import com.vn.akidokids.ui.Class.DetailArticleClass.presenter.DetailArticlePresenter
import com.vn.akidokids.ui.Class.DetailArticleClass.presenter.DetailArticlePresenterImplement
import com.vn.akidokids.ui.Class.DetailArticleClass.view.DetailArticleView
import dagger.Module
import dagger.Provides

@Module
class DetailArticleClassModule {

    @Provides
    internal fun provideInteractor(interactor: DetailArticleClassInteractorImplement):DetailArticleClassInteractor = interactor

    @Provides
    internal fun providePresenter(presenter: DetailArticlePresenterImplement<DetailArticleView, DetailArticleClassInteractor>):DetailArticlePresenter<DetailArticleView, DetailArticleClassInteractor> = presenter
}