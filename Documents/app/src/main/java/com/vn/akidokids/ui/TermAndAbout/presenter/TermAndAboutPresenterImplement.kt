package com.vn.akidokids.ui.TermAndAbout.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.TermAndAboutResult
import com.vn.akidokids.ui.TermAndAbout.interactor.TermAndAboutInteractor
import com.vn.akidokids.ui.TermAndAbout.view.TermAndAboutView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TermAndAboutPresenterImplement<V : TermAndAboutView, I : TermAndAboutInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    dispose: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = dispose
), TermAndAboutPresenter<V, I> {

    var resultTermAndAbout: TermAndAboutResult? = null

    override fun callTermAndAbout() {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.callTermAndAbout().compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet))
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.resultTermAndAbout = response.result
                        getView()?.successCallApi()
                    },
                    {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                    })
            )
        }
    }

    override fun getAbout(): String {
        return this.resultTermAndAbout?.about ?: EMPTY_STRING
    }

    override fun getTerm(): String {
        return this.resultTermAndAbout?.terms ?: EMPTY_STRING
    }
}