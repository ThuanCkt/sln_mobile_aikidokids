package com.vn.akidokids.ui.Course.ListTeacher

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.Course.ListTeacher.view.TeacherAdapter
import com.vn.akidokids.ui.Course.ListTeacher.view.TeacherFragment
import dagger.Module
import dagger.Provides

@Module
class TeacherModule {
    @Provides
    internal fun provideLinearLayout(fragment: TeacherFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)

    @Provides
    internal fun provideAdapter(): TeacherAdapter = TeacherAdapter()
}