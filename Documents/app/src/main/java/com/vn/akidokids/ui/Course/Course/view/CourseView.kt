package com.vn.akidokids.ui.Course.Course.view

import com.vn.akidokids.ui.base.view.MVPView

interface CourseView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi()
}