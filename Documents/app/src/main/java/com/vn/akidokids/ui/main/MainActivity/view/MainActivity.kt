package com.vn.akidokids.ui.main.MainActivity.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.vn.akidokids.data.model.EnumMenuType
import com.vn.akidokids.data.model.MenuItem
import com.vn.akidokids.data.model.MenuSection
import com.vn.akidokids.ui.AccountDetail.view.AccountDetailActivity
import com.vn.akidokids.ui.Class.ClassActivity.view.ClassActivity
import com.vn.akidokids.ui.Course.Course.view.CourseActivity
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPointActivity
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterActivity
import com.vn.akidokids.ui.Message.view.MessageActivity
import com.vn.akidokids.ui.TermAndAbout.view.TermAndAboutActivity
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.ui.main.MainActivity.MainPagerAdapter
import com.vn.akidokids.ui.main.MainActivity.MenuAdapter
import com.vn.akidokids.ui.main.MainActivity.interactor.MainInteractor
import com.vn.akidokids.ui.main.MainActivity.presenter.MainPresenter
import com.vn.akidokids.util.EventBus
import com.vn.akidokids.util.ParameterAttribute
import com.vn.akidokids.util.TypeEvenBus
import com.vn.akidokids.util.Utils
import com.vn.akidokids.util.extension.loadImage
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView, HasAndroidInjector, ViewPager.OnPageChangeListener, InterfaceMenu, CategoryAdapterInterface {

    val SEGMENT_HOME = 0
    val SEGMENT_COURSE = 1
    var isHome:Boolean = true
    var cid:Int = 0

    val  GROUP_COURSE:String = "2"
    val  GROUP_CATEGORY:String = "1"

    val SECTION_CLASS = 1

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var presenter: MainPresenter<MainView, MainInteractor>

    @Inject
    lateinit var categoryAdapter: CategoryAdapter

    @Inject
    lateinit var menuAdapter: MenuAdapter

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager


    internal lateinit var mainPagerAdapter: MainPagerAdapter

    var listSectionMenu: MutableList<MenuSection> = arrayListOf()

    private var disposable: Disposable? = null

    private var disposableLogout: Disposable? = null
    private var disposableLoginSuccess: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        setContentView(com.vn.akidokids.R.layout.activity_main)
        this.setUp()
        btnMenu.setOnClickListener {
            val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }


    fun setUp() {
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rcCategory.layoutManager = layoutManager
        rcCategory.itemAnimator = DefaultItemAnimator()
        rcCategory.adapter = categoryAdapter
        this.refeshDataCategory()
        mainPagerAdapter =
            MainPagerAdapter(supportFragmentManager)
        setUpFeedPagerAdapter()

        segueLeft.setOnClickListener {
            this.isHome = true
            this.cid = 0
            this.changeUISegment()
            this.categoryAdapter.setCidActive(this.cid)
            this.mainPagerAdapter.categoryFragment.setCiid(this.cid)
        }

        SegueRight.setOnClickListener {
            this.isHome = false
            this.changeUISegment()
        }

        imgAvatar.setOnClickListener {
            openInformation()
        }

        btnInformation.setOnClickListener {
            openInformation()
        }

        listSectionMenu = MenuSection.initListMenu(context = this)
        val listMenu= MenuSection.convertOneDimensionalArray(listSectionMenu)
        layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rcMenu.layoutManager = layoutManager
        rcMenu.itemAnimator = DefaultItemAnimator()
        rcMenu.adapter = menuAdapter
        menuAdapter.addMenuToList(listMenu)

        btnLogout.setOnClickListener {
            this.presenter.onDrawerOptionLogoutClick()
            this.showViewNotLogin()
        }

        lbLogin.setOnClickListener {
            this.openLoginAndRegisterActivity(true)
        }

        lbRegister.setOnClickListener {
            this.openLoginAndRegisterActivity(false)
        }

        disposable =
            EventBus.subscribe<TypeEvenBus.ReloadDataUse>()
                // if you want to receive the event on main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                   presenter.getInformationUser()
                })

        disposableLogout = EventBus.subscribe<TypeEvenBus.LogOut>()
            // if you want to receive the event on main thread
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                presenter.onDrawerOptionLogoutClick()
                this.showProgress()
                Handler().postDelayed({
                    val intent = Intent(this, MainActivity::class.java)
                    this.startActivity(intent)
                    this.hideProgress()
                }, 2000)

            })

        disposableLoginSuccess = EventBus.subscribe<TypeEvenBus.LoginSuccess>()
            // if you want to receive the event on main thread
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                this.showViewLogin()
                presenter.getInformationUser()
            })

        if(Utils.isLogin()) {
            showViewLogin()
            return
        }

        showViewNotLogin()
    }

    private fun showViewLogin() {
        this.relativeNotLogin.visibility = View.GONE
        this.relativeLogin.visibility = View.VISIBLE
        listSectionMenu = MenuSection.initListMenu(this)
        val listMenu= MenuSection.convertOneDimensionalArray(listSectionMenu)
        menuAdapter.addMenuToList(listMenu)
    }

    private fun showViewNotLogin() {
        this.relativeNotLogin.visibility = View.VISIBLE
        this.relativeLogin.visibility = View.GONE

        listSectionMenu = MenuSection.initListMenuNotLogin(this)
        val listMenu= MenuSection.convertOneDimensionalArray(listSectionMenu)
        menuAdapter.addMenuToList(listMenu)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        this.presenter.onDetach()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }


    private fun setUpFeedPagerAdapter() {
        mainPagerAdapter.count = 2
        mainViewPager.adapter = mainPagerAdapter
        mainViewPager.offscreenPageLimit = 2
        mainViewPager.setOnPageChangeListener(this)
        mainViewPager.setPagingEnabled(false)

    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    fun activeSegueLeft() {
        if (this.cid > 0) {
            inActiveLeftAndRight()
            return
        }
        segueLeft.setBackgroundResource(com.vn.akidokids.R.drawable.segment_left_active)
        SegueRight.setBackgroundResource(com.vn.akidokids.R.drawable.segment_right)
    }

    fun activeSegueRight() {
        segueLeft.setBackgroundResource(com.vn.akidokids.R.drawable.segment_left)
        SegueRight.setBackgroundResource(com.vn.akidokids.R.drawable.segment_right_active)
    }

    fun inActiveLeftAndRight() {
        SegueRight.setBackgroundResource(com.vn.akidokids.R.drawable.segment_right)
        segueLeft.setBackgroundResource(com.vn.akidokids.R.drawable.segment_left)
    }

    fun changeUISegment() {
        if (this.isHome) {
            activeSegueLeft()
            mainViewPager.setCurrentItem(SEGMENT_HOME)
        } else {
            activeSegueRight()
            mainViewPager.setCurrentItem(SEGMENT_COURSE)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
        this.isHome = mainViewPager.currentItem == SEGMENT_HOME
        if (this.isHome) {
            activeSegueLeft()
        } else {
            activeSegueRight()
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

    override fun clickMenu(menu: Any) {
        if (menu is MenuItem) {
            val menuItem = menu as? MenuItem
            when(menu.menuType) {
                EnumMenuType.CLASS -> openClass(menuItem!!)
                EnumMenuType.ABOUT -> openAbout()
                EnumMenuType.TERM -> openTerm()
                EnumMenuType.HISTORY_POINT -> openHistoryPoint()
                EnumMenuType.MESSSAGE -> openMessage()
                else -> null
            }
        }
    }

    private fun openClass(menu:MenuItem) {
        val intent = Intent(this, ClassActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, menu.id)
        this.startActivity(intent)
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun openTerm() {
        val intent = Intent(this, TermAndAboutActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, 1)
        this.startActivity(intent)
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun openAbout() {
        val intent = Intent(this, TermAndAboutActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, 0)
        this.startActivity(intent)
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun openHistoryPoint() {
        val intent = Intent(this, HistoryPointActivity::class.java)
        this.startActivity(intent)
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun openMessage() {
        val intent = Intent(this, MessageActivity::class.java)
        this.startActivity(intent)
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

        private fun openInformation() {
        val intent = Intent(this, AccountDetailActivity::class.java)
        startActivity(intent)
        val drawerLayout: DrawerLayout = findViewById(com.vn.akidokids.R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    override fun openLoginAndRegisterActivity(isLogin:Boolean) {
        val intent = Intent(this, LoginAndRegisterActivity::class.java)
        intent.putExtra(ParameterAttribute.IS_LOGIN.name, isLogin)
        this.startActivity(intent)
    }

    override fun refeshDataCategory() {
        presenter.listCategory?.let {
            categoryAdapter.setCategoryToList(it)
        }
    }

    override fun categoryClick(position: Int) {
        if (presenter.getGroupMenuIdOnPosition(position) == GROUP_CATEGORY) {
            this.clickCategory(position)
            return
        }

        if (presenter.getGroupMenuIdOnPosition(position) == GROUP_COURSE) {
            this.clickItemCourse(presenter.getCidOnPosition(position).toLong())
            return
        }
    }

    fun clickItemCourse(id: Long) {
        val intent = Intent(this, CourseActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, id)
        this.startActivity(intent)
    }

    fun clickCategory(position: Int) {
        this.cid = presenter.getCidOnPosition(position)
        this.categoryAdapter.setCidActive(this.cid)
        this.inActiveLeftAndRight()
        this.mainViewPager.setCurrentItem(SEGMENT_HOME)
        this.mainPagerAdapter.categoryFragment.setCiid(this.cid)
    }

    override fun getSuccesUserInfor() {
        lbDisplay.text = presenter.getFullname()
        lbEmail.text = presenter.getEmail()
        lbPoint.text = presenter.getPoint()
        imgAvatar.loadImage(presenter.getImageAvatar())
        listSectionMenu = MenuSection.initListMenu(context = this)
        val listMenu= presenter.getListClass()
        listSectionMenu[SECTION_CLASS].listMenuItem  = listMenu
        val listMenuConvert= MenuSection.convertOneDimensionalArray(listSectionMenu)
        menuAdapter.addMenuToList(listMenuConvert)
    }
}


