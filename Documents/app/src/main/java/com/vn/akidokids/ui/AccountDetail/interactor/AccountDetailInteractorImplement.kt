package com.vn.akidokids.ui.AccountDetail.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.UserInforResponse
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import com.vn.akidokids.util.EMPTY_STRING
import io.reactivex.Observable
import javax.inject.Inject

class AccountDetailInteractorImplement @Inject constructor(
    apiHelper: ApiHelper,
    preferenceHelper: PreferenceHelper
) :BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    AccountDetailInteractor {

    override fun getUserInfor(): Observable<UserInforResponse> {
        return apiHelper.performServerUserInfor()
    }

    override fun clearLinkImage() {
        preferenceHelper.setLinkImage(EMPTY_STRING)
    }
}