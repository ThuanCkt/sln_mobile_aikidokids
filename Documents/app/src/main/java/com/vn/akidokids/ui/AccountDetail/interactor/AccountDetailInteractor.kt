package com.vn.akidokids.ui.AccountDetail.interactor

import com.vn.akidokids.data.network.model.UserInforResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface AccountDetailInteractor:MVPInteractor {
    fun getUserInfor(): Observable<UserInforResponse>
    fun clearLinkImage()
}