package com.vn.akidokids.ui.ChangeInformation.presenter

import com.vn.akidokids.data.network.model.UserEntity
import com.vn.akidokids.ui.ChangeInformation.interactor.ChangeInformationInteractor
import com.vn.akidokids.ui.ChangeInformation.view.ChangeInformationView
import com.vn.akidokids.ui.base.presenter.MVPPresenter
import java.util.*

interface ChangeInformationPresenter<V : ChangeInformationView, I : ChangeInformationInteractor> :
    MVPPresenter<V, I> {

    fun callInformationUser()
    fun callProvince()

    fun getUserData(): UserEntity?
    fun getListNameProvince(): List<String>
    fun getIndexDefaultSelectProvince(): Int
    fun setDateOfBirth(date: Date)
    fun provinceSelect(index: Int)
    fun updateData(
        fullName: String,
        nameDisplay: String,
        sex: Int,
        address: String,
        phone: String,
        email: String,
        avatar: String?
    )

    fun setDataCamera(link:String)
}