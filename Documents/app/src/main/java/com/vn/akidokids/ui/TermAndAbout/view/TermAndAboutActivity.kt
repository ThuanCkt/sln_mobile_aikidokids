package com.vn.akidokids.ui.TermAndAbout.view

import android.os.Bundle
import com.vn.akidokids.R
import com.vn.akidokids.ui.TermAndAbout.interactor.TermAndAboutInteractor
import com.vn.akidokids.ui.TermAndAbout.presenter.TermAndAboutPresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.activity_class.*
import kotlinx.android.synthetic.main.fragment_detail_cause.*
import javax.inject.Inject

class TermAndAboutActivity : BaseActivity(),TermAndAboutView  {

    val ABOUT:Int = 0
    val TERM:Int = 1
    var type = ABOUT
    @Inject
    lateinit var presenter:TermAndAboutPresenter<TermAndAboutView, TermAndAboutInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_and_about)
        type = intent.getIntExtra(ParameterAttribute.ID.nameKey, ABOUT)
        presenter.onAttach(this)
        initInterface()
        presenter.callTermAndAbout()
    }
    private fun initInterface() {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_while);
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }
        if (type == TERM) {
            mToolbar.title = resources.getText(R.string.term)
            return
        }
        mToolbar.title = resources.getText(R.string.about)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun successCallApi() {
        if(type == ABOUT) {
            webView.loadData(presenter.getAbout(), "text/html; charset=UTF-8", "")
            return
        }
        webView.loadData(presenter.getTerm(), "text/html; charset=UTF-8", "")
    }

    override fun failCallApi(message: String) {
        this.showMessageToast(message)
    }
}
