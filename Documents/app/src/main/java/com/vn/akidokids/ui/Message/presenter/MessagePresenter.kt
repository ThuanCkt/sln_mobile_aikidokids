package com.vn.akidokids.ui.Message.presenter

import com.vn.akidokids.data.network.model.MessageTransactionData
import com.vn.akidokids.ui.Message.interactor.MessageInteractor
import com.vn.akidokids.ui.Message.view.MessageView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface MessagePresenter<V:MessageView, I:MessageInteractor>: MVPPresenter<V, I> {

    fun callListHistoryTransaction()
    fun getListMessage():List<MessageTransactionData>
    fun deletHistory(position:Int)
    fun loadMore()
    fun pullToRefresh()
}