package com.vn.akidokids.ui.Message.view

import android.actionsheet.demo.com.khoiron.actionsheetiosforandroid.ActionSheet
import android.actionsheet.demo.com.khoiron.actionsheetiosforandroid.Interface.ActionSheetCallBack
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.vn.akidokids.R
import com.vn.akidokids.ui.Message.interactor.MessageInteractor
import com.vn.akidokids.ui.Message.presenter.MessagePresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_history_point.*
import kotlinx.android.synthetic.main.activity_history_point.btnMore
import kotlinx.android.synthetic.main.activity_history_point.mToolbar
import kotlinx.android.synthetic.main.activity_message.*
import kotlinx.android.synthetic.main.activity_message.swipeRefreshLayout
import javax.inject.Inject

class MessageActivity : BaseActivity(), MessageView, MessageAdapter.MessageAdapterAdapterInterface,
    SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var adapter: MessageAdapter

    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var presenter: MessagePresenter<MessageView, MessageInteractor>

    var isLoading: Boolean = false
    var isPullToRefresh: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        setContentView(R.layout.activity_message)
        initInterface()
        presenter.callListHistoryTransaction()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    fun initInterface() {
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        rcMessages.adapter = adapter
        rcMessages.itemAnimator = DefaultItemAnimator()
        rcMessages.layoutManager = linearLayoutManager

        rcMessages?.addOnScrollListener(object :
            PaginationScrollListener(this.linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return false
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                if (isPullToRefresh) {
                    return
                }

                isLoading = true
                presenter.loadMore()
            }
        })

        btnMore.setOnClickListener {
            val data by lazy { ArrayList<String>() }

            data.add(getString(R.string.delete_all))

            ActionSheet(this, data)
                .setTitle(getString(R.string.title_message_option_select))
                .setCancelTitle(getString(R.string.cancel))
                .setColorTitleCancel(resources.getColor(R.color.color_cancel_sheet))
                .setColorTitle(resources.getColor(R.color.color_title_sheet))
                .setColorData(resources.getColor(R.color.color_item_sheet))
                .create(object : ActionSheetCallBack {
                    override fun data(data: String, position: Int) {
                        if (getString(R.string.delete_all).equals(data)) {
                            presenter.deletHistory(-1)
                        }
                    }
                })
        }

        swipeRefreshLayout.setColorSchemeColors(resources.getColor(R.color.color_primary))
        swipeRefreshLayout.setOnRefreshListener(this)
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    //HistoryPoinAdapterInterface
    override fun onClickItem(index: Int) {

        val data by lazy { ArrayList<String>() }

        data.add(getString(R.string.delete))

        ActionSheet(this, data)
            .setTitle(getString(R.string.title_message_option_select))
            .setCancelTitle(getString(R.string.cancel))
            .setColorTitleCancel(resources.getColor(R.color.color_cancel_sheet))
            .setColorTitle(resources.getColor(R.color.color_title_sheet))
            .setColorData(resources.getColor(R.color.color_item_sheet))
            .create(object : ActionSheetCallBack {
                override fun data(data: String, position: Int) {
                    if (getString(R.string.delete).equals(data)) {
                        presenter.deletHistory(index)
                    }
                }
            })
    }

    //delegate view

    override fun successCallApi( isLoadMore: Boolean) {
        this.isLoading = !isLoadMore
        adapter.setListHistory(
            listHistory = presenter.getListMessage(),
            isLoading = isLoadMore
        )
        if(this.isPullToRefresh) {
            isPullToRefresh = false
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun failCallApi(message: String) {
        if (isPullToRefresh) {
            isPullToRefresh = false
            swipeRefreshLayout.isRefreshing = false
        }
        swipeRefreshLayout.isRefreshing = false
        this.showMessageToast(message)
    }

    override fun deleteSuccess(position: Int) {
        this.adapter.setListHistory(presenter.getListMessage())
        if (presenter.getListMessage().count() <= 4) {
            presenter.pullToRefresh()
        }
    }

    override fun shouldShowMore(shouldShowMore: Boolean) {
        if (!shouldShowMore) {
            this.btnMore.visibility = View.GONE
            return
        }

        this.btnMore.visibility = View.VISIBLE
    }

    override fun onRefresh() {
        if (isPullToRefresh) {
            return
        }

        this.isPullToRefresh = true
        presenter.pullToRefresh()
    }
}
