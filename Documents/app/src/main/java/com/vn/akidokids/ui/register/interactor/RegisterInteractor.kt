package com.vn.akidokids.ui.register.interactor


import com.vn.akidokids.data.network.model.LoginResponse
import com.vn.akidokids.data.network.model.ResultUser
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import com.vn.akidokids.util.AppConstants
import io.reactivex.Observable

interface RegisterInteractor:MVPInteractor {

    fun registerAccount(username:String, password:String, fullname:String): Observable<LoginResponse>

    fun updateUserInSharedPref(userResult: ResultUser, loggedInMode: AppConstants.LoggedInMode)
}