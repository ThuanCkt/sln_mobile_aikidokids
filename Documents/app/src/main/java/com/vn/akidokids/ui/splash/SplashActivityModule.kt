package com.vn.akidokids.ui.splash

import com.vn.akidokids.ui.splash.interactor.SplashInteractor
import com.vn.akidokids.ui.splash.interactor.SplashInteractorImplement
import com.vn.akidokids.ui.splash.presenter.SplashPresenter
import com.vn.akidokids.ui.splash.presenter.SplashPresenterImplement
import com.vn.akidokids.ui.splash.view.SplashView
import dagger.Module
import dagger.Provides

@Module
class SplashActivityModule {

    @Provides
    internal fun provideSplashInteractor(interactor: SplashInteractorImplement):SplashInteractor = interactor

    @Provides
    internal fun provideSplashPresenter(splashPresenter: SplashPresenterImplement<SplashView, SplashInteractor>)
            : SplashPresenter<SplashView, SplashInteractor> = splashPresenter
}