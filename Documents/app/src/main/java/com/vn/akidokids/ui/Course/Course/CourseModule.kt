package com.vn.akidokids.ui.Course.Course

import com.vn.akidokids.ui.Course.Course.interactor.CourseInteractor
import com.vn.akidokids.ui.Course.Course.interactor.CourseInteractorImplement
import com.vn.akidokids.ui.Course.Course.presenter.CoursePresenter
import com.vn.akidokids.ui.Course.Course.presenter.CoursePresenterImplement
import com.vn.akidokids.ui.Course.Course.view.CourseView
import dagger.Module
import dagger.Provides

@Module
class CourseModule {

    @Provides
    internal fun provideInteractor(interactor: CourseInteractorImplement): CourseInteractor = interactor

    @Provides
    internal fun providePresenter(presenter: CoursePresenterImplement<CourseView, CourseInteractor>): CoursePresenter<CourseView, CourseInteractor> = presenter

}