package com.vn.akidokids.ui.ForgotPassword

import com.vn.akidokids.ui.ForgotPassword.view.ForgotPasswordFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ForgotPasswordProvider {
    @ContributesAndroidInjector(modules = [ForgotPasswordModule::class])
    internal abstract fun providerForgotPasswordFragment() : ForgotPasswordFragment
}