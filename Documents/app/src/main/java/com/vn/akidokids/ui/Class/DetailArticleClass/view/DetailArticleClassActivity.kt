package com.vn.akidokids.ui.Class.DetailArticleClass.view

import android.os.Bundle
import com.vn.akidokids.R
import com.vn.akidokids.ui.Class.DetailArticleClass.interactor.DetailArticleClassInteractor
import com.vn.akidokids.ui.Class.DetailArticleClass.presenter.DetailArticlePresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.activity_detail_acticle.*
import javax.inject.Inject

class DetailArticleClassActivity : BaseActivity(), DetailArticleView {

    @Inject
    lateinit var presenter: DetailArticlePresenter<DetailArticleView, DetailArticleClassInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_article_class)
        presenter.onAttach(this)
        this.initInterface()
        val idArticle: Long = intent.getLongExtra(ParameterAttribute.ID.nameKey, 0.toLong())
        presenter.callApiDetailArticle(idArticle)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }


    private fun initInterface() {
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun successCallApi() {
        lblTitle.text = presenter.getTitle()
        lbDateDisplay.text = presenter.getDateCreate()
        webView.loadData(presenter.getHtml(), "text/html; charset=UTF-8", "")
    }

    override fun failCallApi(message: String) {
        this.showMessageToast(message)
    }

}
