package com.vn.akidokids.ui.main.Category.interactor

import com.vn.akidokids.data.network.model.ArticleCategoryResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface CategoryInteractor: MVPInteractor {

    fun getListCategory(cid:Int, page:Int, pageSize:Int):Observable<ArticleCategoryResponse>
}