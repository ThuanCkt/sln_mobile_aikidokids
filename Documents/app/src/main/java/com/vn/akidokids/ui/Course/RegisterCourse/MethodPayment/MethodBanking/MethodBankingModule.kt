package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.interactor.MethodBankingInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.interactor.MethodBankingInteractorImplement
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.presenter.MethodBankingPresenter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.presenter.MethodBankingPresenterImplement
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingAdapter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingFragment
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingView
import dagger.Module
import dagger.Provides

@Module
class MethodBankingModule {

    @Provides
    internal fun provideInteractor(interactor: MethodBankingInteractorImplement):MethodBankingInteractor = interactor

    @Provides
    internal fun providePresenter(presenter:MethodBankingPresenterImplement<MethodBankingView, MethodBankingInteractor>):MethodBankingPresenter<MethodBankingView, MethodBankingInteractor> = presenter

    @Provides
    internal fun provideLayoutManager(fragment: MethodBankingFragment) = LinearLayoutManager(fragment.context)

    @Provides
    internal fun provideAdapter():MethodBankingAdapter = MethodBankingAdapter()
}