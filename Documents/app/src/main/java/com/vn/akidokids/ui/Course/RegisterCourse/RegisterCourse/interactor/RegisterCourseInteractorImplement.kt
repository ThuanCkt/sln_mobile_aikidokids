package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.BookingCourseResponse
import com.vn.akidokids.data.network.model.DetailCourseResponse
import com.vn.akidokids.data.network.model.QuantityStudentResponse
import com.vn.akidokids.data.network.model.UserReferResponse
import com.vn.akidokids.data.network.request.CourseRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class RegisterCourseInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    RegisterCourseInteractor {

    override fun getDetailCourse(id:Long): Observable<DetailCourseResponse> {
        return apiHelper.performServerDetailCourse(CourseRequest.DetailCourseRequest(id))
    }

    override fun getPointThroughtQuantityStudent(
        idCourse: Long,
        quantity: String
    ): Observable<QuantityStudentResponse> {
        return apiHelper.performServerQuantityStudent(CourseRequest.QuantityStudentRequest(idCourse, quantity))
    }

    override fun getReferUser(idUser: String): Observable<UserReferResponse> {
        return apiHelper.performServerReferUser(CourseRequest.ReferUserRequest(idUser = idUser))
    }

    override fun registerCourse(params: CourseRequest.BookingCourse): Observable<BookingCourseResponse> {
        return apiHelper.performServerRegistCourse(params)
    }
}