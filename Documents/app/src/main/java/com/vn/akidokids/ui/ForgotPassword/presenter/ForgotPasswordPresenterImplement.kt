package com.vn.akidokids.ui.ForgotPassword.presenter

import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.ui.ForgotPassword.interactor.ForgotPasswordInteractor
import com.vn.akidokids.ui.ForgotPassword.view.ForgotPasswordView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ForgotPasswordPresenterImplement<V : ForgotPasswordView, I : ForgotPasswordInteractor>
@Inject constructor(interactor: I, provider: SchedulerProvider, disposable: CompositeDisposable) :
    BasePresenter<V, I>(interactor, provider, disposable), ForgotPasswordPresenter<V, I> {

    override fun forgotPassword(email: String, note: String) {
        when {
            email.isEmpty() -> this.getView()?.showValidMessage(
                AikidoKidsApplication.getAppResources().getString(
                    R.string.username_not_empty
                )
            )
            else -> {

                interactor?.let {
                    this.getView()?.showProgress()
                    it.forgotPassword(email, note)
                        .compose(schedulerProvider.ioToMainObservableScheduler())
                        .subscribe({ responseForgot ->
                            this.getView()?.hideProgress()
                            if (responseForgot.result == null) {
                                getView()?.showValidMessage(
                                    AikidoKidsApplication.getAppResources().getString(
                                        R.string.please_check_connect_internet
                                    )
                                )
                                return@subscribe
                            }

                            this.getView()?.showValidMessage(responseForgot.result?.msg ?: "")
                        }, { err ->
                            println(err)
                            this.getView()?.hideProgress()
                        })
                }
            }
        }
    }

}