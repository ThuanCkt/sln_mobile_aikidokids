package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.vn.akidokids.R
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentDialog
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor.RegisterCourseInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.presenter.RegisterCoursePresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.MethodPayment
import com.vn.akidokids.util.ParameterAttribute
import com.vn.akidokids.util.extension.hideKeyboard
import com.vn.akidokids.util.extension.loadImageVector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_register_cause.*
import javax.inject.Inject

class RegisterCourseActivity : BaseActivity(), RegisterCourseView, HasAndroidInjector, AdapterView.OnItemSelectedListener {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var presenter: RegisterCoursePresenter<RegisterCourseView, RegisterCourseInteractor>

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    var idCourse:Long = 0
    var method:MethodPayment = MethodPayment.TRANFER_BANK

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        setContentView(com.vn.akidokids.R.layout.activity_register_cause)
        this.initInterface()
        this.setUpClick()
    }

    private fun initInterface() {
        idCourse = intent.getLongExtra(ParameterAttribute.ID.nameKey, 0)
        presenter.callDetaiCourse(id = idCourse)

        val account = preferenceHelper.getUserEntity()
        lbIdAccount.text = account.username.toString()
        lbFullName.text = account.fullname
        lbGmail.text = account.email
        lbPhone.text = account.phoneNumber
        lblSex.text = account.sextext
    }

    private fun setUpClick() {
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        btnResearch.setOnClickListener {
            this.hideKeyboard()
            MethodPaymentDialog.newInstance()?.let{
                it?.show(supportFragmentManager, "Method Payment")
            }
        }

        btnTranferBank.setOnClickListener{
            this.hideKeyboard()
            method = MethodPayment.TRANFER_BANK
            this.setIntefaceTranfer()
        }

        btnDownPayment.setOnClickListener {
            this.hideKeyboard()
            method = MethodPayment.DOWN_PAYMENT
            this.setIntefaceTranfer()
        }

        btnPoint.setOnClickListener {
            this.hideKeyboard()
            method = MethodPayment.POINT
            this.setIntefaceTranfer()
        }

        btnBooking.setOnClickListener {
            this.hideKeyboard()
            this.presenter.registerCourse(this.method)
        }

        btnFindUser.setOnClickListener {
            this.hideKeyboard()
            this.presenter.getReferUser(txtFindIdUser.text.toString())
        }

        spnQuanlityStudent.onItemSelectedListener = this
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector;
    }

   private fun setInterfaceTranferBank() {
        imgTransferBank.loadImageVector(R.drawable.ic_radio_checked)
        imgDownPayment.loadImageVector(R.drawable.ic_radio_unchecked)
        imgPoint.loadImageVector(R.drawable.ic_radio_unchecked)
    }

    private fun setInterfaceDownPayment() {
        imgTransferBank.loadImageVector(R.drawable.ic_radio_unchecked)
        imgDownPayment.loadImageVector(R.drawable.ic_radio_checked)
        imgPoint.loadImageVector(R.drawable.ic_radio_unchecked)
    }

    private fun setInterfaceDownPoint() {
        imgTransferBank.loadImageVector(R.drawable.ic_radio_unchecked)
        imgDownPayment.loadImageVector(R.drawable.ic_radio_unchecked)
        imgPoint.loadImageVector(R.drawable.ic_radio_checked)
    }

    private fun setIntefaceTranfer() {
        when(method) {
            MethodPayment.TRANFER_BANK -> setInterfaceTranferBank()
            MethodPayment.DOWN_PAYMENT -> setInterfaceDownPayment()
            MethodPayment.POINT -> setInterfaceDownPoint()
            else -> setInterfaceTranferBank()
        }
    }

    //delegate spinner

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        presenter.getPointFollowQuantity(idCourse = this.idCourse, position = p2)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    //delegate view
    override fun failCallApi(message: String) {
        this.showMessageToast(message ?: EMPTY_STRING)
//        if(message == "Đăng ký thành công.") {
//            this.finish()
//        }
    }


    override fun successCallApi() {
        lbNameCourse.text = presenter.getNameCourse()
        lbOriginalPrice.text = presenter.getOriginalPrice()
        if (presenter.getDiscountPrice().isNullOrEmpty()) {
            lbDiscount.text = EMPTY_STRING
        } else {
            lbDiscount.text = "Tiết kiệm ${presenter.getDiscountPrice()}"
        }

        lbPriceBuy.text = presenter.getTotalPrice()

        val list = presenter.getListQuanlityStudent()
        if (list.count() <= 1) {
            spnQuanlityStudent.isEnabled = false
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnQuanlityStudent.setAdapter(adapter)
        spnQuanlityStudent.setSelection(presenter.getIndexDefaultSelectQuantity())
    }

    override fun setTextGivePoint(string: String) {
        lbPoint.text = string
    }

    override fun setDiscountText(string: String) {
        lbDiscount.text = string
    }

    override fun setTotaltext(string: String) {
        lbPriceBuy.text = string
    }

    override fun setOriginalText(string: String) {
        lbOriginalPrice.text = string
    }

    override fun setTextUserRefer(string: String) {
        lbFullNameRefer.visibility = View.VISIBLE
        lbFullNameRefer.text = string
    }
}
