package com.vn.akidokids.ui.ChangeInformation.interactor

import com.vn.akidokids.data.network.model.ProvinceResponse
import com.vn.akidokids.data.network.model.ResponseUserId
import com.vn.akidokids.data.network.model.UpdateAvatarReponse
import com.vn.akidokids.data.network.request.UserRequest
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface ChangeInformationInteractor:MVPInteractor {
    fun getUserInfor(idUser:UserRequest.UserIdRequest):Observable<ResponseUserId>
    fun getProvince():Observable<ProvinceResponse>
    fun changeUserInfor(data:UserRequest.ChangeInforRequest):Observable<ResponseUserId>
    fun setDataCamera(link: String)
    fun changeAvatar(avatarBase64:String):Observable<UpdateAvatarReponse>
}