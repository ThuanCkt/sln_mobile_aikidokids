package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MethodPayment
import com.vn.akidokids.ui.base.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_method_down_payment.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class MethodDownPaymentFragment : BaseFragment(), MethodDownPaymentView {

    @Inject
    internal lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    internal lateinit var adapter: MethodDownPaymentAdapter

    private var downPayments:List<MethodPayment> = arrayListOf()

    fun setListDownPayment(downPayments: List<MethodPayment>) {
        this.downPayments = downPayments
        adapter.setListDownPayment(this.downPayments)
    }

    companion object {
        fun newInstance(): MethodDownPaymentFragment =
            MethodDownPaymentFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_method_down_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.setListDownPayment(this.downPayments)
    }

    override fun setUp() {
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rcMethodDownPayment.itemAnimator = DefaultItemAnimator()
        rcMethodDownPayment.layoutManager = linearLayoutManager
        rcMethodDownPayment.adapter = adapter
    }

}
