package com.vn.akidokids.util.extension

import java.text.DecimalFormat

fun Long.convertToStringComma():String {
    val formatterComma = DecimalFormat("#,###")
    return formatterComma.format(this)
}