package com.vn.akidokids.ui.ChangeInformation

import com.vn.akidokids.ui.ChangeInformation.interactor.ChangeInformationInteractor
import com.vn.akidokids.ui.ChangeInformation.interactor.ChangeInformationInteractorImplement
import com.vn.akidokids.ui.ChangeInformation.presenter.ChangeInformationPresenter
import com.vn.akidokids.ui.ChangeInformation.presenter.ChangeInformationPresenterImplement
import com.vn.akidokids.ui.ChangeInformation.view.ChangeInformationView
import dagger.Module
import dagger.Provides

@Module
class ChangeInformationModule {

    @Provides
    fun provideInteractor(interactor: ChangeInformationInteractorImplement): ChangeInformationInteractor =
        interactor

    @Provides
    fun providePresenter(presenter: ChangeInformationPresenterImplement<ChangeInformationView, ChangeInformationInteractor>): ChangeInformationPresenter<ChangeInformationView, ChangeInformationInteractor> =
        presenter
}