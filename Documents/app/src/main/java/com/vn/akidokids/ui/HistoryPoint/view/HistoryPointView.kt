package com.vn.akidokids.ui.HistoryPoint.view

import com.vn.akidokids.ui.base.view.MVPView

interface HistoryPointView:MVPView {
    fun failCallApi(message:String)
    fun successGetListHistoryData(isLoadMore:Boolean)
    fun shouldShowMore(shouldShowMore:Boolean)
    fun deleteSuccess()
}