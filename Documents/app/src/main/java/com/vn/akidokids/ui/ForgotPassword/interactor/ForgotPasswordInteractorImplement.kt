package com.vn.akidokids.ui.ForgotPassword.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.ForgotPasswordResponse
import com.vn.akidokids.data.network.request.ForgotPasswordRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ForgotPasswordInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) :
    BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    ForgotPasswordInteractor {

    override fun forgotPassword(email: String, note: String): Observable<ForgotPasswordResponse> {
        return apiHelper.performServerForgotPassword(
            request = ForgotPasswordRequest(
                email = email,
                note = note
            )
        )
    }
}