package com.vn.akidokids.ui.ChangeInformation.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.ProvinceResponse
import com.vn.akidokids.data.network.model.ResponseUserId
import com.vn.akidokids.data.network.model.UpdateAvatarReponse
import com.vn.akidokids.data.network.request.UserRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import com.vn.akidokids.util.Utils
import io.reactivex.Observable
import javax.inject.Inject

class ChangeInformationInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), ChangeInformationInteractor {

    override fun getUserInfor(idUser: UserRequest.UserIdRequest): Observable<ResponseUserId> {
        return apiHelper.performServerUserInforById(idUser)
    }

    override fun getProvince(): Observable<ProvinceResponse> {
        return apiHelper.performServerProvince()
    }

    override fun changeUserInfor(data: UserRequest.ChangeInforRequest): Observable<ResponseUserId> {
        return apiHelper.performServerUserInforUpdate(data)
    }

    override fun setDataCamera(link: String) {
        preferenceHelper.setLinkImage(link)
    }

    override fun changeAvatar(avatarBase64: String): Observable<UpdateAvatarReponse> {
        return apiHelper.performServerUserAvatarUpdate(UserRequest.ChangeAvartarRequest(Utils.getUserId(), avatarBase64))
    }
}