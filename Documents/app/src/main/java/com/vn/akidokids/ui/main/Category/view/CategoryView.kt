package com.vn.akidokids.ui.main.Category.view

import com.vn.akidokids.data.network.model.DataArticleCategory
import com.vn.akidokids.ui.base.view.MVPView

interface CategoryView : MVPView {

    fun showMessageError(error:String)

    fun successGetListArticleCategory(listArticle:List<DataArticleCategory>?, isLoadMore:Boolean)
}