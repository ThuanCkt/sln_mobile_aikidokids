package com.vn.akidokids.ui.Class.ClassActivity.presenter

import com.vn.akidokids.data.network.model.MemberInClass
import com.vn.akidokids.ui.Class.ClassActivity.interactor.ClassInteractor
import com.vn.akidokids.ui.Class.ClassActivity.view.ClassView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface ClassPresenter<V:ClassView, I:ClassInteractor>:MVPPresenter<V,I> {
    fun getInforClass(id:Long)
    fun getNameClass():String
    fun getThumnailClass():String
    fun getSizeClass():String
    fun getListMember():List<MemberInClass>
}