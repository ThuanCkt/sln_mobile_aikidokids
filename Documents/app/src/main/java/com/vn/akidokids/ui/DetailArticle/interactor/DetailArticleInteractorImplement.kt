package com.vn.akidokids.ui.DetailArticle.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.DetailArticleResponse
import com.vn.akidokids.data.network.request.CategoryRequest
import com.vn.akidokids.data.preferences.AppPreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class DetailArticleInteractorImplement @Inject constructor(
    preferenceHelper: AppPreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    DetailArticleInteractor {

    override fun getDetailArticle(id: Long): Observable<DetailArticleResponse> {
        return apiHelper.performServerDetailArticle(CategoryRequest.ServerDetailArticle(id))
    }
}