package com.vn.akidokids.ui.ChangePassword.presenter

import com.vn.akidokids.ui.ChangePassword.interactor.ChangePasswordInteractor
import com.vn.akidokids.ui.ChangePassword.view.ChangePasswordView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface ChangePasswordPresenter<V : ChangePasswordView, I : ChangePasswordInteractor> :
    MVPPresenter<V, I> {
    fun callApiChangePassword(oldPassword:String, newPassword:String, reNewPassword:String)
}