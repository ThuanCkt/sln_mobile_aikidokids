package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor

import com.vn.akidokids.data.network.model.BookingCourseResponse
import com.vn.akidokids.data.network.model.DetailCourseResponse
import com.vn.akidokids.data.network.model.QuantityStudentResponse
import com.vn.akidokids.data.network.model.UserReferResponse
import com.vn.akidokids.data.network.request.CourseRequest
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface RegisterCourseInteractor: MVPInteractor {
    fun getDetailCourse(id:Long): Observable<DetailCourseResponse>
    fun getPointThroughtQuantityStudent(idCourse:Long, quantity:String):Observable<QuantityStudentResponse>
    fun getReferUser(idUser:String):Observable<UserReferResponse>
    fun registerCourse(params: CourseRequest.BookingCourse):Observable<BookingCourseResponse>
}