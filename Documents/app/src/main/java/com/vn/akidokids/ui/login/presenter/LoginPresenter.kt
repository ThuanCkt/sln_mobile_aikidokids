package com.vn.akidokids.ui.login.presenter

import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.ui.login.interactor.LoginInteractor
import com.vn.akidokids.ui.login.view.LoginView

interface LoginPresenter<V: LoginView, I:LoginInteractor>:MVPPresenter<V,I> {
    fun onServerLoginClicked(usename: String, password: String, isRemember:Boolean)
}