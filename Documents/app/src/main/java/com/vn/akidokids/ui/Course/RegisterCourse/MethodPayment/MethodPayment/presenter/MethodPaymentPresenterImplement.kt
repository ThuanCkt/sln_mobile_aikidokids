package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MethodPayment
import com.vn.akidokids.data.network.model.ResultMethodPayment
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor.MethodPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MethodPaymentPresenterImplement<V : MethodPaymentView, I : MethodPaymentInteractor>
@Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = interactor,
        schedulerProvider = schedulerProvider,
        compositeDisposable = disposable
    ),
    MethodPaymentPresenter<V, I> {

    private var resultMethodPayment: ResultMethodPayment? = null

    override fun getMethodPayment() {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getMethodPayment().compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        this.getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.resultMethodPayment = response.result
                        this.getView()?.successCallApi()

                    },
                    { err ->
                        this.getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                    }
                )
            )
        }
    }

    override fun getListBank(): List<MethodPayment> {
        if (resultMethodPayment == null) {
            return arrayListOf()
        }

        return resultMethodPayment?.banks ?: arrayListOf()
    }

    override fun getListDownPayment(): List<MethodPayment> {
        if (resultMethodPayment == null) {
            return arrayListOf()
        }

        return resultMethodPayment?.organizations ?: arrayListOf()
    }

    override fun getNotePoint(): String {
        if (resultMethodPayment == null) {
            return EMPTY_STRING
        }

        return resultMethodPayment?.pointNote ?: EMPTY_STRING
    }
}