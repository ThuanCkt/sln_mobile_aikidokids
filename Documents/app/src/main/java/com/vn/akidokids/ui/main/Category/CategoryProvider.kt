package com.vn.akidokids.ui.main.Category

import com.vn.akidokids.ui.main.Category.view.CategoryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CategoryProvider {
    @ContributesAndroidInjector(modules = [CategoryModule::class])
    internal abstract fun provideRegisterFragment() : CategoryFragment
}