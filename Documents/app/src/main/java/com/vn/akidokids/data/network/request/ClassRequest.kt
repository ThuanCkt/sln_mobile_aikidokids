package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClassRequest {
    data class ClassInforRequest(
        @Expose
        @SerializedName("idClass")
        var idClass:Long,

        @Expose
        @SerializedName("page")
        var page:Long,
        @Expose
        @SerializedName("size")
        var size:Long
    )

    data class ArticleClassRequest(
        @Expose
        @SerializedName("cid")
        var idClass:Long,

        @Expose
        @SerializedName("page")
        var page:Long,

        @Expose
        @SerializedName("size")
        var size:Long
    )

    data class GalleyClassRequest(
        @Expose
        @SerializedName("cid")
        var idClass:Long
    )

    data class DetailClassRequest(
        @Expose
        @SerializedName("id")
        var idArticle:Long
    )
}