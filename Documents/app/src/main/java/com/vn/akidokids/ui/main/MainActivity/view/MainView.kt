package com.vn.akidokids.ui.main.MainActivity.view

import com.vn.akidokids.ui.base.view.MVPView

interface MainView:MVPView {

    fun openLoginAndRegisterActivity(isLogin:Boolean)

    fun refeshDataCategory()
    fun getSuccesUserInfor()
    fun showFragmentLogout()
}