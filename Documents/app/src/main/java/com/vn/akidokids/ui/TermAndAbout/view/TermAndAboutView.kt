package com.vn.akidokids.ui.TermAndAbout.view

import com.vn.akidokids.ui.base.view.MVPView

interface TermAndAboutView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi()
}