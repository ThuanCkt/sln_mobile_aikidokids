package com.vn.akidokids.ui.Class.ArticleClass.interactor

import com.vn.akidokids.data.network.model.PostArticleClassResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface ArticleClassInteractor:MVPInteractor {

    fun getListArticleClass(idClass:Long, page:Long, pageSize:Long):Observable<PostArticleClassResponse>
}