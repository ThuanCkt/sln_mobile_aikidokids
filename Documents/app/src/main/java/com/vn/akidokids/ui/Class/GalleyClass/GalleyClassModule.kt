package com.vn.akidokids.ui.Course.LessonCourse

import androidx.recyclerview.widget.GridLayoutManager
import com.vn.akidokids.ui.Class.GalleyClass.interactor.GalleyClassInteractor
import com.vn.akidokids.ui.Class.GalleyClass.interactor.GalleyClassInteractorImplement
import com.vn.akidokids.ui.Class.GalleyClass.presenter.GalleyClassPresenter
import com.vn.akidokids.ui.Class.GalleyClass.presenter.GalleyClassPresenterImplement
import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassAdapter
import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassFragment
import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassView
import dagger.Module
import dagger.Provides

@Module
class GalleyClassModule {
    @Provides
    internal fun provideAdapter(fragment: GalleyClassFragment): GalleyClassAdapter = GalleyClassAdapter(fragment)

    @Provides
    internal fun provideGridLayoutManager(fragment: GalleyClassFragment): GridLayoutManager =
        GridLayoutManager(fragment.context, 3)

    @Provides
    internal fun provideInteractor(interactor: GalleyClassInteractorImplement): GalleyClassInteractor =
        interactor

    @Provides
    internal fun providePresenter(presneter: GalleyClassPresenterImplement<GalleyClassView, GalleyClassInteractor>): GalleyClassPresenter<GalleyClassView, GalleyClassInteractor> =
        presneter

}