package com.vn.akidokids.ui.Class.ClassActivity.interactor

import com.vn.akidokids.data.network.model.ClassInforResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface ClassInteractor:MVPInteractor {

    fun getInforClass(id:Long):Observable<ClassInforResponse>
}