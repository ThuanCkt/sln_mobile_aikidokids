package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ArticleCategoryResponse (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String? = null,

    @Expose
    @SerializedName("result")
    var result: ResultArticleCategory? = null
)

data class ResultArticleCategory (
    @Expose
    @SerializedName("page")
    var page: Page? = null,

    @Expose
    @SerializedName("data")
    var data: List<DataArticleCategory>? = null,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null
)

data class DataArticleCategory (
    @Expose
    @SerializedName("id")
    var id: Long,

    @Expose
    @SerializedName("gid")
    var gid: Long,

    @Expose
    @SerializedName("title")
    var title: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("img")
    var img: String? = null,

    @Expose
    @SerializedName("img_thumbnail")
    var imgThumbnail: String? = null,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("contents_html")
    var contentsHTML: String? = null,

    @Expose
    @SerializedName("contents")
    var contents: String? = null,

    @Expose
    @SerializedName("acc_signature")
    var accSignature: Any? = null,

    @Expose
    @SerializedName("slogan")
    var slogan: Any? = null,

    @Expose
    @SerializedName("is_hot")
    var isHot: Boolean,

    @Expose
    @SerializedName("is_cool")
    var isCool: Boolean,

    @Expose
    @SerializedName("is_popular")
    var isPopular: Boolean,

    @Expose
    @SerializedName("cate_name")
    var cateName: String? = null,

    @Expose
    @SerializedName("cate_slug")
    var cateSlug: String? = null,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String? = null,

    @Expose
    @SerializedName("created_by")
    var createdBy: String? = null,

    @Expose
    @SerializedName("created_log")
    var createdLog: String? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    var mode: String? = null,

    @Expose
    @SerializedName("user_name")
    var userName: String? = null,

    @Expose
    @SerializedName("modeaction")
    var modeaction: Long,

    @Expose
    @SerializedName("langid")
    var langid: Long,

    @Expose
    @SerializedName("langcode")
    var langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    var langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    var lstlang: Any? = null
)


data class Page (
    @Expose
    @SerializedName("searchexec")
    var searchexec: Any? = null,

    @Expose
    @SerializedName("totalcount")
    var totalcount: Long,

    @Expose
    @SerializedName("resultcount")
    var resultcount: Long,

    @Expose
    @SerializedName("totalpage")
    var totalpage: Long,

    @Expose
    @SerializedName("currentpage")
    var currentpage: Long,

    @Expose
    @SerializedName("v")
    var pagesize: Long,

    @Expose
    @SerializedName("pageinfo")
    var pageinfo: String,

    @Expose
    @SerializedName("offset")
    var offset: Long,

    @Expose
    @SerializedName("offsetactual")
    var offsetactual: Long,

    @Expose
    @SerializedName("offsetend")
    var offsetend: Long,

    @Expose
    @SerializedName("isloadmore")
    var isloadmore: Boolean,

    @Expose
    @SerializedName("startcount")
    var startcount: Long,

    @Expose
    @SerializedName("endcount")
    var endcount: Long
)
