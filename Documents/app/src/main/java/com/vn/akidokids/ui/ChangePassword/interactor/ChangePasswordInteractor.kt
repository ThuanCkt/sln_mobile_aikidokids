package com.vn.akidokids.ui.ChangePassword.interactor

import com.vn.akidokids.data.network.model.LoginResponse
import com.vn.akidokids.data.network.request.UserRequest
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface ChangePasswordInteractor:MVPInteractor {

    fun callChangePassword(data:UserRequest.ChangePasswordRequest):Observable<LoginResponse>
    fun setToken(token:String)
}