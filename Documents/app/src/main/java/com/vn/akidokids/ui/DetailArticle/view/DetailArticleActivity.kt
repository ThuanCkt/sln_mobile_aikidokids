package com.vn.akidokids.ui.DetailArticle.view

import android.os.Bundle
import android.webkit.WebViewClient
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.ui.DetailArticle.interactor.DetailArticleInteractor
import com.vn.akidokids.ui.DetailArticle.presenter.DetailArticlePresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.activity_detail_acticle.*
import javax.inject.Inject

class DetailArticleActivity : BaseActivity(), DetailArticleView, ArticleRelateAdapter.ArticleRelateAdapterInteface {

    @Inject
    lateinit var presenter: DetailArticlePresenter<DetailArticleView, DetailArticleInteractor>

    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var adapter: ArticleRelateAdapter

    var pId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_acticle)
        this.pId = intent.getLongExtra(ParameterAttribute.ID.nameKey, 0);
        initInterface()
        presenter.onAttach(this)
        presenter.getDetailArticle(this.pId)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    private fun initInterface() {
        rcvArticleRelate.layoutManager = linearLayoutManager
        rcvArticleRelate.itemAnimator = DefaultItemAnimator()
        rcvArticleRelate.adapter = adapter

        mToolbar.setNavigationOnClickListener {
            this.finish()
        }
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    //delegate view
    override fun failCallApi(message: String) {
        this.showMessageToast(message ?: EMPTY_STRING)
    }

    override fun successCallApi() {
        webView.webViewClient = WebViewClient()
        webView.loadData(presenter.getContentHtml(),"text/html", "UTF-8")
        adapter.setListArticleRelate(presenter.getListRelateArticle() ?: arrayListOf())
        lblTitle.text = presenter.getTitleArticle()
        lbDateDisplay.text = presenter.getTextDateCreateArticle()
    }

    //ArticleRelateAdapterInteface
    override fun clickItem(id: Long) {
        this.pId = id
        presenter.getDetailArticle(this.pId)
    }
}
