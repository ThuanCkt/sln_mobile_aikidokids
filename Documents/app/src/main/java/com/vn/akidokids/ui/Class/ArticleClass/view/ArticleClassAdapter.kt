package com.vn.akidokids.ui.Course.LessonCourse.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ClassPostsData
import com.vn.akidokids.util.FORMAT_DATE_APEARANCE
import com.vn.akidokids.util.FORMAT_DATE_API
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.loadImage
import com.vn.akidokids.util.extension.toDateString
import kotlinx.android.synthetic.main.item_lesson_cause.view.*

class ArticleClassAdapter(private val delegate: ArticleClassAdapter.ArticleClassAdapterInterface) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val VIEW_TYPE_ITEM = 0
    val VIEW_TYPE_LOADING = 1

    var isLoading: Boolean = false

    var listArticleClass: List<ClassPostsData> = arrayListOf()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (this.getItemViewType(position)) {
            VIEW_TYPE_ITEM -> {
                val holder = holder as? ArticleClassAdapter.LessonCourseHolder
                holder?.let {
                    it.bindData(position)
                }
            }

            VIEW_TYPE_LOADING -> {
                val menuHolder = holder as? LoadingViewHolder
            }

            else -> return
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            return LessonCourseHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_lesson_cause,
                    parent,
                    false
                )
            )
        }

        return LoadingViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_loading, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        if (position >= listArticleClass.count()) {
            return VIEW_TYPE_LOADING
        }

        return VIEW_TYPE_ITEM
    }

    fun setListArticle(listArticleClass: List<ClassPostsData>, isLoading:Boolean) {
        this.listArticleClass = listArticleClass
        this.isLoading = isLoading
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        if (isLoading) {
            return listArticleClass.count()+ 1
        }
        return listArticleClass.count()
    }

    inner class LessonCourseHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindData(position: Int) {
            listArticleClass[position].createdDate?.let {
                val date = it.convertStringToDate(FORMAT_DATE_API)
                itemView.lbDate.text = date?.toDateString(FORMAT_DATE_APEARANCE)
            }

            listArticleClass[position].img?.let {
                itemView.imgThumbnail.loadImage(it)
            }

            listArticleClass[position].title?.let {
                itemView.lbTitle.text = it
            }

            itemView.setOnClickListener {
                delegate.articleOnClick(listArticleClass[position].groupID)
            }
        }
    }

    inner class LoadingViewHolder(view:View):RecyclerView.ViewHolder(view) {

    }

    interface ArticleClassAdapterInterface {
        fun articleOnClick(id: Long)
    }
}