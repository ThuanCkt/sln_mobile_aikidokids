package com.vn.akidokids.ui.main.MainActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.model.MenuItem
import com.vn.akidokids.data.model.MenuSection
import com.vn.akidokids.ui.main.MainActivity.view.InterfaceMenu
import kotlinx.android.synthetic.main.item_menu.view.*
import kotlinx.android.synthetic.main.item_menu_section.view.*

class MenuAdapter(private val listMenu:MutableList<Any>, private val delegate:InterfaceMenu):RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view: View? = null
        var viewHolder: RecyclerView.ViewHolder? = null
        if (viewType == TypeMenu.SECTION.type) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_section,parent,false);
            viewHolder =  SectionMenuHolder(view);
        }

        if (viewType == TypeMenu.MENU.type) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu,parent,false);
            viewHolder =  MenuHolder(view);
        }

        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return listMenu.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (this.getItemViewType(position)) {
            TypeMenu.SECTION.type -> {
                val holderSection = holder as? SectionMenuHolder
                holderSection?.let {
                    holderSection.clear()
                    holderSection.onBind(position)
                }
            }

            TypeMenu.MENU.type -> {
                val menuHolder = holder as? MenuHolder
                menuHolder?.let {
                    menuHolder.clear()
                    menuHolder.onBind(position)
                }
            }

            else -> return
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (this.listMenu[position] is MenuSection) {
            return TypeMenu.SECTION.type
        }
        return TypeMenu.MENU.type
    }

    internal fun addMenuToList(menus: List<Any>) {
        this.listMenu.clear()
        this.listMenu.addAll(menus)
        notifyDataSetChanged()
    }

    inner class MenuHolder(view:View):RecyclerView.ViewHolder(view) {
        fun clear() {
            itemView.icMenu.setImageDrawable(null)
            itemView.lbMenu.text = ""
        }

        fun onBind(position:Int) {
            val sectionMenu: MenuItem? = listMenu[position] as? MenuItem
            sectionMenu?.let {
                itemView.icMenu.setImageResource(it.iconId ?: 0)
                itemView.lbMenu.text = it.name
            }

            itemView.setOnClickListener {
                delegate.clickMenu(listMenu[position])
            }
        }
    }

    inner class SectionMenuHolder(view:View):RecyclerView.ViewHolder(view) {
        fun clear() {
            itemView.icSectionMenu.setImageDrawable(null)
            itemView.lbSectionMenu.text = ""
        }

        fun onBind(position:Int) {
            val sectionMenu: MenuSection? = listMenu[position] as? MenuSection
            sectionMenu?.let {
                itemView.icSectionMenu.setImageResource(it.iconId ?: 0)
                itemView.lbSectionMenu.text = it.name
            }


        }
    }
}

enum class TypeMenu(val type:Int) {
    SECTION(0),
    MENU(1)
}