package com.vn.akidokids.ui.Message.view

import com.vn.akidokids.ui.base.view.MVPView

interface MessageView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi(isLoadMore:Boolean)
    fun shouldShowMore(shouldShowMore:Boolean)
    fun deleteSuccess(position:Int)
}