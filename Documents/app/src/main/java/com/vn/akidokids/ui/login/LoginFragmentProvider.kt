package com.vn.akidokids.ui.login

import com.vn.akidokids.ui.login.view.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginFragmentProvider {
    @ContributesAndroidInjector(modules = [LoginModule::class])
    abstract internal fun provideLoginFragment(): LoginFragment
}