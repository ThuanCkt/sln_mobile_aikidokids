package com.vn.akidokids.ui.login.view

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vn.akidokids.R
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterActivity
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.ui.login.interactor.LoginInteractor
import com.vn.akidokids.ui.login.presenter.LoginPresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.EventBus
import com.vn.akidokids.util.TypeEvenBus
import com.vn.akidokids.util.extension.hideKeyboard
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginView {

    @Inject
    internal lateinit var presenter: LoginPresenter<LoginView, LoginInteractor>

    var isShowPassword:Boolean = false
    var isRememberPassword:Boolean = false

    companion object {

        internal val TAG = "LoginFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        this.presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)

    }

    override fun setUp() {
        this.setOnClick()
    }

    fun setOnClick() {

        lnRegister.setOnClickListener {
            this.goToRegister()
        }

        btnShowPassword.setOnClickListener {
            showPassOrHidePassword()
        }

        btnForgotPassword.setOnClickListener {
            this.goToForgotPassword()
        }

        btnLogin.setOnClickListener {
            this.hideKeyboard()
            this.presenter.onServerLoginClicked(txtUsername.text.toString(), txtPassword.text.toString(),isRemember = this.isRememberPassword)
        }

        lnRememberPassword.setOnClickListener {
            this.hideKeyboard()
            this.rememberPassword()
        }

        btnBack.setOnClickListener {
            if(LoginAndRegisterActivity.isLoginFist) {
                activity?.finish()
                return@setOnClickListener
            }

            this.goToRegister()
        }
    }

    fun goToRegister() {
        findNavController().navigate(R.id.action_navigation_login_to_navigation_register)
    }

    fun goToForgotPassword(){
        findNavController().navigate(R.id.action_navigation_login_to_navigation_forgot_password)
    }

    fun showPassOrHidePassword() {
        this.isShowPassword = !this.isShowPassword
        if (isShowPassword) {
            txtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            btnShowPassword.text = getString(R.string.hide)
            return
        }
        txtPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        btnShowPassword.text = getString(R.string.show)
    }

    override fun showValidationMessage(message: String?) {
        this.showMessageToast(message ?: EMPTY_STRING)

    }

    override fun openMainActivity() {
        EventBus.post(TypeEvenBus.LoginSuccess("okla"))
        activity?.finish()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    private fun rememberPassword() {
        this.isRememberPassword = !this.isRememberPassword
        if (this.isRememberPassword) {
            icRememberPassword.setImageResource(R.drawable.ic_checked_checkbox)
            return
        }

        icRememberPassword.setImageResource(R.drawable.icons_unchecked_checkbox)
    }
}
