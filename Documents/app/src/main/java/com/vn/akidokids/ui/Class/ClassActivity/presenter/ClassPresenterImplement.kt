package com.vn.akidokids.ui.Class.ClassActivity.presenter

import android.util.Log
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MemberInClass
import com.vn.akidokids.data.network.model.ResultClassInfor
import com.vn.akidokids.ui.Class.ClassActivity.interactor.ClassInteractor
import com.vn.akidokids.ui.Class.ClassActivity.view.ClassView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ClassPresenterImplement<V : ClassView, I : ClassInteractor>
@Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compositeDisposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compositeDisposable
), ClassPresenter<V, I> {

    var resultInforClass: ResultClassInfor? = null

    override fun getInforClass(id: Long) {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getInforClass(id = id).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet))
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.resultInforClass = response.result
                        getView()?.successCallApi()
                    },
                    {err->
                        getView()?.hideProgress()
                        Log.e("ahihi", err.toString())
                    })
            )
        }

    }

    override fun getNameClass(): String {
        return this.resultInforClass?.name ?: EMPTY_STRING
    }

    override fun getThumnailClass(): String {
        return this.resultInforClass?.img ?: EMPTY_STRING
    }

    override fun getSizeClass(): String {
        return  "Tổng số học sinh: ${this.resultInforClass?.actualSize ?: 0}/${this.resultInforClass?.maxSize ?: 0}"
    }

    override fun getListMember(): List<MemberInClass> {
        return this.resultInforClass?.lstuser ?: arrayListOf()
    }
}