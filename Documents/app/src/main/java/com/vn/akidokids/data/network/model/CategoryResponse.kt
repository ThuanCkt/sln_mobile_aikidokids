package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CategoryResponse (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String?,

    @Expose
    @SerializedName("result")
    var result: ResultCategory?
)

data class ResultCategory (
    @Expose
    @SerializedName("data")
    var data: List<Category>? = null,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null

)

data class Category (

    @Expose
    @SerializedName("id")
    var id: String? = null,

    @Expose
    @SerializedName("groupmenuid")
    var groupmenuid: String? = null,

    @Expose
    @SerializedName("parent_id")
    var parentID: Long,

    @Expose
    @SerializedName("name")
    var name: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("img")
    var img: String? = null,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("is_hot")
    var isHot: Boolean,

    @Expose
    @SerializedName("is_popular")
    var isPopular: Boolean,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String? = null,

    @Expose
    @SerializedName("created_by")
    var createdBy: String,

    @Expose
    @SerializedName("created_log")
    var createdLog: String? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null
)