package com.vn.akidokids.ui.login

import com.vn.akidokids.ui.login.interactor.LoginInteractor
import com.vn.akidokids.ui.login.interactor.LoginInteractorImplement
import com.vn.akidokids.ui.login.presenter.LoginPresenter
import com.vn.akidokids.ui.login.presenter.LoginPresenterImplement
import com.vn.akidokids.ui.login.view.LoginView
import dagger.Module
import dagger.Provides

@Module
class LoginModule {
    @Provides
    fun provideInteractor(interactor: LoginInteractorImplement): LoginInteractor = interactor

    @Provides
    fun providePresenter(presenter: LoginPresenterImplement<LoginView, LoginInteractor>): LoginPresenter<LoginView, LoginInteractor> = presenter
}