package com.vn.akidokids.ui.Message.interactor

import com.vn.akidokids.data.network.model.DeteleHistoryResponse
import com.vn.akidokids.data.network.model.MessageTransactionResponse
import com.vn.akidokids.data.network.request.MessageTransactionRequest
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface MessageInteractor:MVPInteractor {
    fun getListHistory(page:Int, pageSize:Int):Observable<MessageTransactionResponse>
    fun deleteMessage(data: MessageTransactionRequest.DeleteTransaction):Observable<DeteleHistoryResponse>
}