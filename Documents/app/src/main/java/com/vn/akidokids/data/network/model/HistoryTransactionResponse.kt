package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HistoryTransactionResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: HistoryTransactionResult? = null
)

data class HistoryTransactionResult (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("page")
    val page: Page? = null,

    @Expose
    @SerializedName("data")
    val data: List<HistoryTransactionData>? = null
)

data class HistoryTransactionData (
    @Expose
    @SerializedName("tran_id")
    val tranID: String? = null,

    @Expose
    @SerializedName("account_name")
    val accountName: String? = null,

    @Expose
    @SerializedName("point_send_text")
    val pointSendText: String? = null,

    @Expose
    @SerializedName("point_after_send_text")
    val pointAfterSendText: String? = null,

    @Expose
    @SerializedName("tran_time")
    val tranTime: String? = null,

    @Expose
    @SerializedName("note")
    val note: String? = null
)
