package com.vn.akidokids.util

val EMPTY_STRING:String = ""
val FORMAT_DATE_API:String = "yyyy-MM-dd'T'HH:mm:ss"
val FORMAT_DATE_APEARANCE:String = "dd/MM/yyyy"
val FORMAT_FULL_DATE:String = "dd/MM/yyyy HH:mm"

val REX_VALID_PASSWORD = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{7,}$".toRegex()