package com.vn.akidokids.ui.login.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.LoginResponse
import com.vn.akidokids.data.network.model.ResultUser
import com.vn.akidokids.data.network.request.LoginRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import com.vn.akidokids.util.AppConstants
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.Utils
import io.reactivex.Observable
import javax.inject.Inject

class LoginInteractorImplement @Inject internal constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper, apiHelper), LoginInteractor {

    override fun doServerLoginApiCall(email: String, password: String): Observable<LoginResponse> {
        return apiHelper.performServerLogin(
            request = LoginRequest.ServerLoginRequest(
                username = email,
                password = password,
                deviceID = Utils.getModelDevice(),
                name = "",
                tokenAPNs = "",
                deviceName = Utils.getNameDevice()
            )
        )
    }

    override fun updateUserInSharedPref(
        userResult: ResultUser,
        loggedInMode: AppConstants.LoggedInMode,
        isRemember: Boolean
    ) =
        preferenceHelper.let {
            it.setCurrentUserId(userResult?.user?.id)
            it.setAccessToken(userResult?.token)
            Utils.token = userResult?.token ?: EMPTY_STRING
            it.setCurrentUserEmail(userResult?.user?.email)
            it.setCurrentUserName(userResult?.user?.username)
            preferenceHelper.setUserEntity(userResult?.user)
            it.setCurrentUserLoggedInMode(loggedInMode)
            it.setIsRemember(isRemember)
        }

}
