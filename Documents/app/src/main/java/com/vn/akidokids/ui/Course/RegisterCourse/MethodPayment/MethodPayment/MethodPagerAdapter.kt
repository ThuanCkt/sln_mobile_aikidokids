package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingFragment
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentFragment
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentDialog
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPoint.MethodPointFragment

class MethodPagerAdapter(dialog: MethodPaymentDialog) : FragmentStatePagerAdapter(dialog.childFragmentManager) {

    private var tabCount = 0

    val methodBank =  MethodBankingFragment.newInstance()
    val methodDownPaymentFragment = MethodDownPaymentFragment.newInstance()
    val methodPointFragment = MethodPointFragment.newInstance()

    override fun getCount(): Int {
        return tabCount
    }

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> methodBank
            1 -> methodDownPaymentFragment
            2 -> methodPointFragment
            else -> null
        }
    }

    internal fun setCount(count: Int) {
        this.tabCount = count
    }

}