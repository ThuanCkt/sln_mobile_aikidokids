package com.vn.akidokids.ui.TermAndAbout.presenter

import com.vn.akidokids.ui.TermAndAbout.interactor.TermAndAboutInteractor
import com.vn.akidokids.ui.TermAndAbout.view.TermAndAboutView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface TermAndAboutPresenter<V:TermAndAboutView, I:TermAndAboutInteractor>:MVPPresenter<V,I> {
    fun callTermAndAbout()
    fun getTerm():String
    fun getAbout():String
}