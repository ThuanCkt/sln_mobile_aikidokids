package com.vn.akidokids.data.network

import com.androidnetworking.common.Priority
import com.google.gson.Gson
import com.rx2androidnetworking.Rx2AndroidNetworking
import com.vn.akidokids.data.network.model.*
import com.vn.akidokids.data.network.request.*
import com.vn.akidokids.util.Utils
import io.reactivex.Observable
import org.json.JSONObject
import javax.inject.Inject

/**
 * Created by jyotidubey on 04/01/18.
 */
class AppApiHelper @Inject constructor(private val apiHeader: ApiHeader) : ApiHelper {
    //Login, register and forgot password account

    override fun performServerLogin(request: LoginRequest.ServerLoginRequest): Observable<LoginResponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
            .addHeaders(apiHeader.publicApiHeader)
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(LoginResponse::class.java)
    }


    override fun performServerRegister(request: RegisterRequest): Observable<LoginResponse> =
        Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_REGISTER)
            .addHeaders(apiHeader.publicApiHeader)
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(LoginResponse::class.java)

    override fun performServerForgotPassword(request: ForgotPasswordRequest): Observable<ForgotPasswordResponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_FORGOT_PASSWORD)
            .addHeaders(apiHeader.publicApiHeader)
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build().getObjectObservable(ForgotPasswordResponse::class.java)
    }

    //Category and article

    override fun performServerListCategory(): Observable<CategoryResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_LIST_CATEGORY)
            .addHeaders(Utils.getHeaderDefault())
            .setPriority(Priority.HIGH)
            .build()
            .getObjectObservable(CategoryResponse::class.java)
    }

    override fun performServerListArticleFollowCategory(request: CategoryRequest.ServerListArticleFollowCategory): Observable<ArticleCategoryResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_LIST_ARTICLER_FOLLOW_CATEGORY)
            .addHeaders(Utils.getHeaderDefault())
            .addQueryParameter(request)
            .build()
            .getObjectObservable(ArticleCategoryResponse::class.java)

    }

    override fun performServerDetailArticle(request: CategoryRequest.ServerDetailArticle): Observable<DetailArticleResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_DETAIL_ARTIClE)
            .addHeaders(Utils.getHeaderDefault())
            .addPathParameter(request)
            .build()
            .getObjectObservable(DetailArticleResponse::class.java)
    }

    //Course

    override fun performServerListCourse(): Observable<ListCourseResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_LIST_COURSE)
            .addHeaders(Utils.getHeaderDefault())
            .build()
            .getObjectObservable(ListCourseResponse::class.java)
    }

    override fun performServerDetailCourse(request: CourseRequest.DetailCourseRequest): Observable<DetailCourseResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_DETAIL_COURSE)
            .addHeaders(Utils.getHeaderDefault()).addPathParameter(request).build()
            .getObjectObservable(DetailCourseResponse::class.java)
    }

    override fun performServerQuantityStudent(request: CourseRequest.QuantityStudentRequest): Observable<QuantityStudentResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_QUANTITY_STUDENT)
            .addHeaders(Utils.getHeader()).addQueryParameter(request).build()
            .getObjectObservable(QuantityStudentResponse::class.java)
    }

    override fun performServerReferUser(request: CourseRequest.ReferUserRequest): Observable<UserReferResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_REFER_USER)
            .addHeaders(Utils.getHeader()).addPathParameter(request).build()
            .getObjectObservable(UserReferResponse::class.java)
    }

    override fun performServerRegistCourse(request: CourseRequest.BookingCourse): Observable<BookingCourseResponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENPOINT_SERVER_BOOKING_COURSE)
            .addHeaders(Utils.getHeader())
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(BookingCourseResponse::class.java)
    }

    override fun performServerMethodPayment(): Observable<MethodPaymentResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_METHOD_PAYMENT)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(MethodPaymentResponse::class.java)
    }

    //Class
    override fun performServerInforClass(request:ClassRequest.ClassInforRequest): Observable<ClassInforResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_INFOR_CLASS)
            .addPathParameter(request)
            .addQueryParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(ClassInforResponse::class.java)
    }

    override fun performServerArticleClass(request: ClassRequest.ArticleClassRequest): Observable<PostArticleClassResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_ARTICLE_INFOR_CLASS)
            .addQueryParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(PostArticleClassResponse::class.java)
    }

    override fun performServerGalleyClass(request: ClassRequest.GalleyClassRequest): Observable<AlbumClassResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_GALLEY_CLASS)
            .addQueryParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(AlbumClassResponse::class.java)
    }

    override fun performServerDetailArticleClass(request: ClassRequest.DetailClassRequest): Observable<DetailArticleClassResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_DETAIL_ARTICLE_CLASS)
            .addPathParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(DetailArticleClassResponse::class.java)
    }

    //User
    override fun performServerUserInfor(): Observable<UserInforResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_USER_INFOR)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(UserInforResponse::class.java)
    }

    override fun performServerChangePassword(request: UserRequest.ChangePasswordRequest): Observable<LoginResponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENPOINT_SERVER_CHANGE_PASSWORD)
            .addHeaders(Utils.getHeader())
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(LoginResponse::class.java)
    }

    override fun performServerUserInforById(request: UserRequest.UserIdRequest): Observable<ResponseUserId> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_USER_INFOR_BY_ID)
            .addPathParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(ResponseUserId::class.java)
    }

    override fun performServerProvince(): Observable<ProvinceResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_PROVINCE)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(ProvinceResponse::class.java)
    }

    override fun performServerUserInforUpdate(request: UserRequest.ChangeInforRequest): Observable<ResponseUserId> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENPOINT_SERVER_CHANGE_INFOR)
            .addHeaders(Utils.getHeader())
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(ResponseUserId::class.java)
    }

    override fun performServerUserAvatarUpdate(request: UserRequest.ChangeAvartarRequest): Observable<UpdateAvatarReponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENPOINT_SERVER_CHANGE_AVATAR)
            .addHeaders(Utils.getHeader())
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(UpdateAvatarReponse::class.java)
    }

    //Term and About
    override fun performServerTermAndServer(): Observable<TermAndAboutResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_TERM_AND_ABOUT)
            .addHeaders(Utils.getHeaderDefault()).build()
            .getObjectObservable(TermAndAboutResponse::class.java)
    }

    //History point
    override fun performServerHistoryPoint(request:HistoryTransactionRequest.ServerListHistoryTransactionRequest): Observable<HistoryTransactionResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_LIST_TRANSACTION_PAY)
            .addQueryParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(HistoryTransactionResponse::class.java)
    }

    override fun performServerDeleteHistoryPoint(request: HistoryTransactionRequest.DeleteTransaction): Observable<DeteleHistoryResponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENPOINT_SERVER_DELETE_TRANSACTION_PAY)
            .addHeaders(Utils.getHeader())
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(DeteleHistoryResponse::class.java)
    }

    override fun performServerMessage(request:MessageTransactionRequest.ServerListMessageTransactionRequest): Observable<MessageTransactionResponse> {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENPOINT_SERVER_LIST_MESSAGE)
            .addQueryParameter(request)
            .addHeaders(Utils.getHeader()).build()
            .getObjectObservable(MessageTransactionResponse::class.java)
    }

    override fun performServerDeleteMessage(request: MessageTransactionRequest.DeleteTransaction): Observable<DeteleHistoryResponse> {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENPOINT_SERVER_DELETE_MESSAGE)
            .addHeaders(Utils.getHeader())
            .addJSONObjectBody(JSONObject(Gson().toJson(request)))
            .build()
            .getObjectObservable(DeteleHistoryResponse::class.java)
    }
}