package com.vn.akidokids.ui.Course.LessonCourse.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Lstakdlessonvm
import com.vn.akidokids.ui.VideoYoutube.VideoYoutubeActivity
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.util.ParameterAttribute
import javax.inject.Inject

class LessonCourseFragment : BaseFragment(), LessonView,
    LessonCourseAdapter.LessonCourseAdapterInterface {
    lateinit var mRecyclerViewArticle: RecyclerView
    var listTutorial: List<Lstakdlessonvm> = arrayListOf()
    var isFree: Boolean = false

    companion object {
        fun newInstance(): LessonCourseFragment {
            return LessonCourseFragment()
        }
    }

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var adapter: LessonCourseAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_lesson_cause, container, false)
        mRecyclerViewArticle = view.findViewById<RecyclerView>(R.id.rcvLessonCause)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter.setListLesson(listTutorial)
    }

    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerViewArticle.layoutManager = layoutManager
        mRecyclerViewArticle.itemAnimator = DefaultItemAnimator()
        mRecyclerViewArticle.adapter = adapter
    }

    fun setListLesson(listTutorial: List<Lstakdlessonvm>, isFree: Boolean) {
        adapter.setListLesson(listTutorial)
        this.isFree = isFree
    }

    //LessonCourseAdapter
    override fun openVideoActivity(code: String) {
        if (!isFree) {
            this.showMessageToast(resources.getString(R.string.this_lesson_not_free_pls_choose_lesson_diffirent))
           return
        }
        val intent = Intent(this.context, VideoYoutubeActivity::class.java)
        intent.putExtra(ParameterAttribute.CODE_YOUTUBE.nameKey, code)
        this.startActivity(intent)
    }

    override fun openDetailActivity(lesson: Lstakdlessonvm) {
        if (!isFree) {
            this.showMessageToast(resources.getString(R.string.this_lesson_not_free_pls_choose_lesson_diffirent))
        }


    }
}
