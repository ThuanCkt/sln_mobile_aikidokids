package com.vn.akidokids.ui.Course.LessonCourse.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.data.model.GalleyModel
import com.vn.akidokids.ui.Class.DetailGalley.DetailGalleyActivity
import com.vn.akidokids.ui.Class.GalleyClass.interactor.GalleyClassInteractor
import com.vn.akidokids.ui.Class.GalleyClass.presenter.GalleyClassPresenter
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.fragment_galley_class.*
import javax.inject.Inject

class GalleyClassFragment : BaseFragment(), GalleyClassView,
    GalleyClassAdapter.GalleyClassAdapterInterface {


    companion object {
        fun newInstance(): GalleyClassFragment {
            return GalleyClassFragment()
        }
    }

    private var idClass: Long = 0.toLong()

    @Inject
    lateinit var gridLayoutManager: GridLayoutManager

    @Inject
    lateinit var adapter: GalleyClassAdapter

    @Inject
    lateinit var presenter: GalleyClassPresenter<GalleyClassView, GalleyClassInteractor>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_galley_class, container, false)
    }

    fun setIdClass(idClass: Long) {
        if (this.idClass != idClass) {
            this.idClass = idClass
            return
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
        setUp()
        presenter.callApiGalley(this.idClass)
    }

    override fun onDestroy() {
        this.presenter.onDetach()
        super.onDestroy()
    }

    override fun setUp() {
        rcvGalleyClass.layoutManager = gridLayoutManager
        rcvGalleyClass.itemAnimator = DefaultItemAnimator()
        rcvGalleyClass.adapter = adapter
    }

    override fun successCallApi() {
        adapter.setListAlbum(presenter.getListGalley())
    }

    override fun failCallApi(message: String) {
    }

    override fun itemGalleyClick(position: Int) {
        val intent = Intent(this.context, DetailGalleyActivity::class.java)
        val galley = GalleyModel(index = position, listImages = presenter.getListGalley())
        intent.putExtra(ParameterAttribute.GALLEY.nameKey, galley)
        this.startActivity(intent)
    }
}
