package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UpdateAvatarReponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,
    @Expose
    @SerializedName("statuscode")
    val statuscode: String?,
    @Expose
    @SerializedName("result")
    val result: UpdateAvatarResult? = null
)

data class UpdateAvatarResult (
    @Expose
    @SerializedName("avatar")
    val avatar: String? = null,
    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean? = false,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null
)
