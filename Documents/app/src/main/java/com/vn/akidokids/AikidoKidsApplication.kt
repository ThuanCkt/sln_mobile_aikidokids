package com.vn.akidokids

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Resources
import androidx.appcompat.app.AppCompatDelegate
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.vn.akidokids.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

class AikidoKidsApplication:Application(), HasAndroidInjector {
    companion object {
        lateinit var resources:Resources
        lateinit var context:Context
        var activityCurrent: Activity? = null

        fun setCurrentActivity(activity: Activity?) {
            this.activityCurrent = activity
            this.activityCurrent?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

        fun getAppResources():Resources {
            return resources
        }
    }
    @Inject
    lateinit internal var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any>  = activityDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        AndroidNetworking.enableLogging();
        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)
        DaggerAppComponent.builder().application(this).build().inject(this)
        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/helveticaneuelight.ttf")
                .build()
        )
        AikidoKidsApplication.resources = getResources()
        AikidoKidsApplication.context = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}