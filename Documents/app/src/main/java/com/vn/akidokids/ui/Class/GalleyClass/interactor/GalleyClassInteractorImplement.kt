package com.vn.akidokids.ui.Class.GalleyClass.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.AlbumClassResponse
import com.vn.akidokids.data.network.request.ClassRequest
import com.vn.akidokids.data.preferences.AppPreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class GalleyClassInteractorImplement @Inject constructor(
    apiHelper: ApiHelper,
    preferenceHelper: AppPreferenceHelper
) : BaseInteractor(apiHelper = apiHelper, preferenceHelper = preferenceHelper), GalleyClassInteractor {

    override fun getGalley(id: Long): Observable<AlbumClassResponse> {
        return apiHelper.performServerGalleyClass(ClassRequest.GalleyClassRequest(id))
    }

}