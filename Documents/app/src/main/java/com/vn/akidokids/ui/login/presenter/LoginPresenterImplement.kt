package com.vn.akidokids.ui.login.presenter

import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ResultUser
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.ui.login.interactor.LoginInteractor
import com.vn.akidokids.ui.login.view.LoginView
import com.vn.akidokids.util.AppConstants
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginPresenterImplement<V : LoginView, I : LoginInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = disposable
), LoginPresenter<V, I> {

    override fun onServerLoginClicked(usename: String, password: String, isRemember: Boolean) {
        when {
            usename.isEmpty() -> this.getView()?.showValidationMessage(
                AikidoKidsApplication.getAppResources().getString(
                    R.string.username_not_empty
                )
            )
            password.isEmpty() -> this.getView()?.showValidationMessage(
                AikidoKidsApplication.getAppResources().getString(
                    R.string.password_not_empty
                )
            )
            else -> {
                getView()?.showProgress()
                interactor?.let {
                    compositeDisposable.add(
                        it.doServerLoginApiCall(usename, password)
                            .compose(schedulerProvider.ioToMainObservableScheduler())
                            .subscribe({ loginResponse ->
                                getView()?.hideProgress()
                                if (loginResponse.result == null) {
                                    this.getView()?.showValidationMessage(
                                        AikidoKidsApplication.getAppResources().getString(R.string.username_or_password_not_correct)
                                    )
                                    return@subscribe
                                }

                                if (loginResponse.result?.isok == true) {
                                    this.getView()?.showValidationMessage(
                                        loginResponse.result?.msg
                                            ?: AikidoKidsApplication.getAppResources().getString(R.string.username_or_password_not_correct)
                                    )
                                    return@subscribe
                                }

                                this.updateUserInSharedPref(
                                    resultUser = loginResponse.result!!,
                                    loggedInMode = AppConstants.LoggedInMode.LOGGED_IN_MODE_SERVER,
                                    isRemember = isRemember
                                )

                                getView()?.openMainActivity()

                            }, { err ->
                                getView()?.hideProgress()
                                this.getView()?.showValidationMessage(
                                    AikidoKidsApplication.getAppResources().getString(R.string.username_or_password_not_correct)
                                )
                            })
                    )
                }
            }
        }
    }

    private fun updateUserInSharedPref(
        resultUser: ResultUser,
        loggedInMode: AppConstants.LoggedInMode,
        isRemember: Boolean
    ) =
        interactor?.updateUserInSharedPref(resultUser, loggedInMode, isRemember)
}