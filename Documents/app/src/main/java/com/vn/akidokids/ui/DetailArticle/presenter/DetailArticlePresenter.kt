package com.vn.akidokids.ui.DetailArticle.presenter

import com.vn.akidokids.data.network.model.ResultDetailArticle
import com.vn.akidokids.ui.DetailArticle.interactor.DetailArticleInteractor
import com.vn.akidokids.ui.DetailArticle.view.DetailArticleView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface DetailArticlePresenter<V:DetailArticleView, I:DetailArticleInteractor>:MVPPresenter<V,I> {
    fun getDetailArticle(id:Long)
    fun getContentHtml():String?
    fun getListRelateArticle(): List<ResultDetailArticle>?
    fun getTitleArticle():String?
    fun getTextDateCreateArticle():String?
}