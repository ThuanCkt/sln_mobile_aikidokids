package com.vn.akidokids.data.model

import android.content.Context
import com.vn.akidokids.R

data class MenuSection(var name:String? = null, var index:Int,var iconId:Int? = null, var listMenuItem:List<MenuItem>? = null) {

    companion object {
        fun initListMenu(context:Context):MutableList<MenuSection> {
            var listMenuSection:MutableList<MenuSection> = mutableListOf()
            var itemSection = MenuSection(
                name = context.getString(R.string.manager_point),
                index = 0,
                iconId = R.drawable.ic_coins
            )
            var listMenuItem:MutableList<MenuItem> = mutableListOf()
            var itemMenuItem:MenuItem = MenuItem(name = context.getString(R.string.history_point), index = 0, menuType = EnumMenuType.HISTORY_POINT)
            listMenuItem.add(itemMenuItem)
            itemSection.listMenuItem = listMenuItem
            listMenuSection.add(itemSection)

            itemSection = MenuSection(name = context.getString(R.string.class_label),index =  1,iconId =  R.drawable.ic_class)
            listMenuItem = mutableListOf()
            itemSection.listMenuItem = listMenuItem
            listMenuSection.add(itemSection)

            itemSection = MenuSection("", 2)
            listMenuItem = mutableListOf()
            itemMenuItem = MenuItem(name = context.getString(R.string.about), index = 0, iconId = R.drawable.ic_about, menuType = EnumMenuType.ABOUT)
            listMenuItem.add(itemMenuItem)
            itemMenuItem = MenuItem(name = context.getString(R.string.term), index = 1, iconId = R.drawable.ic_term, menuType = EnumMenuType.TERM)
            listMenuItem.add(itemMenuItem)
            itemMenuItem = MenuItem(name = context.getString(R.string.announce), index = 3, iconId = R.drawable.ic_notification, menuType = EnumMenuType.MESSSAGE)
            listMenuItem.add(itemMenuItem)
            itemSection.listMenuItem = listMenuItem
            listMenuSection.add(itemSection)
            
            return listMenuSection
        }

        fun initListMenuNotLogin(context:Context):MutableList<MenuSection> {
            var listMenuSection:MutableList<MenuSection> = mutableListOf()

            var listMenuItem:MutableList<MenuItem> = mutableListOf()
            var itemMenuItem:MenuItem = MenuItem(name = context.getString(R.string.history_point), index = 0, menuType = EnumMenuType.HISTORY_POINT)
            var itemSection = MenuSection("", 2)
            listMenuItem = mutableListOf()
            itemMenuItem = MenuItem(name = context.getString(R.string.about), index = 0, iconId = R.drawable.ic_about, menuType = EnumMenuType.ABOUT)
            listMenuItem.add(itemMenuItem)
            itemMenuItem = MenuItem(name = context.getString(R.string.term), index = 1, iconId = R.drawable.ic_term, menuType = EnumMenuType.TERM)
            listMenuItem.add(itemMenuItem)
            itemSection.listMenuItem = listMenuItem
            listMenuSection.add(itemSection)

            return listMenuSection
        }

        fun convertOneDimensionalArray(listMenuSection: List<MenuSection>? = null): MutableList<Any> {
            var listMenu:MutableList<Any> = mutableListOf()
            listMenuSection?.let {
                for (item:Any in it) {
                    if (item is MenuSection) {
                        if (!item.name.isNullOrEmpty()) {
                            listMenu.add(item)
                        }
                        item.listMenuItem?.let { it1 -> listMenu.addAll(it1) }
                    }
                }
            }
            return listMenu
        }
    }
}