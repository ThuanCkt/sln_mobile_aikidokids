package com.vn.akidokids.ui.Course.LessonCourse.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.ui.Class.ArticleClass.interactor.ArticleClassInteractor
import com.vn.akidokids.ui.Class.ArticleClass.presenter.ArticleClassPresenter
import com.vn.akidokids.ui.Class.DetailArticleClass.view.DetailArticleClassActivity
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.util.PaginationScrollListener
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.fragment_article_class.*
import javax.inject.Inject

class ArticleClassFragment : BaseFragment(), ArticleClassView, ArticleClassAdapter.ArticleClassAdapterInterface {
    companion object {
        fun newInstance():ArticleClassFragment {
            return ArticleClassFragment()
        }
    }

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var adapter: ArticleClassAdapter

    @Inject
    lateinit var presenter: ArticleClassPresenter<ArticleClassView, ArticleClassInteractor>

    private var idClass:Long = 0.toLong()

    var isLoading: Boolean = false

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View = inflater.inflate(R.layout.fragment_article_class, container, false)
        return view
    }

    fun setIdClass(idClass:Long) {
        if (this.idClass != idClass) {
            this.idClass = idClass
            return
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
        setUp()
        presenter.getListArticle(idClass)
    }


    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rcvArticleClass.layoutManager = layoutManager
        rcvArticleClass.itemAnimator = DefaultItemAnimator()
        rcvArticleClass.adapter = adapter
        rcvArticleClass?.addOnScrollListener(object : PaginationScrollListener(this.layoutManager) {
            override fun isLastPage(): Boolean {
                return false
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                presenter.loadMore(idClass)
            }
        })
    }

    override fun successCallApi() {
        this.isLoading = !presenter.getIsLoadmore()
        adapter.setListArticle(presenter.getListArticle(), presenter.getIsLoadmore())
    }

    override fun failCallApi(message: String) {
    }

    override fun articleOnClick(id: Long) {
        val intent = Intent(this.context, DetailArticleClassActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, id)
        this.startActivity(intent)
    }
}
