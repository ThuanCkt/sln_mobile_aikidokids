package com.vn.akidokids.ui.SectionExpired

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.vn.akidokids.R
import com.vn.akidokids.util.EventBus
import com.vn.akidokids.util.TypeEvenBus
import kotlinx.android.synthetic.main.fragment_section_expired.*

class SectionExpiredDialog : DialogFragment() {

    companion object {
        fun newInstance(): SectionExpiredDialog? {
            return SectionExpiredDialog()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_section_expired, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btnLogout.setOnClickListener {
            EventBus.post(TypeEvenBus.LogOut())
            this.dismiss()
        }
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.post(TypeEvenBus.LogOut())
    }

}
