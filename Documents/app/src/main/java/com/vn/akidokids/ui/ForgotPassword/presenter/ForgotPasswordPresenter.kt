package com.vn.akidokids.ui.ForgotPassword.presenter

import com.vn.akidokids.ui.ForgotPassword.interactor.ForgotPasswordInteractor
import com.vn.akidokids.ui.ForgotPassword.view.ForgotPasswordView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface ForgotPasswordPresenter<V: ForgotPasswordView, I: ForgotPasswordInteractor>: MVPPresenter<V, I> {
    fun forgotPassword(email:String, note:String)
}