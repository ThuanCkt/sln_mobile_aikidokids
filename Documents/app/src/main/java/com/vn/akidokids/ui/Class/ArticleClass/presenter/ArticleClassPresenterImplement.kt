package com.vn.akidokids.ui.Class.ArticleClass.presenter

import android.util.Log
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ClassPostsData
import com.vn.akidokids.ui.Class.ArticleClass.interactor.ArticleClassInteractor
import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ArticleClassPresenterImplement<V : ArticleClassView, I : ArticleClassInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    dispose: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    compositeDisposable = dispose,
    schedulerProvider = schedulerProvider
), ArticleClassPresenter<V, I> {
    private var page: Long = 1
    private var pageSize: Long = 10

    var listArticles: MutableList<ClassPostsData> = arrayListOf()
    var isLoadmore:Boolean = false
    var isShowLoading:Boolean = true

    override fun getListArticle(idClass: Long) {
        interactor?.let {
            if (isShowLoading) {
                getView()?.showProgress()
            }
            compositeDisposable.add(
                it.getListArticleClass(
                    idClass = idClass,
                    page = this.page,
                    pageSize = this.pageSize
                ).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }
                        this.listArticles.addAll(response.result?.data ?: arrayListOf())
                        getView()?.successCallApi()
                        this.isLoadmore = response.result?.page?.isloadmore ?: false
                        this.page = response.result?.page?.currentpage ?: 1
                    },
                    { err ->
                        getView()?.hideProgress()
                        Log.e("ahihi", err.toString())
                    })
            )
        }
    }


    override fun getListArticle(): List<ClassPostsData> {
        return listArticles
    }

    override fun getIsLoadmore(): Boolean {
        return this.isLoadmore
    }

    override fun loadMore(idClass:Long) {
        this.page += 1
        this.isShowLoading = false
        this.getListArticle(idClass)
    }
}