package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProvinceResponse(
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,
    @Expose
    @SerializedName("result")
    val result: ProvinceResult? = null
)

data class ProvinceResult(
    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean? = false,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("data")
    val data: List<ProvinceDatum>? = null
)

data class ProvinceDatum(
    @Expose
    @SerializedName("id")
    val id: Long,

    @Expose
    @SerializedName("name")
    val name: String? = null
)
