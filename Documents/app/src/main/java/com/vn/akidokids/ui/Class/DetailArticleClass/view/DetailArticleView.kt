package com.vn.akidokids.ui.Class.DetailArticleClass.view

import com.vn.akidokids.ui.base.view.MVPView

interface DetailArticleView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi()
}