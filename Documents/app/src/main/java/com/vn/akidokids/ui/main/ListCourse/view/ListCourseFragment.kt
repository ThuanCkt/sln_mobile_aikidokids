package com.vn.akidokids.ui.main.ListCourse.view


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.vn.akidokids.R
import com.vn.akidokids.ui.Course.Course.view.CourseActivity
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.ui.main.ListCourse.interacror.ListCourseInteractor
import com.vn.akidokids.ui.main.ListCourse.presenter.ListCoursePresenter
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.fragment_list_cause.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ListCourseFragment : BaseFragment(), ListCourseView, CourseDelegate, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var presenter:ListCoursePresenter<ListCourseView, ListCourseInteractor>

    @Inject
    lateinit var listCourseAdapter: ListCourseAdapter

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    companion object {
        fun newInstance(): ListCourseFragment {
            return ListCourseFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_cause, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
        presenter.callListCourse()
        swipeRefresh.setColorSchemeColors(resources.getColor(R.color.color_primary))
        swipeRefresh.setOnRefreshListener(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rcvListCourse.layoutManager = layoutManager
        rcvListCourse.itemAnimator = DefaultItemAnimator()
        rcvListCourse.adapter = listCourseAdapter
    }

    override fun clickItemCourse(id: Long) {
       val intent = Intent(this.activity, CourseActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, id)
        this.startActivity(intent)
    }

    //delegate view

    override fun sucessCallApi() {
        swipeRefresh.isRefreshing = false
        listCourseAdapter.setListCourse(this.presenter.getListCourse())
    }

    override fun onRefresh() {
        this.presenter.pullToRefresh()

    }
}
