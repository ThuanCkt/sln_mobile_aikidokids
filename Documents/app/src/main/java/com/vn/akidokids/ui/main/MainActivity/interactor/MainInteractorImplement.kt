package com.vn.akidokids.ui.main.MainActivity.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.CategoryResponse
import com.vn.akidokids.data.network.model.UserInforResponse
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class MainInteractorImplement @Inject constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper): BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    MainInteractor {

    override fun getListCategory(): Observable<CategoryResponse> {
        return apiHelper.performServerListCategory()
    }

    override fun getUserInfor(): Observable<UserInforResponse> {
        return apiHelper.performServerUserInfor()
    }
}