package com.vn.akidokids.di.builder

import com.vn.akidokids.ui.AccountDetail.AccountDetailModule
import com.vn.akidokids.ui.AccountDetail.view.AccountDetailActivity
import com.vn.akidokids.ui.ChangeInformation.ChangeInformationModule
import com.vn.akidokids.ui.ChangeInformation.view.ChangeInformationActivity
import com.vn.akidokids.ui.ChangePassword.ChangePasswordModule
import com.vn.akidokids.ui.ChangePassword.view.ChangePassActivity
import com.vn.akidokids.ui.Class.ClassActivity.ClassModule
import com.vn.akidokids.ui.Class.ClassActivity.view.ClassActivity
import com.vn.akidokids.ui.Class.DetailArticleClass.DetailArticleClassModule
import com.vn.akidokids.ui.Class.DetailArticleClass.view.DetailArticleClassActivity
import com.vn.akidokids.ui.Course.Course.CourseModule
import com.vn.akidokids.ui.Course.Course.view.CourseActivity
import com.vn.akidokids.ui.Course.LessonCourse.ArticleClassProvide
import com.vn.akidokids.ui.Course.LessonCourse.GalleyClassProvide
import com.vn.akidokids.ui.Course.LessonCourse.LessonCourseProvide
import com.vn.akidokids.ui.Course.ListTeacher.TeacherProvider
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.MethodPaymentProvider
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.RegisterCourseModule
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view.RegisterCourseActivity
import com.vn.akidokids.ui.DetailArticle.DetailArticleModule
import com.vn.akidokids.ui.DetailArticle.view.DetailArticleActivity
import com.vn.akidokids.ui.ForgotPassword.ForgotPasswordProvider
import com.vn.akidokids.ui.HistoryPoint.HistoryPointModule
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPointActivity
import com.vn.akidokids.ui.LoginAndRegister.LoginAndRegisterModule
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterActivity
import com.vn.akidokids.ui.Message.MessageModule
import com.vn.akidokids.ui.Message.view.MessageActivity
import com.vn.akidokids.ui.TermAndAbout.TermAndAboutRouter
import com.vn.akidokids.ui.TermAndAbout.view.TermAndAboutActivity
import com.vn.akidokids.ui.login.LoginFragmentProvider
import com.vn.akidokids.ui.main.Category.CategoryProvider
import com.vn.akidokids.ui.main.ListCourse.ListCourseProvides
import com.vn.akidokids.ui.main.MainActivity.MainActivityModule
import com.vn.akidokids.ui.main.MainActivity.view.MainActivity
import com.vn.akidokids.ui.register.RegisterProvider
import com.vn.akidokids.ui.splash.SplashActivityModule
import com.vn.akidokids.ui.splash.view.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(
        modules = [(LoginAndRegisterModule::class),
            (LoginFragmentProvider::class),
            (RegisterProvider::class),
            (ForgotPasswordProvider::class)
        ]
    )
    abstract fun bindLoginAndRegisterActivity(): LoginAndRegisterActivity


    @ContributesAndroidInjector(
        modules = [(MainActivityModule::class), (CategoryProvider::class), (ListCourseProvides
        ::class)]
    )
    abstract fun bindMainActivity(): MainActivity


    @ContributesAndroidInjector(modules = [(CourseModule::class), (LessonCourseProvide::class), (TeacherProvider::class)])
    abstract fun bindCourseActivity(): CourseActivity


    @ContributesAndroidInjector(modules = [(RegisterCourseModule::class), (MethodPaymentProvider::class)])
    abstract fun bindRegisterCourseActivity(): RegisterCourseActivity

    @ContributesAndroidInjector(modules = [(ClassModule::class), (ArticleClassProvide::class), (GalleyClassProvide::class)])
    abstract fun bindClassActivity(): ClassActivity

    @ContributesAndroidInjector(modules = [(DetailArticleModule::class)])
    abstract fun bindDetailArticleActivity(): DetailArticleActivity

    @ContributesAndroidInjector(modules = [DetailArticleClassModule::class])
    internal abstract fun provideDetailArticleClassActivity(): DetailArticleClassActivity

    @ContributesAndroidInjector(modules = [TermAndAboutRouter::class])
    internal abstract fun provideTermAndAboutActivity(): TermAndAboutActivity

    @ContributesAndroidInjector(modules = [HistoryPointModule::class])
    internal abstract fun provideHistoryPointActivity(): HistoryPointActivity

    @ContributesAndroidInjector(modules = [AccountDetailModule::class])
    internal abstract fun provideAccountDetailActivity(): AccountDetailActivity

    @ContributesAndroidInjector(modules = [ChangePasswordModule::class])
    internal abstract fun provideChangePassActivity(): ChangePassActivity

    @ContributesAndroidInjector(modules = [ChangeInformationModule::class])
    internal abstract fun provideChangeInformationActivity(): ChangeInformationActivity

    @ContributesAndroidInjector(modules = [MessageModule::class])
    internal abstract fun provideMessageActivity(): MessageActivity
}