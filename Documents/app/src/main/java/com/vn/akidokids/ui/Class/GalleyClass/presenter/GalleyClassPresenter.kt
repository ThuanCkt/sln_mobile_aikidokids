package com.vn.akidokids.ui.Class.GalleyClass.presenter

import com.vn.akidokids.data.network.model.AlbumClassData
import com.vn.akidokids.ui.Class.GalleyClass.interactor.GalleyClassInteractor
import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface GalleyClassPresenter<V:GalleyClassView, I:GalleyClassInteractor>:MVPPresenter<V, I> {
    fun callApiGalley(idClass:Long)
    fun getListGalley():List<AlbumClassData>
}