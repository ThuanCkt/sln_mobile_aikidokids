package com.vn.akidokids.ui.main.ListCourse.interacror

import com.vn.akidokids.data.network.model.ListCourseResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface  ListCourseInteractor:MVPInteractor {
    fun getListCourse():Observable<ListCourseResponse>
}