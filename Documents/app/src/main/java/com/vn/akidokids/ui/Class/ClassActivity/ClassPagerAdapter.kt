package com.vn.akidokids.ui.Class.ClassActivity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassFragment
import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassFragment

class ClassPagerAdapter (fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    private var tabCount = 0

    private var articleClassFragment = ArticleClassFragment.newInstance()
    private var galleyClassFragment = GalleyClassFragment.newInstance()
    private var idClass:Long = 0.toLong()
    override fun getCount(): Int {
        return tabCount
    }

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> articleClassFragment
            1 -> galleyClassFragment
            else -> null
        }
    }

    internal fun setCount(count: Int) {
        this.tabCount = count
    }

    fun setIdClass(idClass:Long) {
        this.idClass = idClass
        articleClassFragment.setIdClass(idClass)
        galleyClassFragment.setIdClass(idClass)
    }
}