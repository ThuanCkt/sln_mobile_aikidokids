package com.vn.akidokids.ui.ForgotPassword.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.vn.akidokids.R
import com.vn.akidokids.ui.ForgotPassword.interactor.ForgotPasswordInteractor
import com.vn.akidokids.ui.ForgotPassword.presenter.ForgotPasswordPresenter
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.extension.hideKeyboard
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ForgotPasswordFragment : BaseFragment(), ForgotPasswordView {

    @Inject
    lateinit var presenter: ForgotPasswordPresenter<ForgotPasswordView, ForgotPasswordInteractor>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun setUp() {
        btnBack.setOnClickListener {
            this.goToLogin()
        }

        lnLogin.setOnClickListener {
            this.goToLogin()
        }

        btnSend.setOnClickListener {
            this.hideKeyboard()
            this.presenter.forgotPassword(txtUsername.text.toString(), txtNote.text.toString())
        }
    }

    fun goToLogin(){
        findNavController().popBackStack(R.id.navigation_login, false)
    }

    override fun showValidMessage(message: String) {
        this.showMessageToast(message ?: EMPTY_STRING)
    }
}
