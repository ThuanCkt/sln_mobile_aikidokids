package com.vn.akidokids.ui.Course.Course.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Lstakdlessonvm
import com.vn.akidokids.data.network.model.Lstakdteachervm
import com.vn.akidokids.data.network.model.ResultDetailCourse
import com.vn.akidokids.ui.Course.Course.interactor.CourseInteractor
import com.vn.akidokids.ui.Course.Course.view.CourseView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CoursePresenterImplement<V : CourseView, I : CourseInteractor> @Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = interactor,
        schedulerProvider = schedulerProvider,
        compositeDisposable = disposable
    ),
    CoursePresenter<V, I> {

    private var resultArticle: ResultDetailCourse? = null

    override fun callDetaiCourse(id: Long) {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getDetailCourse(id = id).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet))
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }

                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.resultArticle = response.result
                        getView()?.successCallApi()
                    },
                    {err->
                        getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                    })
            )
        }
    }

    override fun getNameCourse(): String? {
        return resultArticle?.name
    }

    override fun getContentHtml(): String? {
        return resultArticle?.contentsHTML
    }

    override fun isShowThumnailIsYouTube(): Boolean {
        return !resultArticle?.codeYoutobe.isNullOrEmpty()
    }

    override fun getCodeYoutube(): String? {
        return resultArticle?.codeYoutobe
    }

    override fun getLinkThumbnail(): String? {
        return resultArticle?.img
    }

    override fun getSizeInClass():String {
        return resultArticle?.sizeText ?: "0 - 0"
    }

    override fun getRate(): Double {
        return resultArticle?.rate ?: 0.0
    }

    override fun getIsFree(): Boolean {
        return resultArticle?.isFree ?: false
    }

    override fun getPrice(): String? {
        return resultArticle?.price3Text
    }

    override fun getOrginalPrice(): String? {
        return resultArticle?.priceText
    }

    override fun getIDCourse(): String? {
        return resultArticle?.groupIDText
    }

    override fun getCountViews(): String? {
        return resultArticle?.countViewText
    }

    override fun getListLesson(): List<Lstakdlessonvm> {
        return resultArticle?.tutorialdetailsvm?.lstakdlessonvm ?: arrayListOf()
    }

    override fun getListTeacher(): List<Lstakdteachervm> {
        return resultArticle?.lstakdteachervm ?: arrayListOf()
    }
}