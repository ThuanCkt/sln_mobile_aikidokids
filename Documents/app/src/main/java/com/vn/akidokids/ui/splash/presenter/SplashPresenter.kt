package com.vn.akidokids.ui.splash.presenter

import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.ui.splash.interactor.SplashInteractor
import com.vn.akidokids.ui.splash.view.SplashView

interface SplashPresenter<V : SplashView, I : SplashInteractor> : MVPPresenter<V, I>