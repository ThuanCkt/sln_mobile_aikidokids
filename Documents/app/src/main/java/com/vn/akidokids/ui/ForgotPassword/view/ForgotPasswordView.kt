package com.vn.akidokids.ui.ForgotPassword.view

import com.vn.akidokids.ui.base.view.MVPView

interface ForgotPasswordView:MVPView {
    fun showValidMessage(message:String)
}