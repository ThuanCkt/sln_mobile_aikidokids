package com.vn.akidokids.ui.main.ListCourse.interacror

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.ListCourseResponse
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ListCourseInteractorImplement @Inject constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper): BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), ListCourseInteractor {

    override fun getListCourse(): Observable<ListCourseResponse> {
        return apiHelper.performServerListCourse()
    }
}