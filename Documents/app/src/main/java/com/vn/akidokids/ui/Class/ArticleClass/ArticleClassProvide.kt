package com.vn.akidokids.ui.Course.LessonCourse

import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ArticleClassProvide {
    @ContributesAndroidInjector(modules = [(ArticleClassModule::class)])
    abstract internal fun provideLessonCourseFragment(): ArticleClassFragment

}