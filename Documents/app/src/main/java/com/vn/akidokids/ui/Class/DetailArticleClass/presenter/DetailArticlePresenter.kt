package com.vn.akidokids.ui.Class.DetailArticleClass.presenter

import com.vn.akidokids.ui.Class.DetailArticleClass.interactor.DetailArticleClassInteractor
import com.vn.akidokids.ui.Class.DetailArticleClass.view.DetailArticleView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface DetailArticlePresenter<V:DetailArticleView, I: DetailArticleClassInteractor>:MVPPresenter<V,I> {
    fun callApiDetailArticle(id:Long)
    fun getHtml():String
    fun getTitle():String
    fun getDateCreate():String
}