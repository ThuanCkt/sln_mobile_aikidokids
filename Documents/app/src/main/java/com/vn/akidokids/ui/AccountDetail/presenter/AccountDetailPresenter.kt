package com.vn.akidokids.ui.AccountDetail.presenter

import com.vn.akidokids.data.network.model.ResultUserInfor
import com.vn.akidokids.ui.AccountDetail.interactor.AccountDetailInteractor
import com.vn.akidokids.ui.AccountDetail.view.AccountDetailView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface AccountDetailPresenter<V:AccountDetailView, I:AccountDetailInteractor>:MVPPresenter<V, I> {
    fun callInformationUser()
    fun getDataUserInformation():ResultUserInfor?
    fun clearLinkImage()
}