package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment

import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.MethodBankingProvider
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.DownPaymentProvider
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MethodPaymentProvider {

    @ContributesAndroidInjector(modules = [(MethodPaymentModule::class), (MethodBankingProvider::class), (DownPaymentProvider::class)])
    abstract internal fun provideMethodPaymentFragment(): MethodPaymentDialog
}