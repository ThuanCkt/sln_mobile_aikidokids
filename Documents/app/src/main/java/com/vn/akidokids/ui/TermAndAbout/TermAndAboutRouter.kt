package com.vn.akidokids.ui.TermAndAbout

import com.vn.akidokids.ui.TermAndAbout.interactor.TermAndAboutInteractor
import com.vn.akidokids.ui.TermAndAbout.interactor.TermAndAboutInteractorImplement
import com.vn.akidokids.ui.TermAndAbout.presenter.TermAndAboutPresenter
import com.vn.akidokids.ui.TermAndAbout.presenter.TermAndAboutPresenterImplement
import com.vn.akidokids.ui.TermAndAbout.view.TermAndAboutView
import dagger.Module
import dagger.Provides

@Module
class TermAndAboutRouter {

    @Provides
    fun provideInteractor(interactor: TermAndAboutInteractorImplement): TermAndAboutInteractor =
        interactor

    @Provides
    fun providePresenter(presenter: TermAndAboutPresenterImplement<TermAndAboutView, TermAndAboutInteractor>): TermAndAboutPresenter<TermAndAboutView, TermAndAboutInteractor> =
        presenter
}