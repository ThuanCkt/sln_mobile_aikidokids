package com.vn.akidokids.ui.AccountDetail.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ResultUserInfor
import com.vn.akidokids.ui.AccountDetail.interactor.AccountDetailInteractor
import com.vn.akidokids.ui.AccountDetail.view.AccountDetailView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class AccountDetailPresenterImplement<V : AccountDetailView, I : AccountDetailInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    dipose: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = dipose
), AccountDetailPresenter<V, I> {


    private var userInfor: ResultUserInfor? = null

    override fun callInformationUser() {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getUserInfor().compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallUserInfor(AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet))
                            return@subscribe
                        }

                        if (response.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }
                            getView()?.failCallUserInfor(response.result.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.userInfor = response.result
                        this.getView()?.getSuccesUserInfor()
                    }, {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                        err?.let {
                            getView()?.failCallUserInfor(it.errorBody)
                        }
                    })
            )
        }
    }

    override fun getDataUserInformation(): ResultUserInfor? {
        return userInfor
    }

    override fun clearLinkImage() {
        interactor?.clearLinkImage()
    }
}