package com.vn.akidokids.ui.DetailArticle.view

import com.vn.akidokids.ui.base.view.MVPView

interface DetailArticleView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi()
}