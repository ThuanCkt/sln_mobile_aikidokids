package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MethodPayment
import com.vn.akidokids.ui.base.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_method_banking.*
import javax.inject.Inject

class MethodBankingFragment : BaseFragment(), MethodBankingView {
    // TODO: Rename and change types of parameters

    @Inject
    internal lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    internal lateinit var adapter: MethodBankingAdapter

    private var listMethod: List<MethodPayment> = arrayListOf()

    companion object {
        fun newInstance(): MethodBankingFragment =
            MethodBankingFragment()
    }

    fun setListBanks(listMethod: List<MethodPayment>) {
        this.listMethod = listMethod
        adapter.setListBank(this.listMethod)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter.setListBank(this.listMethod)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_method_banking, container, false)
    }

    override fun setUp() {
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        rcMethodBanking.itemAnimator = DefaultItemAnimator()
        rcMethodBanking.layoutManager = linearLayoutManager
        rcMethodBanking.adapter = adapter
    }
}
