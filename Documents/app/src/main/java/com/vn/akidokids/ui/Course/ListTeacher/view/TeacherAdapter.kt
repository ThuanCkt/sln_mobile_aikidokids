package com.vn.akidokids.ui.Course.ListTeacher.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Lstakdteachervm
import com.vn.akidokids.util.EMPTY_STRING
import kotlinx.android.synthetic.main.item_teacher.view.*

class TeacherAdapter:RecyclerView.Adapter<TeacherAdapter.TeacherHolder>(){

    private var listTeacher:List<Lstakdteachervm> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherHolder {
       return TeacherHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_teacher, parent,false))
    }

    override fun getItemCount(): Int {
        return listTeacher.count()
    }

    fun setListTeacher(listTeachers:List<Lstakdteachervm>) {
        this.listTeacher = listTeachers
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: TeacherHolder, position: Int) {
        holder.clear()
        holder.bindData(position)
    }

    inner class TeacherHolder(view:View):RecyclerView.ViewHolder(view) {
        fun clear() {
            itemView.lbNameTeacher.text = EMPTY_STRING
            itemView.lbPhone.text = EMPTY_STRING
        }

        fun bindData(position: Int) {
            listTeacher[position].name?.let{
                itemView.lbNameTeacher.text = it
            }
            listTeacher[position].phone?.let{
                itemView.lbPhone.text = "Số điện thoại: $it"
            }
        }
    }
}