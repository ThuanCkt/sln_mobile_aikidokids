package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.presenter

import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.interactor.MethodDownPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MethodDownPaymentPresenterImplement<V:MethodDownPaymentView, I:MethodDownPaymentInteractor>
@Inject internal constructor(interactor: I, providerSchedulerProvider: SchedulerProvider, disposable: CompositeDisposable):
BasePresenter<V,I>(interactor = interactor, schedulerProvider = providerSchedulerProvider, compositeDisposable = disposable), MethodDownPaymentPresenter<V, I>{
}