package com.vn.akidokids.ui.Course.LessonCourse.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Lstakdlessonvm
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.FORMAT_DATE_APEARANCE
import com.vn.akidokids.util.FORMAT_DATE_API
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.loadImage
import com.vn.akidokids.util.extension.toDateString
import kotlinx.android.synthetic.main.item_cause.view.*

class LessonCourseAdapter(private var listTutorial: List<Lstakdlessonvm>, private var delegate:LessonCourseAdapter.LessonCourseAdapterInterface):RecyclerView.Adapter<LessonCourseAdapter.LessonCourseHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonCourseHolder {
        return LessonCourseHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_lesson_cause,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listTutorial.count()
    }

    override fun onBindViewHolder(holder: LessonCourseHolder, position: Int) {
        holder.clear()
        holder.bindData(position = position)
    }

    fun setListLesson(listTutorial: List<Lstakdlessonvm>) {
        this.listTutorial = listTutorial;
        notifyDataSetChanged()
    }

    inner class LessonCourseHolder(view:View):RecyclerView.ViewHolder(view){

        fun clear() {
            itemView.lbTitle.text = EMPTY_STRING
            itemView.lbDate.text = EMPTY_STRING
            itemView.imgThumbnail.setImageResource(0)
        }

        fun bindData(position:Int) {
            listTutorial[position].name.let {
                itemView.lbTitle.text = it
            }

            listTutorial[position].createdDate?.let {
                val date = it.convertStringToDate(FORMAT_DATE_API)
                itemView.lbDate.text = date?.toDateString(FORMAT_DATE_APEARANCE)
            }

            listTutorial[position].img?.let {
                itemView.imgThumbnail.loadImage(it)
            }

            itemView.setOnClickListener {
                if (listTutorial[position].codeYoutobe.isNullOrEmpty()) {
                    delegate.openDetailActivity(listTutorial[position])
                    return@setOnClickListener
                }

                delegate.openVideoActivity(listTutorial[position]?.codeYoutobe ?: EMPTY_STRING)
            }
        }
    }

    interface LessonCourseAdapterInterface {
        fun openVideoActivity(code:String)
        fun openDetailActivity(lesson:Lstakdlessonvm)
    }
}