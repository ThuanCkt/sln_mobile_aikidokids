package com.vn.akidokids.ui.Class.ClassActivity.view

import com.vn.akidokids.ui.base.view.MVPView

interface ClassView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi()
}