package com.vn.akidokids.ui.base.view

interface MVPView {
    fun showProgress()

    fun hideProgress()
}