package com.vn.akidokids.ui.DetailArticle.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ResultDetailArticle
import com.vn.akidokids.ui.DetailArticle.interactor.DetailArticleInteractor
import com.vn.akidokids.ui.DetailArticle.view.DetailArticleView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.FORMAT_DATE_API
import com.vn.akidokids.util.FORMAT_FULL_DATE
import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.toDateString
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject


class DetailArticlePresenterImplement<V : DetailArticleView, I : DetailArticleInteractor>
@Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    dispose: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = interactor,
        schedulerProvider = schedulerProvider,
        compositeDisposable = dispose
    ), DetailArticlePresenter<V, I> {

    private var detailArticle: ResultDetailArticle? = null

    override fun getDetailArticle(id: Long) {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getDetailArticle(id)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response.result?.isshow == true) {

                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }

                            getView()?.failCallApi(response.result.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        detailArticle = response.result

                        getView()?.successCallApi()
                    }, {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                        getView()?.failCallApi(
                            AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet
                            )
                        )
                    })
            )

        }
    }

    override fun getContentHtml():String? {
        return detailArticle?.contentsHTML
    }

    override fun getListRelateArticle(): List<ResultDetailArticle>? {
        return detailArticle?.lstakdarticlerelated
    }

    override fun getTitleArticle(): String? {
        return detailArticle?.title
    }

    override fun getTextDateCreateArticle(): String? {
        val date = detailArticle?.createdDate?.convertStringToDate(FORMAT_DATE_API)
        date?.let {
           val fullDate = it.toDateString(FORMAT_FULL_DATE)
            val daysArray = arrayOf(
                "Chủ nhật",
                "Thứ 2",
                "Thứ 3",
                "Thứ 4",
                "Thứ 5",
                "Thứ 6",
                "Thứ 7"
            )

            val calendar: Calendar = Calendar.getInstance()
            val day: Int = calendar.get(Calendar.DAY_OF_WEEK)

            return "${daysArray[day]} , $fullDate"
        }

        return EMPTY_STRING
    }
}