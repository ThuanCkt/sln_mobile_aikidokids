package com.vn.akidokids.ui.main.MainActivity.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.data.model.EnumMenuType
import com.vn.akidokids.data.model.MenuItem
import com.vn.akidokids.data.network.model.Category
import com.vn.akidokids.data.network.model.ResultUserInfor
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.ui.main.MainActivity.interactor.MainInteractor
import com.vn.akidokids.ui.main.MainActivity.view.MainView
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.util.Utils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenterImplement<V : MainView, I : MainInteractor>
@Inject internal constructor(
    interceptor: I,
    provider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = interceptor,
        schedulerProvider = provider,
        compositeDisposable = disposable
    ),
    MainPresenter<V, I> {

    override var listCategory: List<Category>? = null
    private var userInfor: ResultUserInfor? = null

    override fun onDrawerOptionLogoutClick() {
        this.interactor.let {
            it?.performUserLogout()
            this.getView()?.let {

            }
        }
    }

    override fun onAttach(view: V?) {
        super.onAttach(view)
        interactor?.let {
            compositeDisposable.add(
                it.getListCategory().compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { responseListCategory ->
                        if (responseListCategory.result == null) {
                            return@subscribe
                        }

                        if (responseListCategory.result?.isshow == true) {
                            return@subscribe
                        }

                        this.listCategory = responseListCategory.result?.data
                        this.getView()?.refeshDataCategory()
                    },
                    {
                       val err = it as? ANError

                    })
            )

           this.getInformationUser()
        }
    }

    override fun getInformationUser() {
        if(!Utils.isLogin()) {
            return
        }
        interactor?.let {
            compositeDisposable.add(
                it.getUserInfor().compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        if (response.result == null) {
                            return@subscribe
                        }

                        if (response.result?.isshow == true) {
                            return@subscribe
                        }

                        if (response.result?.msgcode != null) {
                            handleError(response.result?.msgcode ?: EMPTY_STRING)
                        }

                        this.userInfor = response.result
                        this.getView()?.getSuccesUserInfor()
                    }, {
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                        }
                    })
            )
        }
    }

    override fun getCidOnPosition(position: Int): Int {
        if (position >= this.listCategory?.count() ?: 0) {
            return 0;
        }

        return this.listCategory?.get(position)?.id?.toInt() ?: 0
    }

    override fun getGroupMenuIdOnPosition(position: Int): String {
        if (position >= this.listCategory?.count() ?: 0) {
            return "";
        }

        return this.listCategory?.get(position)?.groupmenuid.toString()
    }

    override fun getFullname(): String {
        return this.userInfor?.displaynametext ?: EMPTY_STRING
    }

    override fun getEmail(): String {
        return this.userInfor?.email ?: EMPTY_STRING
    }

    override fun getPoint(): String {
        return this.userInfor?.pointtext ?: EMPTY_STRING
    }

    override fun getImageAvatar(): String {
        return this.userInfor?.avatar ?: EMPTY_STRING
    }

    override fun getListClass(): List<MenuItem> {
        var listMenuItem:MutableList<MenuItem> = arrayListOf()

        for (item in userInfor?.lstclassvm ?: arrayListOf()) {
            var itemMenuItem = MenuItem(item.name, 0, menuType = EnumMenuType.CLASS, id = item.groupID)
            listMenuItem.add(itemMenuItem)
        }
        return listMenuItem
    }
}