package com.vn.akidokids.ui.LoginAndRegister.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.vn.akidokids.R
import com.vn.akidokids.ui.LoginAndRegister.interactor.LoginAndRegisterInteractor
import com.vn.akidokids.ui.LoginAndRegister.presenter.LoginAndRegisterPresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.ParameterAttribute
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject



class LoginAndRegisterActivity : BaseActivity(), LoginAndRegisterView, HasAndroidInjector {
    companion object {
        var isLoginFist:Boolean = true
    }
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    lateinit var navController:NavController

    @Inject
    lateinit var presenter: LoginAndRegisterPresenter<LoginAndRegisterView, LoginAndRegisterInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LoginAndRegisterActivity.isLoginFist = intent.getBooleanExtra(ParameterAttribute.IS_LOGIN.name, true)
        setContentView(com.vn.akidokids.R.layout.activity_login_andregister)
        navController = findNavController(com.vn.akidokids.R.id.nav_host_fragment)
        if (!LoginAndRegisterActivity.isLoginFist) {
            navController.navigate(R.id.action_navigation_login_to_navigation_register)
        }
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }
}
