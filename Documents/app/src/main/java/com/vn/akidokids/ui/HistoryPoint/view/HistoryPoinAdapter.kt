package com.vn.akidokids.ui.HistoryPoint.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.HistoryTransactionData
import com.vn.akidokids.ui.main.Category.view.ArticleAdapter
import kotlinx.android.synthetic.main.item_history_point.view.*

class HistoryPoinAdapter(
    private var listHistory: MutableList<HistoryTransactionData>,
    val delegate: HistoryPoinAdapter.HistoryPoinAdapterInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val VIEW_TYPE_ITEM = 0
    val VIEW_TYPE_LOADING = 1

    var isLoading:Boolean = false

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            return HistoryPointHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_history_point,
                    parent,
                    false
                )
            )
        }

        return HistoryPointHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_loading, parent, false))

    }

    override fun getItemViewType(position: Int): Int {
        if ( this.listHistory.count() > position)
            return VIEW_TYPE_ITEM

        return VIEW_TYPE_LOADING
    }

    fun setListHistoryAndIsLoading(listHistory: List<HistoryTransactionData>, isLoading:Boolean) {
        this.isLoading = isLoading
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    fun setListHistory(listHistory: List<HistoryTransactionData>) {
        this.listHistory.clear()
        this.listHistory.addAll(listHistory)
        notifyDataSetChanged()
    }

    fun isLoadMore(): Boolean {
        if (this.listHistory.count() <= 4) {
            return true
        }
        return false

    }

    override fun getItemCount(): Int {
        if (this.isLoading)
            return listHistory.count() + 1

        return listHistory.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (this.getItemViewType(position)) {
            VIEW_TYPE_ITEM -> {
                val holder = holder as? HistoryPoinAdapter.HistoryPointHolder
                holder?.let {
                    it.bindData(position)
                }
            }

            VIEW_TYPE_LOADING -> {
                holder as? ArticleAdapter.LoadingViewHolder
            }

            else -> return
        }
    }

    inner class HistoryPointHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindData(position: Int) {
            listHistory[position].accountName?.let {
                itemView.lbAccount.text = it
            }

            listHistory[position].pointSendText?.let {
                itemView.lbPointTransaction.text = it
            }

            listHistory[position].pointAfterSendText?.let {
                itemView.lbBlance.text = it
            }

            listHistory[position].tranTime?.let {
                itemView.lbTime.text = it
            }

            listHistory[position].note?.let {
                itemView.lbNote.text = it
            }

            itemView.setOnClickListener {
                delegate.onClickItem(position)
            }
        }
    }

    inner class LoadingViewHolder(view:View):RecyclerView.ViewHolder(view) {

    }

    interface HistoryPoinAdapterInterface {
        fun onClickItem(index: Int)
    }
}