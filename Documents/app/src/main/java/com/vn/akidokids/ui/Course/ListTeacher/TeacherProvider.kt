package com.vn.akidokids.ui.Course.ListTeacher

import com.vn.akidokids.ui.Course.ListTeacher.view.TeacherFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TeacherProvider {
    @ContributesAndroidInjector(modules = [(TeacherModule::class)])
    internal abstract fun provideTeacherFragment():TeacherFragment
}