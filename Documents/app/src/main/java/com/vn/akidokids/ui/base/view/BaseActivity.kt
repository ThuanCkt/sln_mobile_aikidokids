package com.vn.akidokids.ui.base.view

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.ui.SectionExpired.SectionExpiredDialog
import com.vn.akidokids.util.CommonUtil
import dagger.android.AndroidInjection
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class BaseActivity : AppCompatActivity(), MVPView, BaseFragment.CallBack {

    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        performDI()
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        AikidoKidsApplication.setCurrentActivity(this)
    }

    override fun onDestroy() {
        clearReferences()
        super.onDestroy()
    }

    override fun onPause() {
        clearReferences()
        super.onPause()
    }

    override fun hideProgress() {
        progressDialog?.let { if (it.isShowing) it.cancel() }
    }

    override fun showProgress() {
        hideProgress()
        progressDialog = CommonUtil.showLoadingDialog(this)
    }

    private fun performDI() = AndroidInjection.inject(this)

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    fun showMessageToast(string: String) {
        val toast = Toast.makeText(this,string, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    private fun clearReferences() {
        val currActivity = AikidoKidsApplication.activityCurrent
        if (this == currActivity)
            AikidoKidsApplication.setCurrentActivity(null)
    }

    fun showFragmentLogout() {
        SectionExpiredDialog.newInstance()?.let{
            it?.show(supportFragmentManager, "Expired Payment")
        }
    }
}