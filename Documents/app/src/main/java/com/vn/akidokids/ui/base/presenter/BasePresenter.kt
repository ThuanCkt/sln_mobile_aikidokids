package com.vn.akidokids.ui.base.presenter

import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.ui.base.view.MVPView
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V : MVPView, I : MVPInteractor> internal constructor(protected var interactor: I?, protected val schedulerProvider: SchedulerProvider, protected val compositeDisposable: CompositeDisposable) : MVPPresenter<V, I> {

    private var view: V? = null
    private val isViewAttached: Boolean get() = view != null

    override fun onAttach(view: V?) {
        this.view = view
    }

    override fun getView(): V? = view

    override fun onDetach() {
        compositeDisposable.dispose()
        view = null
        interactor = null
    }

    open fun isUserLoggedIn(): Boolean {
        return true
    }

     fun performUserLogout() {
    }

    fun handleError(msCode:String): Boolean {
        if (msCode == "401" || msCode == "403") {
             AikidoKidsApplication.activityCurrent?.let {
                 if (it is BaseActivity) {
                     it.showFragmentLogout()
                     return true
                 }
             }
        }

        return false
    }
}