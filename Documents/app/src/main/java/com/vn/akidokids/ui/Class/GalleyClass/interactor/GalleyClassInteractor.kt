package com.vn.akidokids.ui.Class.GalleyClass.interactor

import com.vn.akidokids.data.network.model.AlbumClassResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface GalleyClassInteractor:MVPInteractor {
    fun getGalley(id:Long):Observable<AlbumClassResponse>
}