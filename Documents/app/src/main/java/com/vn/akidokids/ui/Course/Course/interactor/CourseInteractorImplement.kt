package com.vn.akidokids.ui.Course.Course.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.DetailCourseResponse
import com.vn.akidokids.data.network.request.CourseRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class CourseInteractorImplement @Inject constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper):
    BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    CourseInteractor {

    override fun getDetailCourse(id:Long): Observable<DetailCourseResponse> {
        return apiHelper.performServerDetailCourse(CourseRequest.DetailCourseRequest(id))
    }
}