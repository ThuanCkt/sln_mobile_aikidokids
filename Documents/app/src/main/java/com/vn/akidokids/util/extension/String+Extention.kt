package com.vn.akidokids.util.extension

import android.text.Editable
import java.text.SimpleDateFormat
import java.util.*

internal fun String.convertStringToDate(formatDate:String):Date? {
    val parser =  SimpleDateFormat(formatDate)
    return parser.parse(this)
}

fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)
