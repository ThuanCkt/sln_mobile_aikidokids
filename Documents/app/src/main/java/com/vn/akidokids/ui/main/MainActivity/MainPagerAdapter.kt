package com.vn.akidokids.ui.main.MainActivity

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.vn.akidokids.ui.main.Category.view.CategoryFragment
import com.vn.akidokids.ui.main.ListCourse.view.ListCourseFragment

class MainPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    private var tabCount = 0

    val categoryFragment = CategoryFragment.newInstance()

    val listCauseFragment = ListCourseFragment.newInstance()

    override fun getCount(): Int {
        return tabCount
    }

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> this.categoryFragment
            1 -> this.listCauseFragment
            else -> null
        }
    }

    internal fun setCount(count: Int) {
        this.tabCount = count
    }

}
