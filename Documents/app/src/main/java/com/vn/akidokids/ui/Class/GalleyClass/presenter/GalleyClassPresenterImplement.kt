package com.vn.akidokids.ui.Class.GalleyClass.presenter

import android.util.Log
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.AlbumClassData
import com.vn.akidokids.ui.Class.GalleyClass.interactor.GalleyClassInteractor
import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class GalleyClassPresenterImplement<V : GalleyClassView, I : GalleyClassInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    dispose: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = dispose
), GalleyClassPresenter<V, I> {

    var listAlbum:List<AlbumClassData> = arrayListOf()

    override fun callApiGalley(idClass:Long) {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getGalley(
                  id = idClass
                ).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }
                        this.listAlbum = response.result?.data ?: arrayListOf()
                        getView()?.successCallApi()
                    },
                    { err ->
                        getView()?.hideProgress()
                        Log.e("ahihi", err.toString())
                    })
            )
        }
    }

    override fun getListGalley(): List<AlbumClassData> {
        return this.listAlbum
    }
}