package com.vn.akidokids.ui.HistoryPoint.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.DeteleHistoryResponse
import com.vn.akidokids.data.network.model.HistoryTransactionResponse
import com.vn.akidokids.data.network.request.HistoryTransactionRequest
import com.vn.akidokids.data.preferences.AppPreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class HistoryPointInteractorImplement @Inject constructor(
    preferenceHelper: AppPreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    HistoryPointInteractor {

    override fun getListHistory(page: Int, sizePage: Int): Observable<HistoryTransactionResponse> {
        return apiHelper.performServerHistoryPoint(
            HistoryTransactionRequest.ServerListHistoryTransactionRequest(
                page = page,
                pageSize = sizePage
            )
        )
    }

    override fun deleteHistoryPoint(data: HistoryTransactionRequest.DeleteTransaction): Observable<DeteleHistoryResponse> {
        return apiHelper.performServerDeleteHistoryPoint(data)
    }
}