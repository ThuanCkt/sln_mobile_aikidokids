package com.vn.akidokids.ui.Course.LessonCourse.view

import com.vn.akidokids.ui.base.view.MVPView

interface GalleyClassView :MVPView {
    fun failCallApi(message:String)
    fun successCallApi()
}
