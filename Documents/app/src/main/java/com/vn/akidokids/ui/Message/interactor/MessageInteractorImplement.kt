package com.vn.akidokids.ui.Message.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.DeteleHistoryResponse
import com.vn.akidokids.data.network.model.MessageTransactionResponse
import com.vn.akidokids.data.network.request.MessageTransactionRequest
import com.vn.akidokids.data.preferences.AppPreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class MessageInteractorImplement @Inject constructor(
    preferenceHelper: AppPreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), MessageInteractor {

    override fun getListHistory(page: Int, pageSize: Int): Observable<MessageTransactionResponse> {
        return apiHelper.performServerMessage(
            request = MessageTransactionRequest.ServerListMessageTransactionRequest(
                page = page,
                pageSize = pageSize
            )
        )
    }

    override fun deleteMessage(data: MessageTransactionRequest.DeleteTransaction): Observable<DeteleHistoryResponse> {
        return apiHelper.performServerDeleteMessage(data)
    }
}