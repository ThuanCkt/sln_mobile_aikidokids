package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.vn.akidokids.R
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.MethodPagerAdapter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor.MethodPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.presenter.MethodPaymentPresenter
import com.vn.akidokids.ui.base.view.BaseDialogView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.fragment_method_payment.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class MethodPaymentDialog : BaseDialogView(), MethodPaymentView, HasAndroidInjector {

    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    internal lateinit var methodPagerAdapter: MethodPagerAdapter

    @Inject
    internal lateinit var presenter:MethodPaymentPresenter<MethodPaymentView, MethodPaymentInteractor>

    companion object {
        fun newInstance(): MethodPaymentDialog? {
            return MethodPaymentDialog()
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_method_payment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
        presenter.getMethodPayment()
        setUpFeedPagerAdapter()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    private fun setUpFeedPagerAdapter() {
        methodPagerAdapter.count = 3
        tabLayout.addTab(tabLayout.newTab().setText("Chuyển khoản"))
        tabLayout.addTab(tabLayout.newTab().setText("Trực tiếp tại VP"))
        tabLayout.addTab(tabLayout.newTab().setText("Điểm"))
        viewPage.adapter = methodPagerAdapter
        viewPage.offscreenPageLimit = tabLayout.tabCount;
        viewPage.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPage.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
        viewPage.setPagingEnabled(false)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return  activityDispatchingAndroidInjector;
    }

    //delegate view

    override fun failCallApi(string: String) {
        this.showMessageToast(string)
    }

    override fun successCallApi() {
        this.methodPagerAdapter.methodBank.setListBanks(presenter.getListBank())
        this.methodPagerAdapter.methodDownPaymentFragment.setListDownPayment(presenter.getListDownPayment())
        this.methodPagerAdapter.methodPointFragment.setNote(presenter.getNotePoint())
    }
}
