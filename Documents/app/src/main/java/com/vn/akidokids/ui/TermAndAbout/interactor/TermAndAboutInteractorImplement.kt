package com.vn.akidokids.ui.TermAndAbout.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.TermAndAboutResponse
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class TermAndAboutInteractorImplement @Inject constructor(
    apiHelper: ApiHelper,
    preferenceHelper: PreferenceHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    TermAndAboutInteractor {

    override fun callTermAndAbout(): Observable<TermAndAboutResponse> {
        return apiHelper.performServerTermAndServer()
    }
}