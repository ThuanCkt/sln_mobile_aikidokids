package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse

import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor.RegisterCourseInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor.RegisterCourseInteractorImplement
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.presenter.RegisterCoursePresenter
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.presenter.RegisterCoursePresenterImplement
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view.RegisterCourseView
import dagger.Module
import dagger.Provides

@Module
class RegisterCourseModule {

    @Provides
    internal fun provideInteractor(interactor: RegisterCourseInteractorImplement):RegisterCourseInteractor = interactor

    @Provides
    internal fun providePresenter(presenter:RegisterCoursePresenterImplement<RegisterCourseView, RegisterCourseInteractor>):RegisterCoursePresenter<RegisterCourseView, RegisterCourseInteractor> = presenter
}