package com.vn.akidokids.ui.main.MainActivity.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Category
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter(categoryListItem:MutableList<Category>, val delegate: CategoryAdapterInterface): RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private val categoryListItem: MutableList<Category> = categoryListItem
    private var cid:Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder = CategoryViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.item_category, parent, false))

    override fun getItemCount(): Int = this.categoryListItem.size

    internal fun setCategoryToList(listCategory: List<Category>) {
        this.categoryListItem.clear()
        this.categoryListItem.addAll(listCategory)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.clear()
        holder.onBind(position, this.cid)
    }

    fun setCidActive(cid:Int) {
        this.cid = cid
        notifyDataSetChanged()
    }

    inner class CategoryViewHolder(view:View):RecyclerView.ViewHolder(view) {
        fun clear() {
            itemView.textCategory.text = ""
        }

        fun onBind(position: Int, cid: Int) {
            val category = categoryListItem[position]
            inflateData(category.name)
            setItemClickListener(position)
            category.id?.let {
                if (it.isEmpty()) {
                    return;
                }
                if (it.toInt() == cid) {
                    itemView.textCategory.setTextColor(AikidoKidsApplication.getAppResources().getColor(R.color.color_primary_light))
                } else {
                    itemView.textCategory.setTextColor(AikidoKidsApplication.getAppResources().getColor(R.color.color_text))
                }
            }
        }

        private fun setItemClickListener(index:Int) {
            itemView.setOnClickListener {
                val category = categoryListItem[position]
                if(category.id.isNullOrEmpty()) {
                    return@setOnClickListener
                }
                delegate.categoryClick(position = index)
            }
        }

        private fun inflateData(title: String?) {
            title?.let { itemView.textCategory.text = it }
        }
    }
}

interface CategoryAdapterInterface{
    fun categoryClick(position: Int)
}