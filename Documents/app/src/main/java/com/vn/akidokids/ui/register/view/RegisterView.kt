package com.vn.akidokids.ui.register.view

import com.vn.akidokids.ui.base.view.MVPView

interface RegisterView: MVPView {
    fun messageError(msg:String)
    fun openMainActivity()
}