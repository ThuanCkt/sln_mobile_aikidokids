package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view

import com.vn.akidokids.ui.base.view.MVPView

interface RegisterCourseView:MVPView {
    fun failCallApi(message:String)
    fun successCallApi()

    fun setTextGivePoint(string: String)
    fun setOriginalText(string: String)
    fun setDiscountText(string: String)
    fun setTotaltext(string: String)
    fun setTextUserRefer(string: String)

}