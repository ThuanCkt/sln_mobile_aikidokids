package com.vn.akidokids.ui.Class.DetailArticleClass.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.DetailArticleClassResponse
import com.vn.akidokids.data.network.request.ClassRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class DetailArticleClassInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper) , DetailArticleClassInteractor{

    override fun callApiArticle(id:Long): Observable<DetailArticleClassResponse> {
        return apiHelper.performServerDetailArticleClass(ClassRequest.DetailClassRequest(id))
    }

}