package com.vn.akidokids.ui.main.Category

import com.vn.akidokids.ui.main.Category.interactor.CategoryInteractor
import com.vn.akidokids.ui.main.Category.interactor.CategoryInteractorImplement
import com.vn.akidokids.ui.main.Category.presenter.CategoryPresenter
import com.vn.akidokids.ui.main.Category.presenter.CategoryPresenterImplement
import com.vn.akidokids.ui.main.Category.view.ArticleAdapter
import com.vn.akidokids.ui.main.Category.view.CategoryFragment
import com.vn.akidokids.ui.main.Category.view.CategoryView
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class CategoryModule {

    @Provides
    internal fun provideInteractor(interactor: CategoryInteractorImplement):CategoryInteractor = interactor

    @Provides
    internal fun providePresenter(presenter:CategoryPresenterImplement<CategoryView, CategoryInteractor>):
            CategoryPresenter<CategoryView, CategoryInteractor> = presenter

    @Provides
    internal fun provideArticleAdapter(fragment: CategoryFragment): ArticleAdapter = ArticleAdapter(ArrayList(), fragment)

}