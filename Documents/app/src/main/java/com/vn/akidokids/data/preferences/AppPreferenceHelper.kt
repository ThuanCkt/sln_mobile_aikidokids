package com.vn.akidokids.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.content.edit
import com.google.gson.Gson
import com.vn.akidokids.data.network.model.UserEntity
import com.vn.akidokids.di.PreferenceInfo
import com.vn.akidokids.util.AppConstants
import javax.inject.Inject


/**
 * Created by jyotidubey on 04/01/18.
 */
class AppPreferenceHelper @Inject constructor(
    context: Context,
    @PreferenceInfo private val prefFileName: String
) : PreferenceHelper {
    companion object {
         val PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE"
         val PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID"
         val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
         val PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME"
         val PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL"
         val PREF_KEY_IS_REMEMBER = "PREF_KEY_IS_REMEMBER"
         val PREF_KEY_IS_USER = "PREF_KEY_IS_USER"
        val AVARTAR = "PREF_KEY_AVARTAR"
    }

    private val mPrefs: SharedPreferences =
        context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    override fun getCurrentUserLoggedInMode() = mPrefs.getInt(
        PREF_KEY_USER_LOGGED_IN_MODE,
        AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type
    )

    override fun getCurrentUserName(): String =
        mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, "ABC").toString()

    override fun setCurrentUserName(userName: String?) =
        mPrefs.edit { putString(PREF_KEY_CURRENT_USER_NAME, userName) }

    override fun getCurrentUserEmail(): String =
        mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, "abc@gmail.com").toString()

    override fun setCurrentUserEmail(email: String?) =
        mPrefs.edit { putString(PREF_KEY_CURRENT_USER_EMAIL, email) }

    override fun getAccessToken(): String{
       return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "").toString()
    }

    override fun setAccessToken(accessToken: String?) =
        mPrefs.edit {
            putString(PREF_KEY_ACCESS_TOKEN, accessToken)
            commit()
        }

    override fun setIsRemember(isRemember: Boolean) {
        mPrefs.edit {
            putBoolean(PREF_KEY_IS_REMEMBER, isRemember)
        }
    }

    override fun getIsRemember(): Boolean {
        return mPrefs.getBoolean(PREF_KEY_IS_REMEMBER, false)
    }

    override fun getCurrentUserId(): Long? {
        val userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX)
        return when (userId) {
            AppConstants.NULL_INDEX -> null
            else -> userId
        }
    }

    override fun setCurrentUserId(userId: String?) {
        mPrefs.edit { putString(PREF_KEY_CURRENT_USER_ID, userId) }
    }

    override fun setCurrentUserLoggedInMode(mode: AppConstants.LoggedInMode) {
        mPrefs.edit { putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.type) }
    }

    override fun setUserEntity(user: UserEntity?) {
        val gson = Gson();
        val json = gson.toJson(user)
        mPrefs.edit { putString(PREF_KEY_IS_USER, json) }
    }

    override fun getUserEntity(): UserEntity {
        val gson = Gson()
        val json = mPrefs.getString(PREF_KEY_IS_USER, "")
        val obj = gson.fromJson<Any>(json, UserEntity::class.java)
        return obj as UserEntity
    }

    override fun setLinkImage(link: String)  = mPrefs.edit {
        putString(AVARTAR, link)
        commit()
    }


}