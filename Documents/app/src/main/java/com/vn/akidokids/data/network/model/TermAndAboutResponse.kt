package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TermAndAboutResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: TermAndAboutResult? = null
)

data class TermAndAboutResult (
    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean? = false,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("settingtype")
    val settingtype: String? = null,

    @Expose
    @SerializedName("cultureinfocode")
    val cultureinfocode: String? = null,

    @Expose
    @SerializedName("about")
    val about: String? = null,

    @Expose
    @SerializedName("terms")
    val terms: String? = null,

    @Expose
    @SerializedName("input_points")
    val inputPoints: String? = null
)
