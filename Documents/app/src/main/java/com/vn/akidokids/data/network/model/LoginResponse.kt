package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jyotidubey on 11/01/18.
 */
data class LoginResponse(
    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String? = null,

    @Expose
    @SerializedName("result")
    var result: ResultUser? = null
)

data class ResultUser(
    @Expose
    @SerializedName("token")
    var token: String? = null,

    @Expose
    @SerializedName("user")
    var user: UserEntity? = null,

    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean? = false,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("data")
    var data: Any? = null
)


data class UserEntity(
    @Expose
    @SerializedName("token")
    var token: String? = null,

    @Expose
    @SerializedName("user")
    var user: UserEntity? = null,

    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean? = false,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("id")
    var id: String? = null,

    @Expose
    @SerializedName("email")
    var email: String? = null,

    @Expose
    @SerializedName("emailconfirmed")
    var emailConfirmed: Long,

    @Expose
    @SerializedName("passwordhash")
    var passwordHash: String? = null,

    @Expose
    @SerializedName("securitystamp")
    var securityStamp: String? = null,

    @Expose
    @SerializedName("phonenumber")
    var phoneNumber: String? = null,

    @Expose
    @SerializedName("phonenumberconfirmed")
    var phoneNumberConfirmed: Long,

    @Expose
    @SerializedName("twofactorenabled")
    var twoFactorEnaccessfailedcountabled: Long,

    @Expose
    @SerializedName("lockoutenddateutc")
    var lockoutenddateutc: String? = null,

    @Expose
    @SerializedName("lockoutenabled")
    var lockoutenabled: Long,

    @Expose
    @SerializedName("accessfailedcount")
    var accessfailedcount: Long,

    @Expose
    @SerializedName("username")
    var username: String? = null,

    @Expose
    @SerializedName("createddateutc")
    var createddateutc: String? = null,

    @Expose
    @SerializedName("passwordhash2")
    var passwordhash2: String? = null,

    @Expose
    @SerializedName("fullname")
    var fullname: String? = null,

    @Expose
    @SerializedName("displayname")
    var displayname: String? = null,

    @Expose
    @SerializedName("displaynametext")
    var displaynametext: String? = null,

    @Expose
    @SerializedName("avatar")
    var avatar: String? = null,

    @Expose
    @SerializedName("otptype")
    var otptype: String? = null,

    @Expose
    @SerializedName("birthday")
    var birthday: String? = null,

    @Expose
    @SerializedName("birthdaytext")
    var birthdaytext: String? = null,

    @Expose
    @SerializedName("sex")
    var sex: Long,

    @Expose
    @SerializedName("sextext")
    var sextext: String? = null,

    @Expose
    @SerializedName("address")
    var address: String? = null,

    @Expose
    @SerializedName("note")
    var note: String? = null,

    @Expose
    @SerializedName("providerid")
    var providerid: Long,

    @Expose
    @SerializedName("code")
    var code: String? = null,

    @Expose
    @SerializedName("usertype")
    var usertype: String? = null,

    @Expose
    @SerializedName("point")
    var point: Long,

    @Expose
    @SerializedName("pointtext")
    var pointtext: String? = null,

    @Expose
    @SerializedName("coin")
    var coin: Long,

    @Expose
    @SerializedName("usernumber")
    var usernumber: Long,

    @Expose
    @SerializedName("usercode")
    var usercode: String? = null,

    @Expose
    @SerializedName("provinceid")
    var provinceid: Long,

    @Expose
    @SerializedName("provincename")
    var provincename: String? = null,

    @Expose
    @SerializedName("deviceid")
    var deviceid: String? = null,

    @Expose
    @SerializedName("devicename")
    var devicename: String? = null,

    @Expose
    @SerializedName("tokenapns")
    var tokenapns: String? = null,

    @Expose
    @SerializedName("usergroup")
    var usergroup:String? = null
)