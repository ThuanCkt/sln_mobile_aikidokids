package com.vn.akidokids.ui.splash.view

import com.vn.akidokids.ui.base.view.MVPView

interface SplashView: MVPView{
    fun showSuccessToast()
    fun showErrorToast()
    fun openMainActivity()
    fun openLoginActivity()
}