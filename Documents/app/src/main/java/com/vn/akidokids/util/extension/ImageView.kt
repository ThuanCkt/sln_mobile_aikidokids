package com.vn.akidokids.util.extension

import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import com.bumptech.glide.Glide
import com.vn.akidokids.R

/**
 * Created by jyotidubey on 24/01/18.
 */
internal fun ImageView.loadImage(url: String) {
    Glide.with(this.context)
            .load(url)
            .asBitmap()
            .placeholder(R.drawable.im_thum_media)
            .centerCrop()
            .into(this)
}

internal fun ImageView.loadImageCenter(url: String) {
    Glide.with(this.context)
        .load(url)
        .asBitmap()
        .placeholder(R.drawable.im_thum_media)
        .fitCenter()
        .into(this)
}

internal  fun ImageView.loadImageVector(resourceId: Int) {
    val drawable = AppCompatResources.getDrawable(context, resourceId)
    this.setImageDrawable(drawable)
}