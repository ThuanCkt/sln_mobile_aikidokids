package com.vn.akidokids.ui.Class.ClassActivity

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.Class.ClassActivity.interactor.ClassInteractor
import com.vn.akidokids.ui.Class.ClassActivity.interactor.ClassInteractorImplement
import com.vn.akidokids.ui.Class.ClassActivity.presenter.ClassPresenter
import com.vn.akidokids.ui.Class.ClassActivity.presenter.ClassPresenterImplement
import com.vn.akidokids.ui.Class.ClassActivity.view.ClassActivity
import com.vn.akidokids.ui.Class.ClassActivity.view.ClassView
import com.vn.akidokids.ui.Class.ClassActivity.view.MemberClassAdapter
import dagger.Module
import dagger.Provides

@Module
class ClassModule {

    @Provides
    internal fun provideInteractor(interactor: ClassInteractorImplement):ClassInteractor = interactor

    @Provides
    internal fun providePresenter(presenter:ClassPresenterImplement<ClassView, ClassInteractor>):ClassPresenter<ClassView, ClassInteractor> = presenter

    @Provides
    internal fun provideAdapterMember():MemberClassAdapter = MemberClassAdapter()

    @Provides
    internal fun providerLinearLayoutManager(activity:ClassActivity):LinearLayoutManager = LinearLayoutManager(activity)
}