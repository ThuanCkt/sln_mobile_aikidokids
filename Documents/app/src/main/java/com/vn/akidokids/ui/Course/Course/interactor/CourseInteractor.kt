package com.vn.akidokids.ui.Course.Course.interactor

import com.vn.akidokids.data.network.model.DetailCourseResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface CourseInteractor:MVPInteractor {
    fun getDetailCourse(id:Long):Observable<DetailCourseResponse>
}