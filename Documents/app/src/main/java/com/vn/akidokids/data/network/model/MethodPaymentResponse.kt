package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MethodPaymentResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: ResultMethodPayment?
)

data class ResultMethodPayment (
    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("point_note")
    val pointNote: String? = null,

    @Expose
    @SerializedName("banks")
    val banks: List<MethodPayment>? = null,

    @Expose
    @SerializedName("organizations")
    val organizations: List<MethodPayment>? = null
)

data class MethodPayment (
    @Expose
    @SerializedName("id")
    val id: String,

    @Expose
    @SerializedName("group_id")
    val groupID: Long,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("slug")
    val slug: String? = null,

    @Expose
    @SerializedName("account_number")
    val accountNumber: String? = null,

    @Expose
    @SerializedName("branch_name")
    val branchName: String? = null,

    @Expose
    @SerializedName("account_owner")
    val accountOwner: String? = null,

    @Expose
    @SerializedName("note")
    val note: String? = null,

    @Expose
    @SerializedName("description")
    val description: Any? = null,

    @Expose
    @SerializedName("description_html")
    val descriptionHTML: Any? = null,

    @Expose
    @SerializedName("is_active")
    val isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    val isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    val createdDate: String,

    @Expose
    @SerializedName("created_by")
    val createdBy: Any? = null,

    @Expose
    @SerializedName("created_log")
    val createdLog: Any? = null,

    @Expose
    @SerializedName("modified_date")
    val modifiedDate: String,

    @Expose
    @SerializedName("modified_by")
    val modifiedBy: Any? = null,

    @Expose
    @SerializedName("modified_log")
    val modifiedLog: Any? = null,

    @Expose
    @SerializedName("mode")
    val mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    val userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    val modeaction: Long,

    @Expose
    @SerializedName("langid")
    val langid: Long,

    @Expose
    @SerializedName("langcode")
    val langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    val langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    val lstlang: Any? = null,

    @Expose
    @SerializedName("parent_id")
    val parentID: Long? = null,

    @Expose
    @SerializedName("phone")
    val phone: String? = null,

    @Expose
    @SerializedName("mobile")
    val mobile: Any? = null,

    @Expose
    @SerializedName("email")
    val email: String? = null,

    @Expose
    @SerializedName("address")
    val address: String? = null,

    @Expose
    @SerializedName("map_embed")
    val mapEmbed: Any? = null
)