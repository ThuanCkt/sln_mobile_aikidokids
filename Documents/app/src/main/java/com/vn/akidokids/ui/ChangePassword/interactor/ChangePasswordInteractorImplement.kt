package com.vn.akidokids.ui.ChangePassword.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.LoginResponse
import com.vn.akidokids.data.network.request.UserRequest
import com.vn.akidokids.data.preferences.AppPreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ChangePasswordInteractorImplement @Inject constructor(
    preferenceHelper: AppPreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    ChangePasswordInteractor {

    override fun callChangePassword(data: UserRequest.ChangePasswordRequest): Observable<LoginResponse> {
        return apiHelper.performServerChangePassword(data)
    }

    override fun setToken(token: String) {
        if (token.isEmpty()) {
            return
        }
        preferenceHelper.setAccessToken(token)
    }
}