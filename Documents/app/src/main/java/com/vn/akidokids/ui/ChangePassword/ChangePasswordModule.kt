package com.vn.akidokids.ui.ChangePassword

import com.vn.akidokids.ui.ChangePassword.interactor.ChangePasswordInteractor
import com.vn.akidokids.ui.ChangePassword.interactor.ChangePasswordInteractorImplement
import com.vn.akidokids.ui.ChangePassword.presenter.ChangePasswordPresenter
import com.vn.akidokids.ui.ChangePassword.presenter.ChangePasswordPresenterImplement
import com.vn.akidokids.ui.ChangePassword.view.ChangePasswordView
import dagger.Module
import dagger.Provides

@Module
class ChangePasswordModule {

    @Provides
    fun provideInteractor(interactor: ChangePasswordInteractorImplement): ChangePasswordInteractor =
        interactor

    @Provides
    fun providePresenter(presenter: ChangePasswordPresenterImplement<ChangePasswordView, ChangePasswordInteractor>): ChangePasswordPresenter<ChangePasswordView, ChangePasswordInteractor> =
        presenter
}