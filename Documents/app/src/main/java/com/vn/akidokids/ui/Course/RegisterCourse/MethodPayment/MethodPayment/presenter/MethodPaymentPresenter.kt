package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.presenter

import com.vn.akidokids.data.network.model.MethodPayment
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor.MethodPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface MethodPaymentPresenter<V: MethodPaymentView, I: MethodPaymentInteractor>:MVPPresenter<V, I> {

    fun getMethodPayment()
    fun getListBank():List<MethodPayment>
    fun getListDownPayment():List<MethodPayment>
    fun getNotePoint():String
}