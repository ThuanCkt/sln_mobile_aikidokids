package com.vn.akidokids.ui.register

import com.vn.akidokids.ui.register.interactor.RegisterInteractor
import com.vn.akidokids.ui.register.interactor.RegisterInteractorImplement
import com.vn.akidokids.ui.register.presenter.RegisterPresenter
import com.vn.akidokids.ui.register.presenter.RegisterPresenterImplement
import com.vn.akidokids.ui.register.view.RegisterView
import dagger.Module
import dagger.Provides

@Module
class RegisterModule {

    @Provides
    fun provideRegisterInteractor(interactor: RegisterInteractorImplement):RegisterInteractor = interactor

    @Provides
    fun provideRegisterPresenter(presenter: RegisterPresenterImplement<RegisterView, RegisterInteractor>):RegisterPresenter<RegisterView, RegisterInteractor> = presenter

}