package com.vn.akidokids.ui.Course.ListTeacher.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Lstakdteachervm
import com.vn.akidokids.ui.base.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_teacher.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class TeacherFragment : BaseFragment() {

    companion object {
      fun newInstance():TeacherFragment {
          return TeacherFragment()
      }
    }
    private var listTeacher:List<Lstakdteachervm> = arrayListOf()

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    @Inject
    internal lateinit var teacherAdapter: TeacherAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_teacher, container, false)
    }

    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rcTeacher.layoutManager = layoutManager
        rcTeacher.itemAnimator = DefaultItemAnimator()
        rcTeacher.adapter = teacherAdapter
        teacherAdapter.setListTeacher(listTeacher)
    }

    fun setListTeacher(listTeachers:List<Lstakdteachervm>) {
        this.listTeacher = listTeacher
        teacherAdapter.setListTeacher(listTeachers)
    }

}
