package com.vn.akidokids.data.network.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class AlbumClassResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: AlbumClassResult? = null)

data class AlbumClassResult (
    @Expose
    @SerializedName("isshow")
    val isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    val msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    val msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    val msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    val msgdetail: String? = null,

    @Expose
    @SerializedName("page")
    val page: Page? = null,

    @Expose
    @SerializedName("data")
    val data: List<AlbumClassData>? = null
)
@Parcelize
data class AlbumClassData (
    @Expose
    @SerializedName("id")
    val id: String? = null,

    @Expose
    @SerializedName("group_id")
    val groupID: Long? = null,

    @Expose
    @SerializedName("class_id")
    val classID: Long,

    @Expose
    @SerializedName("name")
    val name: String? = null,

    @Expose
    @SerializedName("name_gui")
    val nameGUI: String? = null,

    @Expose
    @SerializedName("slug")
    val slug: String? = null,

    @Expose
    @SerializedName("description")
    val description: String? = null,

    @Expose
    @SerializedName("img")
    val img: String? = null,

    @Expose
    @SerializedName("img_thumbnail")
    val imgThumbnail: String? = null,

    @Expose
    @SerializedName("img_with")
    val imgWith: Long,

    @Expose
    @SerializedName("img_height")
    val imgHeight: Long,

    @Expose
    @SerializedName("size")
    val size: Long,

    @Expose
    @SerializedName("img_extension")
    val imgExtension: String? = null,

    @Expose
    @SerializedName("is_active")
    val isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    val isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    val createdDate: String? = null,

    @Expose
    @SerializedName("created_by")
    val createdBy: String? = null,

    @Expose
    @SerializedName("created_log")
    val createdLog: String? = null,

    @Expose
    @SerializedName("modified_date")
    val modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    val modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    val modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    val mode: String? = null,

    @Expose
    @SerializedName("userName")
    val userName: String? = null,

    @Expose
    @SerializedName("modeaction")
    val modeaction: Long,

    @Expose
    @SerializedName("langid")
    val langid: Long,

    @Expose
    @SerializedName("langcode")
    val langcode: String? = null,

    @Expose
    @SerializedName("langname")
    val langname: String? = null,

    @Expose
    @SerializedName("lstlang")
    val lstlang: String? = null
): Parcelable
