package com.vn.akidokids.ui.base.interactor

interface MVPInteractor {
    fun isUserLoggedIn(): Boolean

    fun performUserLogout()
}