package com.vn.akidokids.di.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.vn.akidokids.data.database.AppDatabase
import com.vn.akidokids.data.database.repository.options.OptionsRepo
import com.vn.akidokids.data.database.repository.options.OptionsRepository
import com.vn.akidokids.data.database.repository.questions.QuestionRepo
import com.vn.akidokids.data.database.repository.questions.QuestionRepository
import com.vn.akidokids.data.network.ApiHeader
import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.AppApiHelper
import com.vn.akidokids.data.preferences.AppPreferenceHelper
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.di.PreferenceInfo
import com.vn.akidokids.util.AppConstants
import com.vn.akidokids.util.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.APP_DB_NAME).build()


    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = AppConstants.PREF_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun providePublicApiHeader()
            : ApiHeader.PublicApiHeader = ApiHeader.PublicApiHeader(contentType = "application/json")

    @Provides
    @Singleton
    internal fun provideProtectedApiHeader(preferenceHelper: PreferenceHelper)
            : ApiHeader.ProtectedApiHeader = ApiHeader.ProtectedApiHeader(contentType = "application/json",
        token = preferenceHelper.getAccessToken())

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

    @Provides
    @Singleton
    internal fun provideQuestionRepoHelper(appDatabase: AppDatabase): QuestionRepo = QuestionRepository(appDatabase.questionsDao())

    @Provides
    @Singleton
    internal fun provideOptionsRepoHelper(appDatabase: AppDatabase): OptionsRepo = OptionsRepository(appDatabase.optionsDao())

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()

}