package com.vn.akidokids.ui.Message

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.Message.interactor.MessageInteractor
import com.vn.akidokids.ui.Message.interactor.MessageInteractorImplement
import com.vn.akidokids.ui.Message.presenter.MessagePresenter
import com.vn.akidokids.ui.Message.presenter.MessagePresenterImplement
import com.vn.akidokids.ui.Message.view.MessageActivity
import com.vn.akidokids.ui.Message.view.MessageAdapter
import com.vn.akidokids.ui.Message.view.MessageView
import dagger.Module
import dagger.Provides

@Module
class MessageModule {

    @Provides
    fun provideInteractor(interactor: MessageInteractorImplement): MessageInteractor =
        interactor

    @Provides
    fun providePresenter(presenter: MessagePresenterImplement<MessageView, MessageInteractor>): MessagePresenter<MessageView, MessageInteractor> =
        presenter

    @Provides
    fun provideAdapter(delegate: MessageActivity): MessageAdapter = MessageAdapter(
        ArrayList(), delegate
    )

    @Provides
    fun provideLinearLayoutManage(context: MessageActivity) =
        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
}