package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HistoryTransactionRequest {
    data class DeleteTransaction(
        @Expose
        @SerializedName("user_id")
        var userId:String,

        @Expose
        @SerializedName("list_id")
        var listId:String
    )

    data class ServerListHistoryTransactionRequest internal constructor(
        @Expose
        @SerializedName("page")
        val page:Int,

        @Expose
        @SerializedName("pageSize")
        val pageSize:Int
    )
}