package com.vn.akidokids.ui.Class.ClassActivity.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.ClassInforResponse
import com.vn.akidokids.data.network.request.ClassRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ClassInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), ClassInteractor {

    override fun getInforClass(id: Long): Observable<ClassInforResponse> {
        return apiHelper.performServerInforClass(
            ClassRequest.ClassInforRequest(
                idClass = id,
                page = 1,
                size = 10
            )
        )
    }
}