package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CourseRequest {
    data class DetailCourseRequest(
        @Expose
        @SerializedName("idCourse")
        internal val idGroup:Long
    )

    data class QuantityStudentRequest(
        @Expose
        @SerializedName("id")
        internal val id:Long,

        @Expose
        @SerializedName("quantity")
        internal val quantity:String
    )

    data class ReferUserRequest(
        @Expose
        @SerializedName("idUser")
        internal val idUser:String
    )

    data class BookingCourse(
        @Expose
        @SerializedName("ProductId")
        internal val productId:Long,

        @Expose
        @SerializedName("Price")
        internal val price:String,

        @Expose
        @SerializedName("Quantity")
        internal val quantity:Long,

        @Expose
        @SerializedName("Rate")
        internal val rate:Double,

        @Expose
        @SerializedName("GivePoints")
        internal val givePoint:Long,

        @Expose
        @SerializedName("ProductType")
        internal val productType:String,

        @Expose
        @SerializedName("PaymentType")
        internal val paymentType:String,

        @Expose
        @SerializedName("IntroductionCode")
        internal val introductionCode:String
    )
}