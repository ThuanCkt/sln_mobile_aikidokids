package com.vn.akidokids.ui.Class.DetailGalley

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.R
import com.vn.akidokids.data.model.GalleyModel
import com.vn.akidokids.util.OnSwipeTouchListener
import com.vn.akidokids.util.ParameterAttribute
import com.vn.akidokids.util.extension.loadImageCenter
import kotlinx.android.synthetic.main.activity_detail_acticle.mToolbar
import kotlinx.android.synthetic.main.activity_detail_galley.*


class DetailGalleyActivity : AppCompatActivity(), DetailGalleyAdapter.DetailGalleyAdapterInterface {
    var galleyModel:GalleyModel? = null
    var index:Int = 0;
    lateinit var adapter:DetailGalleyAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_galley)
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        galleyModel = intent.getParcelableExtra(ParameterAttribute.GALLEY.nameKey)
        index = galleyModel?.index ?: 0

        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        this.rcGalley.layoutManager = linearLayoutManager
        adapter = DetailGalleyAdapter(galleyModel?.listImages ?: arrayListOf(), this)
        adapter.setPositionActive(this.index)
        rcGalley.itemAnimator = DefaultItemAnimator()
        rcGalley.adapter = adapter

        this.loadImageDetail()

        imgDetail.setOnTouchListener(object : OnSwipeTouchListener(this@DetailGalleyActivity) {

            override fun onSwipeTop() {
                super.onSwipeTop()
            }

            override fun onSwipeBottom() {
                super.onSwipeBottom()
            }

            override fun onSwipeLeft() {
                super.onSwipeLeft()
                if (index == (galleyModel?.listImages?.count() ?: 1) - 1) {
                    return
                }

                index ++
                itemGalleyClick(index)
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
                if (index == 0) {
                    return
                }
                index --
                itemGalleyClick(index)
            }
        })
    }

    fun loadImageDetail() {
        galleyModel?.listImages?.get(this.index)?.img?.let {
            imgDetail.loadImageCenter(it)
            imgDetail.scaleType = ImageView.ScaleType.FIT_CENTER
        }
    }

    override fun itemGalleyClick(position: Int) {
        this.index = position
        adapter.setPositionActive(this.index)
        this.loadImageDetail()
    }
}
