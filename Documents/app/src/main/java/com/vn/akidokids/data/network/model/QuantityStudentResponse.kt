package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class QuantityStudentResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: QuantityStudentResult?
)

data class QuantityStudentResult (
    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("course_id")
    val courseID: Long,

    @Expose
    @SerializedName("quantity")
    val quantity: Long,

    @Expose
    @SerializedName("give_points")
    val givePoints: Long,

    @Expose
    @SerializedName("give_points_text")
    val givePointsText: String? = null
)