package com.vn.akidokids.ui.main.ListCourse

import com.vn.akidokids.ui.main.ListCourse.interacror.ListCourseInteractor
import com.vn.akidokids.ui.main.ListCourse.interacror.ListCourseInteractorImplement
import com.vn.akidokids.ui.main.ListCourse.presenter.ListCoursePresenter
import com.vn.akidokids.ui.main.ListCourse.presenter.ListCoursePresenterImplement
import com.vn.akidokids.ui.main.ListCourse.view.ListCourseAdapter
import com.vn.akidokids.ui.main.ListCourse.view.ListCourseFragment
import com.vn.akidokids.ui.main.ListCourse.view.ListCourseView
import dagger.Module
import dagger.Provides

@Module
class ListCourseModule {
    @Provides
    internal fun provideInteractor(interactor: ListCourseInteractorImplement):ListCourseInteractor = interactor

    @Provides
    internal fun providePresenter(presenter: ListCoursePresenterImplement<ListCourseView, ListCourseInteractor>):ListCoursePresenter<ListCourseView, ListCourseInteractor> = presenter

    @Provides
    internal fun provideAdapter(fragment: ListCourseFragment):ListCourseAdapter = ListCourseAdapter(fragment, ArrayList())
}