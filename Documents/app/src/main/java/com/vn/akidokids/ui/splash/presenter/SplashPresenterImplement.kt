package com.vn.akidokids.ui.splash.presenter

import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.ui.splash.interactor.SplashInteractor
import com.vn.akidokids.ui.splash.view.SplashView
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashPresenterImplement<V : SplashView, I : SplashInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable):
    BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), SplashPresenter<V, I>  {

    override fun onAttach(view: V?) {
        super.onAttach(view)
        this.decideActivityToOpen()
    }

    override fun isUserLoggedIn(): Boolean {
        interactor?.let { return it.isUserLoggedIn() }
        return false
    }

    private fun decideActivityToOpen() = getView()?.let {
        it.openMainActivity()
    }

}