package com.vn.akidokids.ui.main.Category.presenter

import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.ui.main.Category.interactor.CategoryInteractor
import com.vn.akidokids.ui.main.Category.view.CategoryView
import com.vn.akidokids.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CategoryPresenterImplement<V : CategoryView, I : CategoryInteractor>
@Inject internal constructor(
    internal: I,
    provider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = internal,
        schedulerProvider = provider,
        compositeDisposable = disposable
    ), CategoryPresenter<V, I> {

    var page: Long = 1
    var sizePage: Int = 10
    var isLoadMore: Boolean = false
    var cid: Int = 1
    var isShowLoading: Boolean = true

    override fun onAttach(view: V?) {
        super.onAttach(view)
    }

    override fun setCID(cid: Int) {
        this.cid = cid
        this.getListArticle()
    }


    private fun getListArticle() {
        this.interactor?.let {
            if (this.isShowLoading) {
                this.getView()?.showProgress()
            }

            it.getListCategory(this.cid, page.toInt(), sizePage)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({ response ->
                    this.getView()?.hideProgress()
                    this.isShowLoading = true
                    if (response.result == null) {
                        return@subscribe
                    }

                    if (response.result?.isshow == true) {
                        this.getView()?.showMessageError(response.result?.msg ?: "")
                        if(this.page.equals(1)) {
                            this.getView()?.successGetListArticleCategory(
                                ArrayList(),
                                isLoadMore = false
                            )
                        }
                        return@subscribe
                    }

                    this.isLoadMore = false
                    this.page = response.result?.page?.currentpage ?: 1
                    this.getView()?.successGetListArticleCategory(
                        response.result?.data,
                        isLoadMore = response.result?.page?.isloadmore ?: false
                    )
                }, {
                    this.isShowLoading = true;
                    this.getView()?.hideProgress()
                })
        }

    }

    override fun loadMore() {
        if (this.isLoadMore) {
            return
        }

        this.isShowLoading = false

        this.isLoadMore = true
        this.page += 1

        this.getListArticle()
    }

    override fun pullToRefresh() {
        page = 1
        this.isShowLoading = false
        this.getListArticle()
    }
}