package com.vn.akidokids.ui.LoginAndRegister

import com.vn.akidokids.ui.LoginAndRegister.interactor.LoginAndRegisterInteractor
import com.vn.akidokids.ui.LoginAndRegister.interactor.LoginAndRegisterInteractorImplement
import com.vn.akidokids.ui.LoginAndRegister.presenter.LoginAndRegisterPresenter
import com.vn.akidokids.ui.LoginAndRegister.presenter.LoginAndRegisterPresenterImplement
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterView
import dagger.Module
import dagger.Provides

@Module
class LoginAndRegisterModule {

    @Provides
    fun provideInteractor(interactor: LoginAndRegisterInteractorImplement):LoginAndRegisterInteractor = interactor

    @Provides
    fun providePresenter(presenter:LoginAndRegisterPresenterImplement<LoginAndRegisterView, LoginAndRegisterInteractor>):LoginAndRegisterPresenter<LoginAndRegisterView, LoginAndRegisterInteractor> = presenter
}