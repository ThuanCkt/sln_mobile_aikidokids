package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPoint


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vn.akidokids.R
import com.vn.akidokids.util.EMPTY_STRING
import kotlinx.android.synthetic.main.fragment_method_point.*

/**
 * A simple [Fragment] subclass.
 */
class MethodPointFragment : Fragment() {

    companion object {
        fun newInstance():MethodPointFragment = MethodPointFragment()
    }

    private var note:String = EMPTY_STRING

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_method_point, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lbNote.text = this.note
    }

    fun setNote(note:String) {
        this.note = note
        lbNote.text = this.note
    }
}
