package com.vn.akidokids.ui.Class.ArticleClass.presenter

import com.vn.akidokids.data.network.model.ClassPostsData
import com.vn.akidokids.ui.Class.ArticleClass.interactor.ArticleClassInteractor
import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface ArticleClassPresenter<V:ArticleClassView, I:ArticleClassInteractor>:MVPPresenter<V, I> {
    fun getListArticle(idClass:Long)
    fun getListArticle():List<ClassPostsData>
    fun getIsLoadmore():Boolean
    fun loadMore(idClass:Long)
}