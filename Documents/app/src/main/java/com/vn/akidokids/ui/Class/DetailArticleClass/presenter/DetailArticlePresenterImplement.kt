package com.vn.akidokids.ui.Class.DetailArticleClass.presenter

import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.DetailArticleClassResult
import com.vn.akidokids.ui.Class.DetailArticleClass.interactor.DetailArticleClassInteractor
import com.vn.akidokids.ui.Class.DetailArticleClass.view.DetailArticleView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.*
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.toDateString
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject

class DetailArticlePresenterImplement<V : DetailArticleView, I : DetailArticleClassInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    dispose: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    compositeDisposable = dispose,
    schedulerProvider = schedulerProvider
), DetailArticlePresenter<V, I> {

    private var resultArticle:DetailArticleClassResult? = null

    override fun callApiDetailArticle(id: Long) {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.callApiArticle(id)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response.result?.isshow == true) {
                            getView()?.failCallApi(response.result.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.resultArticle = response.result

                        getView()?.successCallApi()
                    }, { err ->
                        getView()?.hideProgress()
                        getView()?.failCallApi(
                            AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet
                            )
                        )
                    })
            )

        }
    }

    override fun getDateCreate(): String {
        if (resultArticle?.createdDate == null) {
            return EMPTY_STRING
        }
        val date = resultArticle?.createdDate?.convertStringToDate(FORMAT_DATE_API)
        date?.let {
            val fullDate = it.toDateString(FORMAT_FULL_DATE)
            val daysArray = arrayOf(
                "Chủ nhật",
                "Thứ 2",
                "Thứ 3",
                "Thứ 4",
                "Thứ 5",
                "Thứ 6",
                "Thứ 7"
            )

            val calendar: Calendar = Calendar.getInstance()
            val day: Int = calendar.get(Calendar.DAY_OF_WEEK)

            return "${daysArray[day]} , $fullDate"
        }
        return date?.toDateString(FORMAT_DATE_APEARANCE) ?: EMPTY_STRING
    }

    override fun getHtml(): String {
        return resultArticle?.contentsHTML ?: EMPTY_STRING
    }

    override fun getTitle(): String {
        return resultArticle?.title ?: EMPTY_STRING
    }
}