package com.vn.akidokids.ui.main.MainActivity.presenter

import com.vn.akidokids.data.model.MenuItem
import com.vn.akidokids.data.network.model.Category
import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.ui.main.MainActivity.interactor.MainInteractor
import com.vn.akidokids.ui.main.MainActivity.view.MainView

interface MainPresenter<V: MainView, I: MainInteractor>:MVPPresenter<V, I> {
    fun onDrawerOptionLogoutClick()
    fun getCidOnPosition(position:Int):Int
    fun getGroupMenuIdOnPosition(position:Int):String
    var listCategory:List<Category>?

    fun getFullname():String
    fun getEmail():String
    fun getPoint():String
    fun getImageAvatar():String
    fun getListClass():List<MenuItem>

    fun getInformationUser()
}