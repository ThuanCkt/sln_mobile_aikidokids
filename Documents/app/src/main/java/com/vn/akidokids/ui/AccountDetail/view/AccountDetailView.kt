package com.vn.akidokids.ui.AccountDetail.view

import com.vn.akidokids.ui.base.view.MVPView

interface AccountDetailView:MVPView {
    fun getSuccesUserInfor()
    fun failCallUserInfor(message:String)
}