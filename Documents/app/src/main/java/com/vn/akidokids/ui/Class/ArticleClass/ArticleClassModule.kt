package com.vn.akidokids.ui.Course.LessonCourse

import com.vn.akidokids.ui.Class.ArticleClass.interactor.ArticleClassInteractor
import com.vn.akidokids.ui.Class.ArticleClass.interactor.ArticleClassInteractorImplement
import com.vn.akidokids.ui.Class.ArticleClass.presenter.ArticleClassPresenter
import com.vn.akidokids.ui.Class.ArticleClass.presenter.ArticleClassPresenterImplement
import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassAdapter
import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassFragment
import com.vn.akidokids.ui.Course.LessonCourse.view.ArticleClassView
import dagger.Module
import dagger.Provides

@Module
class ArticleClassModule {
    @Provides
    internal fun provideAdapter(fragment: ArticleClassFragment): ArticleClassAdapter = ArticleClassAdapter(fragment)

    @Provides
    internal fun provideInteractor(interactor: ArticleClassInteractorImplement): ArticleClassInteractor =
        interactor

    @Provides
    internal fun providePresenter(presenter: ArticleClassPresenterImplement<ArticleClassView, ArticleClassInteractor>): ArticleClassPresenter<ArticleClassView, ArticleClassInteractor> =
        presenter
}