package com.vn.akidokids.data.model

data class MenuItem(
    var name: String? = null,
    var index: Int,
    var iconId: Int? = null,
    var menuType: EnumMenuType,
    var id: Long? = null
)

enum class EnumMenuType {
    HISTORY_POINT,
    CLASS,
    ABOUT,
    TERM,
    MESSSAGE
}