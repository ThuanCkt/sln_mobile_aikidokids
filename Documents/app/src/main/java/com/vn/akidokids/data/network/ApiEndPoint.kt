package com.vn.akidokids.data.network

import com.vn.akidokids.BuildConfig

/**
 * Created by jyotidubey on 11/01/18.
 */
object ApiEndPoint {

    //Login and register
    val ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL + "/auth/login"
    val ENDPOINT_SERVER_REGISTER = BuildConfig.BASE_URL + "/auth/register"
    val ENDPOINT_SERVER_FORGOT_PASSWORD = BuildConfig.BASE_URL + "/auth/forgot-password"

    //Category and article
    val ENPOINT_SERVER_LIST_CATEGORY = BuildConfig.BASE_URL + "/akdmenu/list"
    val ENPOINT_SERVER_LIST_ARTICLER_FOLLOW_CATEGORY = BuildConfig.BASE_URL + "/akdarticle/get-list-article-by-category"
    val ENPOINT_SERVER_DETAIL_ARTIClE  = BuildConfig.BASE_URL + "/akdarticle/getbyid/{idArticle}"

    //Course
    var ENPOINT_SERVER_LIST_COURSE = BuildConfig.BASE_URL + "/akdcourse/list"
    var ENPOINT_SERVER_DETAIL_COURSE = BuildConfig.BASE_URL + "/akdcourse/getbyid/{idCourse}"
    var ENPOINT_SERVER_QUANTITY_STUDENT = BuildConfig.BASE_URL + "/akdcourse/get-point-by-course-and-quantity"
    var ENPOINT_SERVER_REFER_USER = BuildConfig.BASE_URL + "/user/get-user-by-code/{idUser}"
    var ENPOINT_SERVER_BOOKING_COURSE = BuildConfig.BASE_URL + "/akdorders/booking"
    var ENPOINT_SERVER_METHOD_PAYMENT = BuildConfig.BASE_URL + "/akdcourse/get-method-payment"

    //Class
    var ENPOINT_SERVER_INFOR_CLASS = BuildConfig.BASE_URL + "/akdclass/get-class-info/{idClass}"
    var ENPOINT_SERVER_ARTICLE_INFOR_CLASS = BuildConfig.BASE_URL + "/akdclass/class-list-post"
    var ENPOINT_SERVER_GALLEY_CLASS = BuildConfig.BASE_URL + "/akdclass/get-class-album"
    var ENPOINT_SERVER_DETAIL_ARTICLE_CLASS  = BuildConfig.BASE_URL + "/akdclass/get-post-by-id/{id}"

    //User
    var ENPOINT_SERVER_USER_INFOR = BuildConfig.BASE_URL + "/user/get-user-info"
    var ENPOINT_SERVER_CHANGE_PASSWORD = BuildConfig.BASE_URL + "/user/change-password"
    var ENPOINT_SERVER_USER_INFOR_BY_ID = BuildConfig.BASE_URL + "/user/getbyid/{UserId}"
    var ENPOINT_SERVER_PROVINCE = BuildConfig.BASE_URL + "/user/list-province"
    var ENPOINT_SERVER_CHANGE_INFOR = BuildConfig.BASE_URL + "/user/update-user"
    var ENPOINT_SERVER_CHANGE_AVATAR = BuildConfig.BASE_URL + "/user/change-avatar"

    //Term and about
    var ENPOINT_SERVER_TERM_AND_ABOUT = BuildConfig.BASE_URL + "/user/get-company-info"

    //Transaction
    var ENPOINT_SERVER_LIST_TRANSACTION_PAY = BuildConfig.BASE_URL + "/user/list-transaction-pay"
    var ENPOINT_SERVER_DELETE_TRANSACTION_PAY = BuildConfig.BASE_URL + "/user/delete-transaction-pay"

    //Message
    var ENPOINT_SERVER_LIST_MESSAGE = BuildConfig.BASE_URL + "/user/list-messages"
    var ENPOINT_SERVER_DELETE_MESSAGE = BuildConfig.BASE_URL + "/user/delete-messages"
}