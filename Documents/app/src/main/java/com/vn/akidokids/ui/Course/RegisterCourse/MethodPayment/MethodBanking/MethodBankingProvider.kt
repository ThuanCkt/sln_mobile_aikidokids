package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking

import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MethodBankingProvider {
    @ContributesAndroidInjector(modules = [(MethodBankingModule::class)])
    abstract fun provideBankingMethodFragment():MethodBankingFragment
}