package com.vn.akidokids.ui.splash.interactor

import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface SplashInteractor: MVPInteractor {
    fun seedQuestions(): Observable<Boolean>
    fun seedOptions(): Observable<Boolean>
}