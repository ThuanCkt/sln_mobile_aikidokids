package com.vn.akidokids.ui.ChangePassword.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.request.UserRequest
import com.vn.akidokids.ui.ChangePassword.interactor.ChangePasswordInteractor
import com.vn.akidokids.ui.ChangePassword.view.ChangePasswordView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.REX_VALID_PASSWORD
import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.util.Utils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ChangePasswordPresenterImplement<V : ChangePasswordView, I : ChangePasswordInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    compose: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = compose
), ChangePasswordPresenter<V, I> {

    override fun callApiChangePassword(
        oldPassword: String,
        newPassword: String,
        reNewPassword: String
    ) {
        when {
            oldPassword.isEmpty() -> getView()?.validChangePassword(AikidoKidsApplication.getAppResources().getString(R.string.password_old_not_empty))
            newPassword.isEmpty() -> getView()?.validChangePassword(AikidoKidsApplication.getAppResources().getString(R.string.password_new_not_empty))
            reNewPassword.isEmpty() -> getView()?.validChangePassword(AikidoKidsApplication.getAppResources().getString(R.string.re_password_new_not_empty))
            reNewPassword != newPassword -> getView()?.validChangePassword(AikidoKidsApplication.getAppResources().getString(R.string.password_new_not_same_re_password_new))
            !REX_VALID_PASSWORD.matches(newPassword) -> getView()?.validChangePassword(AikidoKidsApplication.getAppResources().getString(R.string.password_new_not_same))
            else -> {
                val data = UserRequest.ChangePasswordRequest(
                    Utils.getUserId(),
                    password = oldPassword,
                    passwordNew = newPassword,
                    passwordNewAgain = reNewPassword
                )
                interactor?.let {
                    getView()?.showProgress()
                    compositeDisposable.add(
                        it.callChangePassword(data = data).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                            { response ->
                                getView()?.hideProgress()
                                if (response.result == null) {
                                    getView()?.validChangePassword(
                                        AikidoKidsApplication.getAppResources().getString(
                                            R.string.please_check_connect_internet
                                        )
                                    )
                                    return@subscribe
                                }

                                if (response?.result?.isshow == true) {
                                    if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                        return@subscribe
                                    }
                                    getView()?.validChangePassword(response?.result?.msg ?: EMPTY_STRING)
                                    return@subscribe
                                }
                                
                                it.setToken(response.result?.token ?: EMPTY_STRING)
                                this.getView()?.changeSuccess()
                            },
                            { err ->
                                getView()?.hideProgress()
                                val err = it as? ANError
                                if(err?.errorCode == 401 || err?.errorCode == 403) {
                                    this.handleError("401")
                                    return@subscribe
                                }
                                err?.let {
                                    getView()?.validChangePassword(it.errorBody)
                                }

                            })
                    )
                }
            }
        }

    }
}