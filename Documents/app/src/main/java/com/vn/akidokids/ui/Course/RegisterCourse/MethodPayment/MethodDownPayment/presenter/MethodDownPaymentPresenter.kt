package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.presenter

import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.interactor.MethodDownPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface MethodDownPaymentPresenter<V:MethodDownPaymentView, I:MethodDownPaymentInteractor>:MVPPresenter<V, I> {
}