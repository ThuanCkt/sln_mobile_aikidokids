package com.vn.akidokids.ui.Course.LessonCourse

import com.vn.akidokids.ui.Course.LessonCourse.view.GalleyClassFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class GalleyClassProvide {
    @ContributesAndroidInjector(modules = [(GalleyClassModule::class)])
    abstract internal fun providerGalleyClassFragment(): GalleyClassFragment

}