package com.vn.akidokids.ui.Class.DetailGalley

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.AlbumClassData
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.extension.loadImage
import kotlinx.android.synthetic.main.galley_item_thumb.view.*

class DetailGalleyAdapter(private val listImages:List<AlbumClassData>,private val delegate:DetailGalleyAdapterInterface):RecyclerView.Adapter<DetailGalleyAdapter.DetailGalleyHolder>() {
    private var positionActive:Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailGalleyHolder {
        return DetailGalleyHolder( LayoutInflater.from(parent.context).inflate(
            R.layout.galley_item_thumb,
            parent,
            false
        ))
    }

    fun setPositionActive(positionActive:Int) {
        this.positionActive = positionActive
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listImages.count()
    }

    override fun onBindViewHolder(holder: DetailGalleyHolder, position: Int) {
        holder.bindData(position, positionActive)
    }

    inner class DetailGalleyHolder(view:View):RecyclerView.ViewHolder(view) {
        fun bindData(position:Int, positionActive:Int) {
            if (position == positionActive) {
                itemView.lnActive.visibility = View.VISIBLE
            } else {
                itemView.lnActive.visibility = View.GONE
            }

            itemView.imgThumbnail.loadImage(listImages[position].img ?: EMPTY_STRING)
            itemView.setOnClickListener {
                delegate.itemGalleyClick(position)
            }
        }
    }

    interface DetailGalleyAdapterInterface {
        fun itemGalleyClick(position: Int)
    }
}