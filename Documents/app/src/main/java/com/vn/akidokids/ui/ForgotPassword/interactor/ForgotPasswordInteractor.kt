package com.vn.akidokids.ui.ForgotPassword.interactor

import com.vn.akidokids.data.network.model.ForgotPasswordResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface ForgotPasswordInteractor:MVPInteractor {
    fun forgotPassword(email:String, note:String):Observable<ForgotPasswordResponse>
}