package com.vn.akidokids.di.component

import android.app.Application
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.di.builder.ActivityBuilder
import com.vn.akidokids.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (AppModule::class), (ActivityBuilder::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: AikidoKidsApplication)

}