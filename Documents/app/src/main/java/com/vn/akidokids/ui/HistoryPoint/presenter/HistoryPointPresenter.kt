package com.vn.akidokids.ui.HistoryPoint.presenter

import com.vn.akidokids.data.network.model.HistoryTransactionData
import com.vn.akidokids.ui.HistoryPoint.interactor.HistoryPointInteractor
import com.vn.akidokids.ui.HistoryPoint.view.HistoryPointView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface HistoryPointPresenter<V:HistoryPointView, I:HistoryPointInteractor>: MVPPresenter<V, I> {

    fun callListHistoryTransaction()
    fun getListHistoryPoint():List<HistoryTransactionData>
    fun deleteHistory(position:Int)
    fun loadMore()
    fun pullToRefresh()
}