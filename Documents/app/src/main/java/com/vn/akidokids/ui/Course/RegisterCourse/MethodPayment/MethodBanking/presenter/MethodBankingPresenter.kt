package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.presenter

import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.interactor.MethodBankingInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface MethodBankingPresenter<V:MethodBankingView, I:MethodBankingInteractor>: MVPPresenter<V, I> {
}