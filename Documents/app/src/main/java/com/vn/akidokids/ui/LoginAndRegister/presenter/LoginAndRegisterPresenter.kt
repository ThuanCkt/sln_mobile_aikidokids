package com.vn.akidokids.ui.LoginAndRegister.presenter

import com.vn.akidokids.ui.LoginAndRegister.interactor.LoginAndRegisterInteractor
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface LoginAndRegisterPresenter<V:LoginAndRegisterView, I:LoginAndRegisterInteractor>:MVPPresenter<V, I> {
}