package com.vn.akidokids.ui.Course.LessonCourse

import com.vn.akidokids.ui.Course.LessonCourse.view.LessonCourseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LessonCourseProvide {
    @ContributesAndroidInjector(modules = [(LessonCourseModule::class)])
    abstract internal fun provideLessonCourseFragment(): LessonCourseFragment

}