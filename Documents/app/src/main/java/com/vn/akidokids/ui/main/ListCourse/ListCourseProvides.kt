package com.vn.akidokids.ui.main.ListCourse

import com.vn.akidokids.ui.main.ListCourse.view.ListCourseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ListCourseProvides {

    @ContributesAndroidInjector(modules = [(ListCourseModule::class)])
    internal abstract fun provideListCourseFragment():ListCourseFragment
}