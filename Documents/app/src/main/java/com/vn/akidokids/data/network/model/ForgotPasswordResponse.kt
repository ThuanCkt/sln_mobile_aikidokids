package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ForgotPasswordResponse (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String? = null,

    @Expose
    @SerializedName("result")
    var result: ResultForgotPassword? = null
)

data class ResultForgotPassword (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("data")
    var data: Any? = null
)