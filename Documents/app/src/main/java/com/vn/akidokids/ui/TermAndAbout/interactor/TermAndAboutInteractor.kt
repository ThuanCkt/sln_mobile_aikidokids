package com.vn.akidokids.ui.TermAndAbout.interactor

import com.vn.akidokids.data.network.model.TermAndAboutResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface TermAndAboutInteractor:MVPInteractor {
    fun callTermAndAbout():Observable<TermAndAboutResponse>
}
