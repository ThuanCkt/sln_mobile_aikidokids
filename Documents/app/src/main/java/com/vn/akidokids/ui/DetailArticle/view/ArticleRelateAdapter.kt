package com.vn.akidokids.ui.DetailArticle.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ResultDetailArticle
import com.vn.akidokids.util.EMPTY_STRING
import kotlinx.android.synthetic.main.item_article_relate.view.*

class ArticleRelateAdapter internal constructor(private var listArticleRelate: List<ResultDetailArticle>, private val delegate:ArticleRelateAdapter.ArticleRelateAdapterInteface) :
    RecyclerView.Adapter<ArticleRelateAdapter.ArticleRelateHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleRelateHolder {
        return ArticleRelateHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_article_relate,
                parent,
                false
            )
        )
    }

    fun setListArticleRelate(listArticleRelate: List<ResultDetailArticle>) {
        this.listArticleRelate = listArticleRelate
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listArticleRelate.count()
    }

    override fun onBindViewHolder(holder: ArticleRelateHolder, position: Int) {
        holder.clear()
        holder.bindData(position, delegate = this.delegate)
    }

    inner class ArticleRelateHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
            itemView.lbArticleRelate.text = EMPTY_STRING
        }

        fun bindData(position: Int, delegate: ArticleRelateAdapterInteface) {
            listArticleRelate[position].title.let {
                itemView.lbArticleRelate.text = it
            }

            itemView.setOnClickListener {
                delegate.clickItem(listArticleRelate[position].id)
            }
        }
    }

    interface ArticleRelateAdapterInteface {
        fun clickItem(id: Long)
    }

}