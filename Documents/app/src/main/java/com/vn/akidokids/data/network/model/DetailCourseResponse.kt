package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetailCourseResponse (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String? = null,

    @Expose
    @SerializedName("result")
    var result: ResultDetailCourse? = null
) 

data class ResultDetailCourse (
    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("tutorialdetailsvm")
    var tutorialdetailsvm: Tutorialdetailsvm? = null,

    @Expose
    @SerializedName("lstakdteachervm")
    var lstakdteachervm: List<Lstakdteachervm>? = null,

    @Expose
    @SerializedName("lstquantitystudent")
    var lstquantitystudent: List<Lstquantitystudent>? = null,

    @Expose
    @SerializedName("id")
    var id: String? = null,

    @Expose
    @SerializedName("group_id")
    var groupID: Long,

    @Expose
    @SerializedName("group_id_text")
    var groupIDText: String? = null,

    @Expose
    @SerializedName("ccid")
    var ccid: Any? = null,

    @Expose
    @SerializedName("course_name")
    var courseName: String? = null,

    @Expose
    @SerializedName("course_slug")
    var courseSlug: String? = null,

    @Expose
    @SerializedName("name")
    var name: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("give_points")
    var givePoints: Long,

    @Expose
    @SerializedName("give_points_text")
    var givePointsText: String? = null,

    @Expose
    @SerializedName("price")
    var price: Long,

    @Expose
    @SerializedName("price_text")
    var priceText: String? = null,

    @Expose
    @SerializedName("price2")
    var price2: Long,

    @Expose
    @SerializedName("price2_text")
    var price2Text: String? = null,

    @Expose
    @SerializedName("price3")
    var price3: Long,

    @Expose
    @SerializedName("price3_text")
    var price3Text: String? = null,

    @Expose
    @SerializedName("rate")
    var rate: Double,

    @Expose
    @SerializedName("min_size")
    var minSize: Long,

    @Expose
    @SerializedName("actual_size")
    var actualSize: Long,

    @Expose
    @SerializedName("max_size")
    var maxSize: Long,

    @Expose
    @SerializedName("size_text")
    var sizeText: String? = null,

    @Expose
    @SerializedName("start_date_expected")
    var startDateExpected: String? = null,

    @Expose
    @SerializedName("end_date_expected")
    var endDateExpected: String? = null,

    @Expose
    @SerializedName("start_date_actual")
    var startDateActual: String? = null,

    @Expose
    @SerializedName("end_date_actual")
    var endDateActual: String? = null,

    @Expose
    @SerializedName("star")
    var star: Long,

    @Expose
    @SerializedName("count_view")
    var countView: Long,

    @Expose
    @SerializedName("count_view_text")
    var countViewText: String? = null,

    @Expose
    @SerializedName("img")
    var img: String? = null,

    @Expose
    @SerializedName("code_youtobe")
    var codeYoutobe: String? = null,

    @Expose
    @SerializedName("link_youtobe")
    var linkYoutobe: String? = null,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("description_html")
    var descriptionHTML: Any? = null,

    @Expose
    @SerializedName("contents")
    var contents: Any? = null,

    @Expose
    @SerializedName("contents_html")
    var contentsHTML: String? = null,

    @Expose
    @SerializedName("status")
    var status: String? = null,

    @Expose
    @SerializedName("status_text")
    var statusText: String? = null,

    @Expose
    @SerializedName("level")
    var level: String? = null,

    @Expose
    @SerializedName("level_text")
    var levelText: String,

    @Expose
    @SerializedName("orderby")
    var orderby: Long,

    @Expose
    @SerializedName("is_finish")
    var isFinish: Boolean,

    @Expose
    @SerializedName("is_finish_text")
    var isFinishText: String,

    @Expose
    @SerializedName("is_free")
    var isFree: Boolean,

    @Expose
    @SerializedName("is_free_text")
    var isFreeText: String? = null,

    @Expose
    @SerializedName("is_hot")
    var isHot: Boolean,

    @Expose
    @SerializedName("is_cool")
    var isCool: Boolean,

    @Expose
    @SerializedName("is_popular")
    var isPopular: Boolean,

    @Expose
    @SerializedName("created_date_text")
    var createdDateText: String,

    @Expose
    @SerializedName("modified_date_text")
    var modifiedDateText: String,

    @Expose
    @SerializedName("tutorialid")
    var tutorialid: Any? = null,

    @Expose
    @SerializedName("lstteacherid")
    var lstteacherid: List<Any?>,

    @Expose
    @SerializedName("lsttutorial")
    var lsttutorial: List<Any?>,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String,

    @Expose
    @SerializedName("created_by")
    var createdBy: String,

    @Expose
    @SerializedName("created_log")
    var createdLog: Any? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    var mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    var userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    var modeaction: Long,

    @Expose
    @SerializedName("langid")
    var langid: Long,

    @Expose
    @SerializedName("langcode")
    var langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    var langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    var lstlang: Any? = null
)

data class Lstakdteachervm (
    @Expose
    @SerializedName("id")
    var id: String? = null,

    @Expose
    @SerializedName("group_id")
    var groupID: Long,

    @Expose
    @SerializedName("name")
    var name: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("birthday")
    var birthday: String? = null,

    @Expose
    @SerializedName("birthday_text")
    var birthdayText: String? = null,

    @Expose
    @SerializedName("phone")
    var phone: String? = null,

    @Expose
    @SerializedName("job")
    var job: String? = null,

    @Expose
    @SerializedName("skill")
    var skill: String? = null,

    @Expose
    @SerializedName("experience")
    var experience: String? = null,

    @Expose
    @SerializedName("hobby")
    var hobby: String? = null,

    @Expose
    @SerializedName("img")
    var img: String,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("description_html")
    var descriptionHTML: Any? = null,

    @Expose
    @SerializedName("contents")
    var contents: Any? = null,

    @Expose
    @SerializedName("contents_html")
    var contentsHTML: String? = null,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String,

    @Expose
    @SerializedName("created_by")
    var createdBy: String? = null,

    @Expose
    @SerializedName("created_log")
    var createdLog: String? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    var mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    var userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    var modeaction: Long,

    @Expose
    @SerializedName("langid")
    var langid: Long,

    @Expose
    @SerializedName("langcode")
    var langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    var langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    var lstlang: Any? = null
)

data class Lstquantitystudent (
    var selected: Boolean,
    var value: String,
    var text: String
)

data class Tutorialdetailsvm (
    @Expose
    @SerializedName("id")
    var id: String? = null,

    @Expose
    @SerializedName("group_id")
    var groupID: Long,

    @Expose
    @SerializedName("gid")
    var gid: String? = null,

    @Expose
    @SerializedName("cate_name")
    var cateName: String? = null,

    @Expose
    @SerializedName("cate_slug")
    var cateSlug: String? = null,

    @Expose
    @SerializedName("name")
    var name: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("level")
    var level: String? = null,

    @Expose
    @SerializedName("level_text")
    var levelText: String? = null,

    @Expose
    @SerializedName("code_youtobe")
    var codeYoutobe: String? = null,

    @Expose
    @SerializedName("link_youtobe")
    var linkYoutobe: Any? = null,

    @Expose
    @SerializedName("img")
    var img: String? = null,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("description_html")
    var descriptionHTML: String? = null,

    @Expose
    @SerializedName("contents")
    var contents: String? = null,

    @Expose
    @SerializedName("contents_html")
    var contentsHTML: String? = null,

    @Expose
    @SerializedName("orderby")
    var orderby: Long,

    @Expose
    @SerializedName("is_free")
    var isFree: Boolean,

    @Expose
    @SerializedName("is_hot")
    var isHot: Boolean,

    @Expose
    @SerializedName("is_cool")
    var isCool: Boolean,

    @Expose
    @SerializedName("is_popular")
    var isPopular: Boolean,

    @Expose
    @SerializedName("lstakdlessonvm")
    var lstakdlessonvm: List<Lstakdlessonvm>? = null,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String,

    @Expose
    @SerializedName("created_by")
    var createdBy: String,

    @Expose
    @SerializedName("created_log")
    var createdLog: Any? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    var mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    var userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    var modeaction: Long,

    @Expose
    @SerializedName("langid")
    var langid: Long,

    @Expose
    @SerializedName("langcode")
    var langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    var langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    var lstlang: Any? = null
)

data class Lstakdlessonvm (
    @Expose
    @SerializedName("id")
    var id: String? = null,

    @Expose
    @SerializedName("group_id")
    var groupID: Long,

    @Expose
    @SerializedName("tutorial_id")
    var tutorialID: Long,

    @Expose
    @SerializedName("tutorial_name")
    var tutorialName: String? = null,

    @Expose
    @SerializedName("tutorial_slug")
    var tutorialSlug: String? = null,

    @Expose
    @SerializedName("name")
    var name: String? = null,

    @Expose
    @SerializedName("slug")
    var slug: String? = null,

    @Expose
    @SerializedName("code_youtobe")
    var codeYoutobe: String? = null,

    @Expose
    @SerializedName("link_youtobe")
    var linkYoutobe: String? = null,

    @Expose
    @SerializedName("post_type")
    var postType: String? = null,

    @Expose
    @SerializedName("post_type_text")
    var postTypeText: String? = null,

    @Expose
    @SerializedName("post_date")
    var postDate: String? = null,

    @Expose
    @SerializedName("img")
    var img: String? = null,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("description_html")
    var descriptionHTML: Any? = null,

    @Expose
    @SerializedName("contents")
    var contents: Any? = null,

    @Expose
    @SerializedName("contents_html")
    var contentsHTML: String? = null,

    @Expose
    @SerializedName("orderby")
    var orderby: Long,

    @Expose
    @SerializedName("is_important")
    var isImportant: Boolean,

    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    var isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    var createdDate: String? = null,

    @Expose
    @SerializedName("created_by")
    var createdBy: String? = null,

    @Expose
    @SerializedName("created_log")
    var createdLog: String? = null,

    @Expose
    @SerializedName("modified_date")
    var modifiedDate: String? = null,

    @Expose
    @SerializedName("modified_by")
    var modifiedBy: String? = null,

    @Expose
    @SerializedName("modified_log")
    var modifiedLog: String? = null,

    @Expose
    @SerializedName("mode")
    var mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    var userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    var modeaction: Long,

    @Expose
    @SerializedName("langid")
    var langid: Long,

    @Expose
    @SerializedName("langcode")
    var langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    var langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    var lstlang: Any? = null
)
