package com.vn.akidokids.ui.main.MainActivity.interactor

import com.vn.akidokids.data.network.model.CategoryResponse
import com.vn.akidokids.data.network.model.UserInforResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface MainInteractor:MVPInteractor {

    fun getListCategory():Observable<CategoryResponse>
    fun getUserInfor():Observable<UserInforResponse>
}