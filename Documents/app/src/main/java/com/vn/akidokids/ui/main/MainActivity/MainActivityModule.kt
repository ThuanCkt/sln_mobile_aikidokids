package com.vn.akidokids.ui.main.MainActivity

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.main.MainActivity.interactor.MainInteractor
import com.vn.akidokids.ui.main.MainActivity.interactor.MainInteractorImplement
import com.vn.akidokids.ui.main.MainActivity.presenter.MainPresenter
import com.vn.akidokids.ui.main.MainActivity.presenter.MainPresenterImplement
import com.vn.akidokids.ui.main.MainActivity.view.CategoryAdapter
import com.vn.akidokids.ui.main.MainActivity.view.MainActivity
import com.vn.akidokids.ui.main.MainActivity.view.MainView
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideInteractor(interactor: MainInteractorImplement): MainInteractor = interactor

    @Provides
    fun providePreventer(presenter: MainPresenterImplement<MainView, MainInteractor>): MainPresenter<MainView, MainInteractor> = presenter

    @Provides
    fun provideCategoryAdapter(activity: MainActivity): CategoryAdapter =
        CategoryAdapter(ArrayList(), delegate = activity)

    @Provides
    fun provideLinearLayoutManager(activity: MainActivity):LinearLayoutManager = LinearLayoutManager(activity)

    @Provides
    internal fun provideMenuAdapter(activity: MainActivity): MenuAdapter = MenuAdapter(ArrayList(), activity)
}