package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PostArticleClassResponse(
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: ClassPostsResult? = null
)

data class ClassPostsResult(
    @Expose
    @SerializedName("isshow")
    val isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    val msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    val msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    val msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    val msgdetail: String? = null,

    @Expose
    @SerializedName("page")
    val page: Page? = null,

    @Expose
    @SerializedName("data")
    val data: List<ClassPostsData>? = null
)

data class ClassPostsData(
    @Expose
    @SerializedName("id")
    val id: String,

    @Expose
    @SerializedName("group_id")
    val groupID: Long,

    @Expose
    @SerializedName("gid")
    val gid: Any? = null,

    @Expose
    @SerializedName("title")
    val title: String? = null,

    @Expose
    @SerializedName("slug")
    val slug: String? = null,

    @Expose
    @SerializedName("img")
    val img: String? = null,

    @Expose
    @SerializedName("img_thumbnail")
    val imgThumbnail: String? = null,

    @Expose
    @SerializedName("description")
    val description: String? = null,

    @Expose
    @SerializedName("description_html")
    val descriptionHTML: String? = null,

    @Expose
    @SerializedName("contents")
    val contents: String? = null,

    @Expose
    @SerializedName("contents_html")
    val contentsHTML: String? = null,

    @Expose
    @SerializedName("acc_signature")
    val accSignature: String? = null,


    @Expose
    @SerializedName("is_hot")
    val isHot: Boolean,

    @Expose
    @SerializedName("is_cool")
    val isCool: Boolean,

    @Expose
    @SerializedName("is_active")
    val isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    val isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    val createdDate: String? = null,

    @Expose
    @SerializedName("created_by")
    val createdBy: String? = null,

    @Expose
    @SerializedName("created_log")
    val createdLog: Any? = null,

    @Expose
    @SerializedName("modified_date")
    val modifiedDate: String,

    @Expose
    @SerializedName("modified_by")
    val modifiedBy: Any? = null,

    @Expose
    @SerializedName("modified_log")
    val modifiedLog: Any? = null,

    @Expose
    @SerializedName("mode")
    val mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    val userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    val modeaction: Long,

    @Expose
    @SerializedName("langid")
    val langid: Long,

    @Expose
    @SerializedName("langcode")
    val langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    val langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    val lstlang: Any? = null
)