package com.vn.akidokids.ui.ChangeInformation.view

import com.vn.akidokids.ui.base.view.MVPView

interface ChangeInformationView:MVPView {
    fun succesUserInfor()
    fun successProvince()
    fun successUpdateData()
    fun failCallUserInfor(message:String)

}