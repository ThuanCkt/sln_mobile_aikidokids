package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment

import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DownPaymentProvider {
    @ContributesAndroidInjector(modules = [MethodDownPaymentModule::class])
    abstract fun provideMethodDownPaymentFragment():MethodDownPaymentFragment
}