package com.vn.akidokids.util.extension

import java.text.SimpleDateFormat
import java.util.*

internal fun Date.toDateString(formatDate:String):String? {
    val formatter = SimpleDateFormat(formatDate)
    return formatter.format(this)
}