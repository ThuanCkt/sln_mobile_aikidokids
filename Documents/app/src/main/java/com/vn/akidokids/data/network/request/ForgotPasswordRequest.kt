package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ForgotPasswordRequest(
    @Expose
    @SerializedName("email")
    internal val email:String,

    @Expose
    @SerializedName("note")
    internal val note:String
)