package com.vn.akidokids.ui.register.view

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vn.akidokids.R
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterActivity
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.ui.register.interactor.RegisterInteractor
import com.vn.akidokids.ui.register.presenter.RegisterPresenterImplement
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.EventBus
import com.vn.akidokids.util.TypeEvenBus
import com.vn.akidokids.util.extension.hideKeyboard
import kotlinx.android.synthetic.main.fragment_register.*
import javax.inject.Inject

class RegisterFragment : BaseFragment(), RegisterView {
    @Inject
    lateinit var presenter:RegisterPresenterImplement<RegisterView, RegisterInteractor>

    var isShowPassword:Boolean = false
    var isShowRePassword:Boolean = false
    var isAgreeTerm:Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun setUp() {
        btnBack.setOnClickListener {
            btnBack.setOnClickListener {
                if(!LoginAndRegisterActivity.isLoginFist) {
                    activity?.finish()
                    return@setOnClickListener
                }

                this.goToLogin()
            }
        }

        lnLogin.setOnClickListener {
            this.goToLogin()
        }

        btnShowPassword.setOnClickListener {
            this.showPassOrHidePassword()
        }

        btnReShowPassword.setOnClickListener {
            this.showPassOrHideRePassword()
        }

        btnAgree.setOnClickListener {
            this.clickAgreeTerm()
        }

        btnRegister.setOnClickListener {
            this.hideKeyboard()
            this.presenter.registerAccount(username = txtUsername.text.toString(), fullname = txtFullName.text.toString(), password = txtPassword.text.toString(), rePassword = txtRePassword.text.toString(), isAgree = this.isAgreeTerm)
        }
    }

    fun goToLogin() {
        findNavController().popBackStack(R.id.navigation_login, false)
    }

    fun showPassOrHidePassword() {
        this.isShowPassword = !this.isShowPassword
        if (isShowPassword) {
            txtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            btnShowPassword.text = getString(R.string.hide)
            return
        }
        txtPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        btnShowPassword.text = getString(R.string.show)
    }

    fun showPassOrHideRePassword() {
        this.isShowRePassword = !this.isShowRePassword
        if (this.isShowRePassword) {
            txtRePassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            btnReShowPassword.text = getString(R.string.hide)
            return
        }
        txtRePassword.transformationMethod = PasswordTransformationMethod.getInstance()
        btnReShowPassword.text = getString(R.string.show)
    }

    fun clickAgreeTerm() {
        this.isAgreeTerm = !this.isAgreeTerm

        if (this.isAgreeTerm) {
            icAgree.setImageResource(R.drawable.ic_checked_checkbox)
            return
        }

        icAgree.setImageResource(R.drawable.icons_unchecked_checkbox)
    }

    //View delegate
    override fun messageError(msg: String) {
        this.showMessageToast(msg ?: EMPTY_STRING)    }

    override fun openMainActivity() {
        EventBus.post(TypeEvenBus.LoginSuccess("okla"))
        activity?.finish()
    }
}
