package com.vn.akidokids.ui.Class.DetailArticleClass.interactor

import com.vn.akidokids.data.network.model.DetailArticleClassResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface DetailArticleClassInteractor:MVPInteractor {
    fun callApiArticle(id:Long):Observable<DetailArticleClassResponse>
}