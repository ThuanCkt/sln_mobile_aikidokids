package com.vn.akidokids.ui.LoginAndRegister.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class LoginAndRegisterInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    LoginAndRegisterInteractor {

}