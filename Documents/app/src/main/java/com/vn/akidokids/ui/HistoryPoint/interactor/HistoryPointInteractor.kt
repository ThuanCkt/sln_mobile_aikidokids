package com.vn.akidokids.ui.HistoryPoint.interactor

import com.vn.akidokids.data.network.model.DeteleHistoryResponse
import com.vn.akidokids.data.network.model.HistoryTransactionResponse
import com.vn.akidokids.data.network.request.HistoryTransactionRequest
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface HistoryPointInteractor:MVPInteractor {
    fun getListHistory(page:Int, sizePage:Int):Observable<HistoryTransactionResponse>
    fun deleteHistoryPoint(data:HistoryTransactionRequest.DeleteTransaction):Observable<DeteleHistoryResponse>
}