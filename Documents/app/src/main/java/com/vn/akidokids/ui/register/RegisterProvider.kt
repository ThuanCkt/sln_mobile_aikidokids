package com.vn.akidokids.ui.register

import com.vn.akidokids.ui.register.view.RegisterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class RegisterProvider {
    @ContributesAndroidInjector(modules = [RegisterModule::class])
    internal abstract fun provideRegisterFragment() : RegisterFragment
}