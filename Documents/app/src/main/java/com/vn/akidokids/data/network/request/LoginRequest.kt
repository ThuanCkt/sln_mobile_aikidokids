package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by jyotidubey on 11/01/18.
 */
class LoginRequest {

    data class ServerLoginRequest internal constructor(
        @Expose
        @SerializedName("username") internal val username: String,
        @Expose
        @SerializedName("password") internal val password: String,
        @Expose
        @SerializedName("deviceID") internal val deviceID: String,
        @Expose
        @SerializedName("name") internal val name: String,
        @Expose
        @SerializedName("tokenAPNs") internal val tokenAPNs:String,
        @Expose
        @SerializedName("deviceName") internal val deviceName:String
    )
}