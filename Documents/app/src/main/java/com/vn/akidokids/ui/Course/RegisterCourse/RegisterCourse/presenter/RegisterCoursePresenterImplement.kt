package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.presenter

import android.util.Log
import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.Lstquantitystudent
import com.vn.akidokids.data.network.model.ResultDetailCourse
import com.vn.akidokids.data.network.request.CourseRequest
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor.RegisterCourseInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view.RegisterCourseView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.*
import com.vn.akidokids.util.extension.convertToStringComma
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RegisterCoursePresenterImplement<V : RegisterCourseView, I : RegisterCourseInteractor>
@Inject internal constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = interactor,
        schedulerProvider = schedulerProvider,
        compositeDisposable = disposable
    ), RegisterCoursePresenter<V, I> {

    private var resultCourse: ResultDetailCourse? = null
    private var quantityStudent: Long = 1.toLong()
    private var givePoints: Long = 0.toLong()
    private var idRefer: String = EMPTY_STRING

    override fun callDetaiCourse(id: Long) {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getDetailCourse(id = id).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.resultCourse = response.result
                        this.givePoints = response.result?.givePoints ?: 0.toLong()
                        this.quantityStudent = response.result?.lstquantitystudent?.filter {
                            it.selected == true
                        }?.first()?.value?.toLong() ?: 1

                        getView()?.successCallApi()
                    },
                    {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                    })
            )
        }
    }

    override fun getNameCourse(): String? {
        return resultCourse?.name
    }

    override fun getOriginalPrice(): String? {
        return resultCourse?.priceText
    }

    override fun getDiscountPrice(): String? {
        if (resultCourse?.price2 == 0.toLong()) {
            return EMPTY_STRING
        }
        return resultCourse?.price2Text
    }

    override fun getTotalPrice(): String? {
        return resultCourse?.price3Text
    }

    override fun getListQuanlityStudent(): List<String> {
        return resultCourse?.lstquantitystudent?.map { it ->
            it.text
        } ?: arrayListOf()
    }

    override fun getIndexDefaultSelectQuantity(): Int {
        return resultCourse?.lstquantitystudent?.withIndex()?.filter { (i, value) ->
            value.selected == true
        }?.first()?.index ?: 0
    }

    override fun getPointFollowQuantity(idCourse: Long, position: Int) {
        val quantityStudent: Lstquantitystudent = resultCourse?.lstquantitystudent?.get(position)
            ?: return
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getPointThroughtQuantityStudent(
                    idCourse = idCourse,
                    quantity = quantityStudent!!.value
                ).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }
                        this.quantityStudent = response.result?.quantity ?: 1.toLong()
                        this.givePoints = response.result?.givePoints
                        this.getView()?.setTextGivePoint("${response.result?.givePointsText}")
                        val priceNew =
                            (this.resultCourse?.price ?: 0.toLong()) * this.quantityStudent
                        val price2New =
                            (this.resultCourse?.price2 ?: 0.toLong()) * this.quantityStudent
                        val price3New =
                            (this.resultCourse?.price3 ?: 0.toLong()) * this.quantityStudent
                        this.getView()?.setOriginalText("${priceNew.convertToStringComma()} đ")
                        if (price2New != 0.toLong()) {
                            this.getView()
                                ?.setDiscountText("Tiết kiệm: ${price2New.convertToStringComma()} đ")
                        }
                        this.getView()?.setTotaltext("${price3New.convertToStringComma()} đ")
                    },
                    { err ->
                        Log.e("ahihi", err.toString())
                        getView()?.hideProgress()
                    })
            )
        }
    }

    override fun getReferUser(userId: String) {
        when {
            userId.isNullOrEmpty() -> getView()?.failCallApi(
                AikidoKidsApplication.getAppResources().getString(
                    R.string.id_user_refer_not_empty
                )
            )
            else -> {
                this.idRefer = EMPTY_STRING
                interactor?.let {
                    getView()?.showProgress()
                    compositeDisposable.add(
                        it.getReferUser(idUser = userId).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                            { response ->
                                getView()?.hideProgress()
                                if (response.result == null) {
                                    getView()?.failCallApi(
                                        AikidoKidsApplication.getAppResources().getString(
                                            R.string.please_check_connect_internet
                                        )
                                    )
                                    return@subscribe
                                }

                                if (response?.result?.isshow == true) {
                                    getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                                    return@subscribe
                                }

                                if (response.result?.fullname.isNullOrEmpty()) {
                                    return@subscribe
                                }


                                this.idRefer = userId
                                this.getView()
                                    ?.setTextUserRefer(response.result?.fullname ?: EMPTY_STRING)
                            },
                            { err ->
                                Log.e("ahihi", err.toString())
                                getView()?.hideProgress()
                            })
                    )
                }
            }
        }
    }

    override fun registerCourse(methodPayment: MethodPayment) {
        if (resultCourse == null) {
            return
        }

        val params = CourseRequest.BookingCourse(
            productId = this.resultCourse?.groupID ?: 0,
            price = "${this.resultCourse?.price ?: 0}",
            quantity = this.quantityStudent,
            productType = TypeProduct.COURSE.nameType,
            paymentType = "${methodPayment.mehthod}",
            rate = this.resultCourse?.rate ?: 0.0,
            introductionCode = this.idRefer,
            givePoint = this.givePoints
        )
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.registerCourse(params = params).compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallApi(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }
                            getView()?.failCallApi(response?.result?.msg ?: EMPTY_STRING)
                            if (methodPayment == MethodPayment.POINT) {
                                EventBus.post(TypeEvenBus.ReloadDataUse(response?.result?.msg ?: EMPTY_STRING))
                            }

                            return@subscribe
                        }
                    },
                    { err ->
                        getView()?.hideProgress()
                    })
            )
        }
    }
}