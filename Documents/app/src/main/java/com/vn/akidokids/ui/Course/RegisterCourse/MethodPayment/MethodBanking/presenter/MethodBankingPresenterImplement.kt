package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.presenter

import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.interactor.MethodBankingInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodBanking.view.MethodBankingView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MethodBankingPresenterImplement<V:MethodBankingView, I:MethodBankingInteractor>
@Inject internal constructor(interactor: I, providerSchedulerProvider: SchedulerProvider, disposable: CompositeDisposable):
BasePresenter<V, I>(interactor = interactor, schedulerProvider = providerSchedulerProvider, compositeDisposable = disposable), MethodBankingPresenter<V, I>{
}