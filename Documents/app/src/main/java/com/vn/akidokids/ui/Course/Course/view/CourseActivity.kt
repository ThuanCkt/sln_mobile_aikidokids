package com.vn.akidokids.ui.Course.Course.view

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.viewpager.widget.ViewPager
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.vn.akidokids.ui.Course.Course.CoursePagerAdapter
import com.vn.akidokids.ui.Course.Course.interactor.CourseInteractor
import com.vn.akidokids.ui.Course.Course.presenter.CoursePresenter
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view.RegisterCourseActivity
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.ParameterAttribute
import com.vn.akidokids.util.Utils
import com.vn.akidokids.util.extension.loadImage
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_cause.*
import javax.inject.Inject


class CourseActivity : BaseActivity(), CourseView, HasAndroidInjector, ViewPager.OnPageChangeListener{
    private val INFOR:Int = 0
    private val LESSON:Int = 1
    private val TEACHER:Int = 2

    private var indexSection:Int = INFOR
    private var idCourse:Long = 0
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var presenter: CoursePresenter<CourseView, CourseInteractor>

    internal lateinit var CoursePagerAdapter: CoursePagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        initInterface()
        idCourse = intent.getLongExtra(ParameterAttribute.ID.nameKey, 0)
        presenter.callDetaiCourse(idCourse)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    private fun setUpFeedPagerAdapter() {
        CoursePagerAdapter = CoursePagerAdapter(supportFragmentManager)
        CoursePagerAdapter.count = 3
        viewPage.adapter = CoursePagerAdapter
        viewPage.setOnPageChangeListener(this)
        viewPage.setPagingEnabled(false)
    }

    fun initInterface() {
        setContentView(com.vn.akidokids.R.layout.activity_cause)
        mToolbar.setNavigationIcon(com.vn.akidokids.R.drawable.ic_arrow_back_while);
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        tvPriceOld.paintFlags = tvPriceOld.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        lifecycle.addObserver(youtubePlayerView)

        setUpFeedPagerAdapter()

        lnInformation.setOnClickListener {
            this.indexSection = INFOR
            viewPage.setCurrentItem(this.INFOR)
            activeInfor()

        }

        lnLesson.setOnClickListener {
            this.indexSection = LESSON
            viewPage.setCurrentItem(this.LESSON)
            activeLesson()
        }

        lnTeacher.setOnClickListener {
            this.indexSection = TEACHER
            viewPage.setCurrentItem(this.TEACHER)
            activeTeacher()
        }

        btnRegister.setOnClickListener {
            if(!Utils.isRegisterCourse()) {
                this.showMessageToast("Bạn phải đăng nhập với tài khoản VIP")
                return@setOnClickListener
            }
            val intent = Intent(this, RegisterCourseActivity::class.java)
            intent.putExtra(ParameterAttribute.ID.nameKey, idCourse)
            this.startActivity(intent)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector;
    }


    private fun activeInfor() {
        lnInformation.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom_select)
        lnLesson.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom)
        lnTeacher.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom)
        CoursePagerAdapter.detailCourseFragment.loadDetailCourse(presenter.getContentHtml() ?: "")
    }

    private fun activeLesson() {
        lnInformation.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom)
        lnLesson.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom_select)
        lnTeacher.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom)
        CoursePagerAdapter.lessonCourseFragment.setListLesson(presenter.getListLesson(), presenter.getIsFree())
    }

    private fun activeTeacher() {
        lnInformation.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom)
        lnLesson.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom)
        lnTeacher.setBackgroundResource(com.vn.akidokids.R.drawable.divide_bottom_select)
        CoursePagerAdapter.teacherFragment.setListTeacher(presenter.getListTeacher())
    }

    override fun onPageScrollStateChanged(state: Int) {
        this.indexSection = viewPage.currentItem
        if (this.indexSection == INFOR) {
            activeInfor()
            return
        }

        if (this.indexSection == LESSON) {
            activeLesson()
            return
        }

        activeTeacher()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    fun setIsFreeAndDiscount(isFree: Boolean, rate:Double) {
        if (isFree) {
            btnRegister.visibility = View.GONE
            lnPriceOld.visibility = View.GONE
            return
        }

        if (rate == 0.0) {
            lnPriceOld.visibility = View.GONE
            return
        }
    }

    fun setUpViewYoutubeOrImageThumbnail(isYoutube:Boolean) {
        if (isYoutube) {
            imgThumbnail.visibility = View.INVISIBLE
            youtubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                    val videoId = presenter.getCodeYoutube() ?: "laWrqB_6a8k"
                    youTubePlayer.loadVideo(videoId, 0f)
                    youTubePlayer.pause()
                }
            })
            return;
        }
        imgThumbnail.loadImage(presenter.getCodeYoutube() ?: "")
    }

    //delegate view
    override fun failCallApi(message: String) {
        this.showMessageToast(message ?: EMPTY_STRING)
    }


    override fun successCallApi() {
        this.setIsFreeAndDiscount(isFree = presenter.getIsFree(), rate = presenter.getRate())
        this.setUpViewYoutubeOrImageThumbnail(presenter.isShowThumnailIsYouTube())
        lbTitle.text = presenter.getNameCourse()
        lbCourseId.text = presenter.getIDCourse()
        if(Utils.isRegisterCourse()) {
            tvPriceOld.text = presenter.getOrginalPrice()
            lbPrice.text = presenter.getPrice()
        } else {
            tvPriceOld.text = EMPTY_STRING
            lbPrice.text = EMPTY_STRING
        }
        lbSize.text = presenter.getSizeInClass()
        lbCountViews.text = presenter.getCountViews()
        CoursePagerAdapter.detailCourseFragment.loadDetailCourse(presenter.getContentHtml() ?: "")
        CoursePagerAdapter.lessonCourseFragment.setListLesson(presenter.getListLesson(), presenter.getIsFree())
        CoursePagerAdapter.teacherFragment.setListTeacher(presenter.getListTeacher())
    }



}
