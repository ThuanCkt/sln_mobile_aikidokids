package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view

import com.vn.akidokids.ui.base.view.MVPView

interface MethodPaymentView:MVPView {

    fun failCallApi(string: String)
    fun successCallApi()
}