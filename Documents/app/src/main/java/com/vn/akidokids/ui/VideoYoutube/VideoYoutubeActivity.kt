package com.vn.akidokids.ui.VideoYoutube

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.vn.akidokids.R
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.activity_cause.*

class VideoYoutubeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_youtube)
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_while);
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        val code = intent.getStringExtra(ParameterAttribute.CODE_YOUTUBE.nameKey)
        lifecycle.addObserver(youtubePlayerView)
        youtubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                youTubePlayer.loadVideo(code, 0f)
                youTubePlayer.pause()
            }
        })
    }
}
