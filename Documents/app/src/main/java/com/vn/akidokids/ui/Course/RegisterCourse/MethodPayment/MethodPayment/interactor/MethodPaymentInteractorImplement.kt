package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.MethodPaymentResponse
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class MethodPaymentInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) :
    BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    MethodPaymentInteractor {

    override fun getMethodPayment(): Observable<MethodPaymentResponse> {
        return apiHelper.performServerMethodPayment()
    }
}