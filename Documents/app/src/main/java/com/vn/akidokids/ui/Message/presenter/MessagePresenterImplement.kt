package com.vn.akidokids.ui.Message.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MessageTransactionData
import com.vn.akidokids.data.network.request.MessageTransactionRequest
import com.vn.akidokids.ui.Message.interactor.MessageInteractor
import com.vn.akidokids.ui.Message.view.MessageView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.util.Utils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MessagePresenterImplement<V : MessageView, I : MessageInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) : BasePresenter<V, I>(
    interactor = interactor,
    schedulerProvider = schedulerProvider,
    compositeDisposable = disposable
), MessagePresenter<V, I> {

    var listMessages: MutableList<MessageTransactionData> = arrayListOf()
    var page: Long = 1
    var sizePage: Int = 10
    var isLoadMore: Boolean = false
    var isShowLoading: Boolean = true

    override fun callListHistoryTransaction() {
        interactor?.let {
            if (this.isShowLoading) {
                this.getView()?.showProgress()
            }
            compositeDisposable.add(
                it.getListHistory(pageSize = this.sizePage, page = this.page.toInt()).compose(
                    schedulerProvider.ioToMainObservableScheduler()
                )
                    .subscribe({ response ->
                        this.isShowLoading = true
                        getView()?.hideProgress()
                        if (response.result == null) {
                            this.getView()?.failCallApi(
                                message = AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)) {
                                return@subscribe
                            }
                            this.getView()
                                ?.failCallApi(message = response.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.isLoadMore = false
                        this.page = response.result?.page?.currentpage ?: 1
                        response.result?.let { it ->
                            listMessages.addAll(it.data ?: ArrayList())
                            getView()?.shouldShowMore(listMessages.count() > 0)
                            this.getView()?.successCallApi(
                                isLoadMore = it.page?.isloadmore ?: false
                            )
                        }
                    }, { error ->
                        this.getView()?.hideProgress()
                        this.isShowLoading = true;
                        val err = it as? ANError
                        if (err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                        this.getView()?.failCallApi(
                            message = AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet
                            )
                        )
                    })
            )
        }
    }

    override fun deletHistory(position: Int) {
        if (position != -1 && position >= listMessages.count()) {
            return
        }
        var id = "delete_all"
        if (position == -1) {
            id = listMessages[position].msg_id.toString()
        }
        val data = MessageTransactionRequest.DeleteTransaction(
            userId = Utils.getUserId(),
            listId = id
        )
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.deleteMessage(data).compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            this.getView()?.failCallApi(
                                message = AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        this.getView()?.failCallApi(message = response.result?.msg ?: EMPTY_STRING)
                        if (response.isok == true) {
                            if (position == -1) {
                                this.listMessages.clear()
                            } else {
                                this.listMessages.removeAt(position)
                            }
                            this.getView()?.deleteSuccess(position)
                        }
                        return@subscribe

                    }, { error ->
                        this.getView()?.hideProgress()
                        this.getView()?.failCallApi(
                            message = AikidoKidsApplication.getAppResources().getString(
                                R.string.please_check_connect_internet
                            )
                        )
                    })
            )
        }
    }

    override fun getListMessage(): List<MessageTransactionData> {
        return listMessages ?: arrayListOf()
    }

    override fun loadMore() {
        if (this.isLoadMore) {
            return
        }

        this.isShowLoading = false

        this.isLoadMore = true
        this.page += 1

        this.callListHistoryTransaction()
    }

    override fun pullToRefresh() {
        page = 1
        this.isShowLoading = false
        this.listMessages.clear()
        this.callListHistoryTransaction()
    }
}