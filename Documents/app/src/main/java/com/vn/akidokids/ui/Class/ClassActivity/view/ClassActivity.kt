package com.vn.akidokids.ui.Class.ClassActivity.view

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.vn.akidokids.R
import com.vn.akidokids.ui.Class.ClassActivity.ClassPagerAdapter
import com.vn.akidokids.ui.Class.ClassActivity.interactor.ClassInteractor
import com.vn.akidokids.ui.Class.ClassActivity.presenter.ClassPresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.ParameterAttribute
import com.vn.akidokids.util.extension.loadImage
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_class.*
import javax.inject.Inject

class ClassActivity : BaseActivity(), ClassView, HasAndroidInjector,ViewPager.OnPageChangeListener{

    private val ARTICLE:Int = 0
    private val GALLEY:Int = 1

    private var indexSection:Int = ARTICLE

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    @Inject
    lateinit var memberClassAdapter: MemberClassAdapter

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var presenter:ClassPresenter<ClassView, ClassInteractor>

    internal lateinit var classPagerAdapter: ClassPagerAdapter

    private var idClass:Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_class)
        presenter.onAttach(this)
        this.initInterface()
        this.idClass = intent.getLongExtra(ParameterAttribute.ID.nameKey, 0)
        presenter.getInforClass(this.idClass)
        classPagerAdapter.setIdClass(this.idClass)
    }

    override fun onDestroy() {
        this.presenter.onDetach()
        super.onDestroy()
    }

    private fun initInterface() {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_while);
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rcMemberInClass.itemAnimator = DefaultItemAnimator()
        rcMemberInClass.layoutManager = layoutManager
        rcMemberInClass.adapter = memberClassAdapter

        classPagerAdapter = ClassPagerAdapter(supportFragmentManager)
        classPagerAdapter.count = 2
        viewPage.adapter = classPagerAdapter
        viewPage.setOnPageChangeListener(this)
        viewPage.setPagingEnabled(false)

        btnArticle.setOnClickListener {
            indexSection = ARTICLE
            activeArticle()
            viewPage.currentItem =this.indexSection
        }

        btnGalley.setOnClickListener {
            indexSection = GALLEY
            activeGalley()
            viewPage.currentItem =this.indexSection
        }
    }

    fun activeArticle() {
        btnArticle.setBackgroundResource(R.drawable.divide_bottom_select)
        btnGalley.setBackgroundResource(R.drawable.divide_bottom)
    }

    fun activeGalley() {
        btnArticle.setBackgroundResource(R.drawable.divide_bottom)
        btnGalley.setBackgroundResource(R.drawable.divide_bottom_select)
    }

    override fun onPageScrollStateChanged(state: Int) {
        this.indexSection = viewPage.currentItem
        if (this.indexSection == ARTICLE) {
            activeArticle()
            return
        }

        if (this.indexSection == GALLEY) {
            activeGalley()
            return
        }

        activeArticle()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector;
    }

    //delegate View
    override fun successCallApi() {
        lbNameClass.text = this.presenter.getNameClass()
        lbSizeClass.text = this.presenter.getSizeClass()
        imgThumbnailClass.loadImage(this.presenter.getThumnailClass())
        memberClassAdapter.setListMember(this.presenter.getListMember())
    }

    override fun failCallApi(message: String) {
        this.showMessageToast(message)
    }
}
