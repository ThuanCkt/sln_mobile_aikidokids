package com.vn.akidokids.ui.HistoryPoint.view

import android.actionsheet.demo.com.khoiron.actionsheetiosforandroid.ActionSheet
import android.actionsheet.demo.com.khoiron.actionsheetiosforandroid.Interface.ActionSheetCallBack
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.vn.akidokids.R
import com.vn.akidokids.ui.HistoryPoint.interactor.HistoryPointInteractor
import com.vn.akidokids.ui.HistoryPoint.presenter.HistoryPointPresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.PaginationScrollListener
import kotlinx.android.synthetic.main.activity_history_point.*
import javax.inject.Inject

class HistoryPointActivity : BaseActivity(), HistoryPointView,
    HistoryPoinAdapter.HistoryPoinAdapterInterface, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var adapter: HistoryPoinAdapter

    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager

    @Inject
    lateinit var presenter: HistoryPointPresenter<HistoryPointView, HistoryPointInteractor>

    var isLoading: Boolean = false
    var isPullToRefresh: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        setContentView(R.layout.activity_history_point)
        initInterface()
        presenter.callListHistoryTransaction()
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    fun initInterface() {
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        rcvHistoryPoint.adapter = adapter
        rcvHistoryPoint.itemAnimator = DefaultItemAnimator()
        rcvHistoryPoint.layoutManager = linearLayoutManager

        rcvHistoryPoint?.addOnScrollListener(object :
            PaginationScrollListener(this.linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return false
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                if (isPullToRefresh) {
                    return
                }

                isLoading = true
                presenter.loadMore()
            }
        })

        btnMore.setOnClickListener {
            val data by lazy { ArrayList<String>() }

            data.add(getString(R.string.delete_all))

            ActionSheet(this, data)
                .setTitle(getString(R.string.title_option_select))
                .setCancelTitle(getString(R.string.cancel))
                .setColorTitleCancel(resources.getColor(R.color.color_cancel_sheet))
                .setColorTitle(resources.getColor(R.color.color_title_sheet))
                .setColorData(resources.getColor(R.color.color_item_sheet))
                .create(object : ActionSheetCallBack {
                    override fun data(data: String, position: Int) {
                        if (getString(R.string.delete_all).equals(data)) {
                            presenter.deleteHistory(-1)
                        }
                    }
                })
        }

        swipeRefreshLayout.setColorSchemeColors(resources.getColor(R.color.color_primary))
        swipeRefreshLayout.setOnRefreshListener(this)
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    //HistoryPoinAdapterInterface
    override fun onClickItem(index: Int) {

        val data by lazy { ArrayList<String>() }

        data.add(getString(R.string.delete))

        ActionSheet(this, data)
            .setTitle(getString(R.string.title_option_select))
            .setCancelTitle(getString(R.string.cancel))
            .setColorTitleCancel(resources.getColor(R.color.color_cancel_sheet))
            .setColorTitle(resources.getColor(R.color.color_title_sheet))
            .setColorData(resources.getColor(R.color.color_item_sheet))
            .create(object : ActionSheetCallBack {
                override fun data(data: String, position: Int) {
                    if (getString(R.string.delete).equals(data)) {
                        presenter.deleteHistory(index)
                    }
                }
            })
    }

    //delegate view

    override fun successGetListHistoryData(
        isLoadMore: Boolean
    ) {
        adapter.setListHistoryAndIsLoading(
            listHistory = presenter.getListHistoryPoint(),
            isLoading = isLoadMore
        )
        isLoading = !isLoadMore

        if (isPullToRefresh) {
            isPullToRefresh = false
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun failCallApi(message: String) {
        if (isPullToRefresh) {
            isPullToRefresh = false
            swipeRefreshLayout.isRefreshing = false
        }
        this.showMessageToast(message)
    }

    override fun shouldShowMore(shouldShowMore: Boolean) {
        if (!shouldShowMore) {
            this.btnMore.visibility = View.GONE
            return
        }

        this.btnMore.visibility = View.VISIBLE
    }

    override fun onRefresh() {
        if (isPullToRefresh) {
            return
        }

        this.isPullToRefresh = true
        presenter.pullToRefresh()
    }

    override fun deleteSuccess() {
        this.adapter.setListHistory(presenter.getListHistoryPoint())
        if (presenter.getListHistoryPoint().count() <= 4) {
            presenter.pullToRefresh()
        }
    }
}
