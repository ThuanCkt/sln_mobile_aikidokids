package com.vn.akidokids.ui.ForgotPassword

import com.vn.akidokids.ui.ForgotPassword.interactor.ForgotPasswordInteractor
import com.vn.akidokids.ui.ForgotPassword.interactor.ForgotPasswordInteractorImplement
import com.vn.akidokids.ui.ForgotPassword.presenter.ForgotPasswordPresenter
import com.vn.akidokids.ui.ForgotPassword.presenter.ForgotPasswordPresenterImplement
import com.vn.akidokids.ui.ForgotPassword.view.ForgotPasswordView
import dagger.Module
import dagger.Provides

@Module
class ForgotPasswordModule {
    @Provides
    fun provideInteractor(interactor: ForgotPasswordInteractorImplement):ForgotPasswordInteractor = interactor

    @Provides
    fun providePresenter(presenter:ForgotPasswordPresenterImplement<ForgotPasswordView, ForgotPasswordInteractor>):ForgotPasswordPresenter<ForgotPasswordView, ForgotPasswordInteractor> = presenter
}