package com.vn.akidokids.util

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.os.Build
import android.util.TypedValue
import com.google.gson.Gson
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.BuildConfig
import com.vn.akidokids.data.network.ApiHeader
import com.vn.akidokids.data.network.model.UserEntity
import com.vn.akidokids.data.preferences.AppPreferenceHelper


class Utils {

    companion object {
        fun getNameDevice(): String {
            return Build.MANUFACTURER
        }

        fun getModelDevice(): String {
            return Build.MODEL
        }

        fun setClipboard(context: Context, text: String) {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
                clipboard.text = text
            } else {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
                val clip = android.content.ClipData.newPlainText("Copied Text", text)
                clipboard.setPrimaryClip(clip)
            }
        }

        fun getHeader(): ApiHeader.ProtectedApiHeader {
            if (!this.isLogin()) {
                return ApiHeader.ProtectedApiHeader(
                    contentType = "application/json",
                    token = BuildConfig.AUTHEN_DEFAULT
                )
            }
            val mPrefs: SharedPreferences =
                AikidoKidsApplication.context.getSharedPreferences(
                    AppConstants.PREF_NAME,
                    Context.MODE_PRIVATE
                )
            return ApiHeader.ProtectedApiHeader(
                contentType = "application/json",
                token = mPrefs.getString(AppPreferenceHelper.PREF_KEY_ACCESS_TOKEN, "").toString()
            )
        }

        fun getHeaderDefault(): ApiHeader.ProtectedApiHeader {
            return ApiHeader.ProtectedApiHeader(
                contentType = "application/json",
                token = BuildConfig.AUTHEN_DEFAULT
            )
        }

        fun getUserId(): String {
            val mPrefs: SharedPreferences =
                AikidoKidsApplication.context.getSharedPreferences(
                    AppConstants.PREF_NAME,
                    Context.MODE_PRIVATE
                )
            return mPrefs.getString(AppPreferenceHelper.PREF_KEY_CURRENT_USER_ID, "").toString()
        }

        fun getAvartar(): String {
            val mPrefs: SharedPreferences =
                AikidoKidsApplication.context.getSharedPreferences(
                    AppConstants.PREF_NAME,
                    Context.MODE_PRIVATE
                )
            return mPrefs.getString(AppPreferenceHelper.AVARTAR, "").toString()
        }

        fun isLogin(): Boolean {
            val mPrefs: SharedPreferences =
                AikidoKidsApplication.context.getSharedPreferences(
                    AppConstants.PREF_NAME,
                    Context.MODE_PRIVATE
                )
            return mPrefs.getInt(
                AppPreferenceHelper.PREF_KEY_USER_LOGGED_IN_MODE,
                AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type
            ) != AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type

        }

        fun isRegisterCourse():Boolean {
            val mPrefs: SharedPreferences =
                AikidoKidsApplication.context.getSharedPreferences(
                    AppConstants.PREF_NAME,
                    Context.MODE_PRIVATE
                )
            if(isLogin()) {
                val gson = Gson()
                val json = mPrefs.getString(AppPreferenceHelper.PREF_KEY_IS_USER, "")
                val user = gson.fromJson<Any>(json, UserEntity::class.java) as? UserEntity
                if(user == null) {
                    return false;
                }

                if(user.usergroup == "0") {
                    return false
                }

                return true
            }

            return false
        }

        fun dp2px(dp: Int): Int {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp.toFloat(),
                Resources.getSystem().getDisplayMetrics()
            ).toInt()
        }

        var token: String = ""
    }
}

enum class ParameterAttribute(val nameKey: String) {
    ID("id"),
    CODE_YOUTUBE("code_youtube"),
    GALLEY("galley"),
    INDEX("index"),
    IS_LOGIN("is_login")
}

enum class MethodPayment(val mehthod: Int) {
    TRANFER_BANK(1),
    DOWN_PAYMENT(2),
    POINT(3)
}

enum class TypeProduct(val nameType: String) {
    COURSE("0"),
    PRODUCT("1")
}