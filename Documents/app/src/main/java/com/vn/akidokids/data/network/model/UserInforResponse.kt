package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserInforResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String,

    @Expose
    @SerializedName("result")
    val result: ResultUserInfor? = null
)

data class ResultUserInfor (
    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    var msgdetail: String? = null,

    @Expose
    @SerializedName("lstclassvm")
    val lstclassvm: List<Lstclassvm>? = null,

    @Expose
    @SerializedName("id")
    val id: String,

    @Expose
    @SerializedName("email")
    val email: String,

    @Expose
    @SerializedName("emailconfirmed")
    val emailconfirmed: Long,

    @Expose
    @SerializedName("passwordhash")
    val passwordhash: String,

    @Expose
    @SerializedName("securitystamp")
    val securitystamp: String,

    @Expose
    @SerializedName("phonenumber")
    val phonenumber: String,
    @Expose
    @SerializedName("phonenumberconfirmed")
    val phonenumberconfirmed: Long,

    @Expose
    @SerializedName("twofactorenabled")
    val twofactorenabled: Long,

    @Expose
    @SerializedName("lockoutenddateutc")
    val lockoutenddateutc: String,

    @Expose
    @SerializedName("lockoutenabled")
    val lockoutenabled: Long,

    @Expose
    @SerializedName("accessfailedcount")
    val accessfailedcount: Long,

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("createddateutc")
    val createddateutc: String,

    @Expose
    @SerializedName("v")
    val passwordhash2: Any? = null,

    @Expose
    @SerializedName("fullname")
    val fullname: String,

    @Expose
    @SerializedName("displayname")
    val displayname: String,

    @Expose
    @SerializedName("displaynametext")
    val displaynametext: String,

    @Expose
    @SerializedName("avatar")
    val avatar: String,

    @Expose
    @SerializedName("otptype")
    val otptype: String,

    @Expose
    @SerializedName("birthday")
    val birthday: String,

    @Expose
    @SerializedName("birthdaytext")
    val birthdaytext: String,

    @Expose
    @SerializedName("sex")
    val sex: Long,

    @Expose
    @SerializedName("sextext")
    val sextext: String,

    @Expose
    @SerializedName("address")
    val address: String,

    @Expose
    @SerializedName("note")
    val note: Any? = null,

    @Expose
    @SerializedName("providerid")
    val providerid: Long,

    @Expose
    @SerializedName("code")
    val code: Any? = null,

    @Expose
    @SerializedName("usertype")
    val usertype: Any? = null,

    @Expose
    @SerializedName("point")
    val point: Long,

    @Expose
    @SerializedName("pointtext")
    val pointtext: String,

    @Expose
    @SerializedName("coin")
    val coin: Long,

    @Expose
    @SerializedName("usernumber")
    val usernumber: Long,

    @Expose
    @SerializedName("usercode")
    val usercode: String,

    @Expose
    @SerializedName("provinceid")
    val provinceid: Long,

    @Expose
    @SerializedName("provincename")
    val provincename: String,

    @Expose
    @SerializedName("deviceid")
    val deviceid: Any? = null,

    @Expose
    @SerializedName("devicename")
    val devicename: Any? = null,

    @Expose
    @SerializedName("tokenapns")
    val tokenapns: Any? = null,

    @Expose
    @SerializedName("usergrouptext")
    val usergrouptext: String? = null
)

data class Lstclassvm (

    @Expose
    @SerializedName("id")
    val id: String,

    @Expose
    @SerializedName("group_id")
    val groupID: Long,

    @Expose
    @SerializedName("group_id_text")
    val groupIDText: String,

    @Expose
    @SerializedName("course_id")
    val courseID: Long,

    @Expose
    @SerializedName("course_name")
    val courseName: Any? = null,

    @Expose
    @SerializedName("name")
    val name: String,

    @Expose
    @SerializedName("slug")
    val slug: String,

    @Expose
    @SerializedName("min_size")
    val minSize: Long,

    @Expose
    @SerializedName("actual_size")
    val actualSize: Long,

    @Expose
    @SerializedName("max_size")
    val maxSize: Long,

    @Expose
    @SerializedName("start_date_expected")
    val startDateExpected: String,

    @Expose
    @SerializedName("end_date_expected")
    val endDateExpected: String,

    @Expose
    @SerializedName("start_date_actual")
    val startDateActual: String,

    @Expose
    @SerializedName("end_date_actual")
    val endDateActual: String,

    @Expose
    @SerializedName("img")
    val img: String,

    @Expose
    @SerializedName("description")
    val description: String,

    @Expose
    @SerializedName("description_html")
    val descriptionHTML: Any? = null,

    @Expose
    @SerializedName("contents")
    val contents: Any? = null,

    @Expose
    @SerializedName("contents_html")
    val contentsHTML: String,

    @Expose
    @SerializedName("lstcourse")
    val lstcourse: List<Any?>,

    @Expose
    @SerializedName("is_active")
    val isActive: Boolean,

    @Expose
    @SerializedName("is_draft")
    val isDraft: Boolean,

    @Expose
    @SerializedName("created_date")
    val createdDate: String,

    @Expose
    @SerializedName("created_by")
    val createdBy: String,

    @Expose
    @SerializedName("created_log")
    val createdLog: Any? = null,

    @Expose
    @SerializedName("modified_date")
    val modifiedDate: String,

    @Expose
    @SerializedName("modified_by")
    val modifiedBy: String,

    @Expose
    @SerializedName("modified_log")
    val modifiedLog: String,

    @Expose
    @SerializedName("mode")
    val mode: Any? = null,

    @Expose
    @SerializedName("user_name")
    val userName: Any? = null,

    @Expose
    @SerializedName("modeaction")
    val modeaction: Long,

    @Expose
    @SerializedName("langid")
    val langid: Long,

    @Expose
    @SerializedName("langcode")
    val langcode: Any? = null,

    @Expose
    @SerializedName("langname")
    val langname: Any? = null,

    @Expose
    @SerializedName("lstlang")
    val lstlang: List<Any?>
)
