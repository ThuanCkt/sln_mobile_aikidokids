package com.vn.akidokids.ui.Course.DetaiCourse

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vn.akidokids.R
import kotlinx.android.synthetic.main.fragment_detail_cause.*


class DetailCourseFragment : Fragment() {
    var html:String = "";

    companion object {
        fun newInstance():DetailCourseFragment {
            return DetailCourseFragment()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       return inflater.inflate(R.layout.fragment_detail_cause, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.loadData(html, "text/html; charset=UTF-8", null)
    }

    fun loadDetailCourse(html:String) {
        if (this.html == html) {
            return;
        }
        this.html = html
        webView.loadData(html, "text/html; charset=UTF-8", null)
    }
}
