package com.vn.akidokids.ui.DetailArticle

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.DetailArticle.interactor.DetailArticleInteractor
import com.vn.akidokids.ui.DetailArticle.interactor.DetailArticleInteractorImplement
import com.vn.akidokids.ui.DetailArticle.presenter.DetailArticlePresenter
import com.vn.akidokids.ui.DetailArticle.presenter.DetailArticlePresenterImplement
import com.vn.akidokids.ui.DetailArticle.view.ArticleRelateAdapter
import com.vn.akidokids.ui.DetailArticle.view.DetailArticleActivity
import com.vn.akidokids.ui.DetailArticle.view.DetailArticleView
import dagger.Module
import dagger.Provides

@Module
class DetailArticleModule {

    @Provides
    fun provideInteractor(interactor: DetailArticleInteractorImplement): DetailArticleInteractor =
        interactor

    @Provides
    fun providePresenter(presenter: DetailArticlePresenterImplement<DetailArticleView, DetailArticleInteractor>): DetailArticlePresenter<DetailArticleView, DetailArticleInteractor> =
        presenter

    @Provides
    fun provideLineLearLayoutManager(activity: DetailArticleActivity): LinearLayoutManager =
        LinearLayoutManager(activity)

    @Provides
    internal fun provideAdapterArticleRelate(activity: DetailArticleActivity): ArticleRelateAdapter =
        ArticleRelateAdapter(arrayListOf(), delegate = activity)
}