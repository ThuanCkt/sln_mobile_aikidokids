package com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.presenter

import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.interactor.RegisterCourseInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.RegisterCourse.view.RegisterCourseView
import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.util.MethodPayment

interface RegisterCoursePresenter<V:RegisterCourseView, I:RegisterCourseInteractor>:MVPPresenter<V, I> {
    fun callDetaiCourse(id:Long)
    fun getNameCourse():String?
    fun getOriginalPrice():String?
    fun getDiscountPrice():String?
    fun getTotalPrice():String?
    fun getListQuanlityStudent():List<String>
    fun getIndexDefaultSelectQuantity():Int
    fun getPointFollowQuantity(idCourse:Long, position:Int)
    fun getReferUser(userId:String)
    fun registerCourse(methodPayment: MethodPayment)
}