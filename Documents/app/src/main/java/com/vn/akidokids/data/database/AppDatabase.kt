package com.vn.akidokids.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vn.akidokids.data.database.repository.options.Options
import com.vn.akidokids.data.database.repository.options.OptionsDao
import com.vn.akidokids.data.database.repository.questions.Question
import com.vn.akidokids.data.database.repository.questions.QuestionsDao

/**
 * Created by jyotidubey on 03/01/18.
 */
@Database(entities = [(Question::class), (Options::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun optionsDao(): OptionsDao

    abstract fun questionsDao(): QuestionsDao

}