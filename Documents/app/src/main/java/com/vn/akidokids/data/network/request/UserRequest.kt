package com.vn.akidokids.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserRequest {
    data class ChangePasswordRequest internal constructor(
        @Expose
        @SerializedName("UserId") internal val userId: String,
        @Expose
        @SerializedName("Password") internal val password: String,
        @Expose
        @SerializedName("PasswordNew") internal val passwordNew: String,
        @Expose
        @SerializedName("PasswordNewAgain") internal val passwordNewAgain: String
    )

    data class UserIdRequest internal constructor(
        @Expose
        @SerializedName("UserId") internal val userId: String)

    data class ChangeInforRequest internal constructor(
        @Expose
        @SerializedName("UserId") internal val userId: String,
        @Expose
        @SerializedName("FullName") internal val fullName: String,
        @Expose
        @SerializedName("DisplayName") internal val displayName: String,
        @Expose
        @SerializedName("PhoneNumber") internal val phoneNumber: String,
        @Expose
        @SerializedName("Birthday") internal val birthDay: String,
        @Expose
        @SerializedName("Sex") internal val sex: Int,
        @Expose
        @SerializedName("Address") internal val address: String,
        @Expose
        @SerializedName("ProvinceId") internal val provinceID: Long,
        @Expose
        @SerializedName("Email") internal val email: String
    )

    data class ChangeAvartarRequest internal constructor(
        @Expose
        @SerializedName("UserId") internal val userId: String,
        @Expose
        @SerializedName("AvatarBase64") internal val avatar: String

    )
}