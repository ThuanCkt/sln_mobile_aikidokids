package com.vn.akidokids.ui.Course.Course.presenter

import com.vn.akidokids.data.network.model.Lstakdlessonvm
import com.vn.akidokids.data.network.model.Lstakdteachervm
import com.vn.akidokids.ui.Course.Course.interactor.CourseInteractor
import com.vn.akidokids.ui.Course.Course.view.CourseView
import com.vn.akidokids.ui.base.presenter.MVPPresenter

interface CoursePresenter<V: CourseView, I: CourseInteractor>:MVPPresenter<V, I> {
    fun callDetaiCourse(id:Long)
    fun getNameCourse():String?
    fun getPrice():String?
    fun getOrginalPrice():String?
    fun getCountViews():String?
    fun getIDCourse():String?
    fun getContentHtml():String?
    fun isShowThumnailIsYouTube():Boolean
    fun getCodeYoutube():String?
    fun getLinkThumbnail():String?
    fun getSizeInClass():String
    fun getRate():Double
    fun getIsFree():Boolean
    fun getListLesson():List<Lstakdlessonvm>
    fun getListTeacher():List<Lstakdteachervm>
}