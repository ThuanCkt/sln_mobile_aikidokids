package com.vn.akidokids.ui.AccountDetail.view

import android.content.Intent
import android.os.Bundle
import com.vn.akidokids.R
import com.vn.akidokids.ui.AccountDetail.interactor.AccountDetailInteractor
import com.vn.akidokids.ui.AccountDetail.presenter.AccountDetailPresenter
import com.vn.akidokids.ui.ChangeInformation.view.ChangeInformationActivity
import com.vn.akidokids.ui.ChangePassword.view.ChangePassActivity
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.EventBus
import com.vn.akidokids.util.TypeEvenBus
import com.vn.akidokids.util.extension.loadImage
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_account_detail.*
import kotlinx.android.synthetic.main.activity_history_point.mToolbar
import javax.inject.Inject

class AccountDetailActivity : BaseActivity(), AccountDetailView {
    @Inject
    lateinit var presenter:AccountDetailPresenter<AccountDetailView, AccountDetailInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
        setContentView(R.layout.activity_account_detail)
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }
        presenter.callInformationUser()

        btnChangePassword.setOnClickListener {
            val intent = Intent(this, ChangePassActivity::class.java)
            this.startActivity(intent)
        }

        btnChange.setOnClickListener {
            presenter.clearLinkImage()
            val intent = Intent(this, ChangeInformationActivity::class.java)
            this.startActivity(intent)
        }

        EventBus.subscribe<TypeEvenBus.ReloadDataUse>()
            // if you want to receive the event on main thread
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                presenter.callInformationUser()
            })
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun getSuccesUserInfor() {
        presenter.getDataUserInformation()?.let {
            lblFullname.text = it.fullname
            lblNameDisplay.text = it.displayname
            lblUsername.text = it.username
            lblBirtday.text = it.birthdaytext
            lblAddress.text = it.address
            lblPhone.text = it.phonenumber
            lblEmail.text = it.email
            lblPoint.text = it.pointtext
            lblReferralCode.text = it.usercode
            lbGroup.text = it.usergrouptext
            imgAvatarUser.loadImage(it.avatar)
        }

    }

    override fun failCallUserInfor(message:String) {
        showMessageToast(message)
    }
}
