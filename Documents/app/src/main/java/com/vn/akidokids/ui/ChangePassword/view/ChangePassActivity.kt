package com.vn.akidokids.ui.ChangePassword.view

import android.os.Bundle
import com.vn.akidokids.R
import com.vn.akidokids.ui.ChangePassword.interactor.ChangePasswordInteractor
import com.vn.akidokids.ui.ChangePassword.presenter.ChangePasswordPresenter
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.util.extension.hideKeyboard
import kotlinx.android.synthetic.main.activity_change_pass.*
import kotlinx.android.synthetic.main.activity_history_point.mToolbar
import javax.inject.Inject

class ChangePassActivity : BaseActivity(),
    ChangePasswordView {

    @Inject
    lateinit var presenter: ChangePasswordPresenter<ChangePasswordView, ChangePasswordInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pass)
        presenter.onAttach(this)
        mToolbar.setNavigationOnClickListener {
            this.finish()
        }

        btnChange.setOnClickListener {
            this.hideKeyboard()
            this.presenter.callApiChangePassword(
                oldPassword = txtPasswordOld.text.toString(),
                newPassword = txtPasswordNew.text.toString(),
                reNewPassword = txtRePasswordNew.text.toString()
            )
        }

        btnCancel.setOnClickListener {
            this.hideKeyboard()
            txtPasswordOld.text.clear()
            txtPasswordNew.text.clear()
            txtRePasswordNew.text.clear()
        }
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun validChangePassword(string: String) {
        showMessageToast(string)
    }

    override fun changeSuccess() {
        showMessageToast(getString(R.string.change_pass_word_success))
    }
}
