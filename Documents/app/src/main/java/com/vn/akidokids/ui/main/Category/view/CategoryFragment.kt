package com.vn.akidokids.ui.main.Category.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.DataArticleCategory
import com.vn.akidokids.ui.DetailArticle.view.DetailArticleActivity
import com.vn.akidokids.ui.base.view.BaseFragment
import com.vn.akidokids.ui.main.Category.interactor.CategoryInteractor
import com.vn.akidokids.ui.main.Category.presenter.CategoryPresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.PaginationScrollListener
import com.vn.akidokids.util.ParameterAttribute
import kotlinx.android.synthetic.main.fragment_category.*
import java.util.*
import javax.inject.Inject

class CategoryFragment : BaseFragment(), CategoryView, SwipeRefreshLayout.OnRefreshListener, ArticleDelegate {

    @Inject
    lateinit var presenter: CategoryPresenter<CategoryView, CategoryInteractor>

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    @Inject
    internal lateinit var articleAdapter: ArticleAdapter

    var cid:Int = 0
    var isLoading: Boolean = false
    var isPullToRefresh:Boolean = false;

    companion object {

        fun newInstance(): CategoryFragment {
            return CategoryFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)

        presenter.setCID(cid = this.cid)
    }

    fun setCiid(cid:Int) {
        this.cid = cid
        this.isPullToRefresh = true
        presenter.setCID(cid = this.cid)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rcArticle.layoutManager = layoutManager
        rcArticle.itemAnimator = DefaultItemAnimator()
        rcArticle.adapter = articleAdapter
        swipeRefreshLayout.setColorSchemeColors(resources.getColor(R.color.color_primary))
        swipeRefreshLayout.setOnRefreshListener(this)

        rcArticle?.addOnScrollListener(object : PaginationScrollListener(this.layoutManager) {
            override fun isLastPage(): Boolean {
                return false
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                if (isPullToRefresh) {
                    return
                }

                isLoading = true
                presenter.loadMore()
            }
        })
    }

    override fun onRefresh() {
        if (isPullToRefresh) {
            return
        }

        isPullToRefresh = true
        this.presenter.pullToRefresh()

    }

    //delegate recycle view adapter

    override fun clickArticle(position: Long) {
        val intent = Intent(this.context, DetailArticleActivity::class.java)
        intent.putExtra(ParameterAttribute.ID.nameKey, position)
        this.activity?.startActivity(intent)
    }

    //delegate presenter

    override fun showMessageError(error: String) {
        this.showMessageToast(error ?: EMPTY_STRING)
    }

    override fun successGetListArticleCategory(listArticle: List<DataArticleCategory>?, isLoadMore:Boolean) {
        articleAdapter.addCatergoryToList(listArticle ?: ArrayList(), isLoadMore, isPullToRefresh)

        isLoading = !isLoadMore

        if(isPullToRefresh) {
            isPullToRefresh = false
            swipeRefreshLayout.isRefreshing = false
        }


    }
}
