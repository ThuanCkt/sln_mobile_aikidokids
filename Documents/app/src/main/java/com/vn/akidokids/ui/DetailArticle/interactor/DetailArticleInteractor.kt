package com.vn.akidokids.ui.DetailArticle.interactor

import com.vn.akidokids.data.network.model.DetailArticleResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface DetailArticleInteractor:MVPInteractor {
    fun getDetailArticle(id:Long): Observable<DetailArticleResponse>
}