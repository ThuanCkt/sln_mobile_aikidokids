package com.vn.akidokids.ui.Class.ArticleClass.interactor

import com.vn.akidokids.data.network.ApiHelper
import com.vn.akidokids.data.network.model.PostArticleClassResponse
import com.vn.akidokids.data.network.request.ClassRequest
import com.vn.akidokids.data.preferences.PreferenceHelper
import com.vn.akidokids.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ArticleClassInteractorImplement @Inject constructor(
    preferenceHelper: PreferenceHelper,
    apiHelper: ApiHelper
) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper),
    ArticleClassInteractor {

    override fun getListArticleClass(
        idClass: Long,
        page: Long,
        pageSize: Long
    ): Observable<PostArticleClassResponse> {
        return apiHelper.performServerArticleClass(
            ClassRequest.ArticleClassRequest(
                idClass = idClass,
                page = page,
                size = pageSize
            )
        )
    }
}