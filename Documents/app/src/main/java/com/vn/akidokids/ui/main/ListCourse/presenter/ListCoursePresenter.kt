package com.vn.akidokids.ui.main.ListCourse.presenter

import com.vn.akidokids.data.network.model.DataCourse
import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.ui.main.ListCourse.interacror.ListCourseInteractor
import com.vn.akidokids.ui.main.ListCourse.view.ListCourseView

interface  ListCoursePresenter<V: ListCourseView, I:ListCourseInteractor>:MVPPresenter<V, I> {

    fun callListCourse()
    fun pullToRefresh()
    fun getListCourse():List<DataCourse>
}