package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.MethodPayment
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.Utils
import kotlinx.android.synthetic.main.item_method_down_payment.view.*

class MethodDownPaymentAdapter:RecyclerView.Adapter<MethodDownPaymentAdapter.MethodDownPaymentHolder>() {
    private var downPayments: List<MethodPayment> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MethodDownPaymentHolder {
        return MethodDownPaymentHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_method_down_payment,parent, false))
    }

    fun setListDownPayment(downPayments: List<MethodPayment>) {
        this.downPayments = downPayments
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return downPayments.count();
    }

    override fun onBindViewHolder(holder: MethodDownPaymentHolder, position: Int) {
        holder.clear()
        holder.bindData(position)
    }

    inner class MethodDownPaymentHolder(view: View):RecyclerView.ViewHolder(view) {
        fun clear() {
            itemView.lbAddress.text = EMPTY_STRING
            itemView.lbPhone.text = EMPTY_STRING
            itemView.lbEmail.text = EMPTY_STRING
            itemView.lbNote.text = EMPTY_STRING
            itemView.lbName.text = EMPTY_STRING
        }

        fun bindData(position: Int) {
            itemView.lbAddress.text = downPayments[position].address
            itemView.lbPhone.text =  downPayments[position].phone
            itemView.lbEmail.text =  downPayments[position].email
            itemView.lbNote.text =  downPayments[position].note
            itemView.lbName.text =  "${position + 1}, ${downPayments[position].name}"
            itemView.setOnClickListener {
                Utils.setClipboard(AikidoKidsApplication.context,  downPayments[position].address ?: EMPTY_STRING)
                Toast.makeText(AikidoKidsApplication.context, AikidoKidsApplication.getAppResources().getString(R.string.copy_completed), Toast.LENGTH_LONG).show()
            }
        }
    }
}