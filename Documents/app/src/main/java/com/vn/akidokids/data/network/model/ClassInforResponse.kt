package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ClassInforResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: ResultClassInfor? = null
)

data class ResultClassInfor (
    @Expose
    @SerializedName("isshow")
    val isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    val msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    val msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    val msgtitle: String? = null,

    @Expose
    @SerializedName("msgdetail")
    val msgdetail: String? = null,

    @Expose
    @SerializedName("lstuser")
    val lstuser: List<MemberInClass>?= null,

    @Expose
    @SerializedName("name")
    val name: String? = null,

    @Expose
    @SerializedName("actual_size")
    val actualSize: Long,

    @Expose
    @SerializedName("max_size")
    val maxSize: Long,

    @Expose
    @SerializedName("img")
    val img: String? = null
)


data class MemberInClass (
    @Expose
    @SerializedName("id")
    val id: String? = null,

    @Expose
    @SerializedName("email")
    val email: String? = null,

    @Expose
    @SerializedName("emailconfirmed")
    val emailconfirmed: Long,

    @Expose
    @SerializedName("passwordhash")
    val passwordhash: String? = null,

    @Expose
    @SerializedName("securitystamp")
    val securitystamp: String? = null,

    @Expose
    @SerializedName("phonenumber")
    val phonenumber: String? = null,

    @Expose
    @SerializedName("phonenumberconfirmed")
    val phonenumberconfirmed: Long,

    @Expose
    @SerializedName("twofactorenabled")
    val twofactorenabled: Long,

    @Expose
    @SerializedName("lockoutenddateutc")
    val lockoutenddateutc: String,

    @Expose
    @SerializedName("lockoutenabled")
    val lockoutenabled: Long,

    @Expose
    @SerializedName("accessfailedcount")
    val accessfailedcount: Long,

    @Expose
    @SerializedName("username")
    val username: String? = null,

    @Expose
    @SerializedName("createddateutc")
    val createddateutc: String? = null,

    @Expose
    @SerializedName("passwordhash2")
    val passwordhash2: Any? = null,

    @Expose
    @SerializedName("fullname")
    val fullname: String? = null,

    @Expose
    @SerializedName("displayname")
    val displayname: String? = null,

    @Expose
    @SerializedName("displaynametext")
    val displaynametext: String,

    @Expose
    @SerializedName("avatar")
    val avatar: String? = null,

    @Expose
    @SerializedName("otptype")
    val otptype: String? = null,

    @Expose
    @SerializedName("birthday")
    val birthday: String? = null,

    @Expose
    @SerializedName("birthdaytext")
    val birthdaytext: String? = null,

    @Expose
    @SerializedName("sex")
    val sex: Long,

    @Expose
    @SerializedName("address")
    val address: String? = null,

    @Expose
    @SerializedName("note")
    val note: String? = null,

    @Expose
    @SerializedName("providerid")
    val providerid: Long,

    @Expose
    @SerializedName("code")
    val code: Any? = null,

    @Expose
    @SerializedName("usertype")
    val usertype: Any? = null,

    @Expose
    @SerializedName("point")
    val point: Long,

    @Expose
    @SerializedName("pointtext")
    val pointtext: String? = null
)


