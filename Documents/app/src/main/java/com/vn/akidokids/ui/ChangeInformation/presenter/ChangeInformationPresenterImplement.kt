package com.vn.akidokids.ui.ChangeInformation.presenter

import com.androidnetworking.error.ANError
import com.vn.akidokids.AikidoKidsApplication
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.ProvinceResult
import com.vn.akidokids.data.network.model.UserEntity
import com.vn.akidokids.data.network.request.UserRequest
import com.vn.akidokids.ui.ChangeInformation.interactor.ChangeInformationInteractor
import com.vn.akidokids.ui.ChangeInformation.view.ChangeInformationView
import com.vn.akidokids.ui.base.presenter.BasePresenter
import com.vn.akidokids.util.EMPTY_STRING
import com.vn.akidokids.util.FORMAT_DATE_API
import com.vn.akidokids.util.SchedulerProvider
import com.vn.akidokids.util.Utils
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.toDateString
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject

class ChangeInformationPresenterImplement<V : ChangeInformationView, I : ChangeInformationInteractor> @Inject constructor(
    interactor: I,
    schedulerProvider: SchedulerProvider,
    disposable: CompositeDisposable
) :
    BasePresenter<V, I>(
        interactor = interactor,
        schedulerProvider = schedulerProvider,
        compositeDisposable = disposable
    ), ChangeInformationPresenter<V, I> {

    private var userEntity: UserEntity? = null
    private var provinceResult: ProvinceResult? = null

    private var dateSelect: Date? = null
    private var provinceId: Long = 0

    override fun callInformationUser() {
        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.getUserInfor(UserRequest.UserIdRequest(Utils.getUserId())).compose(
                    schedulerProvider.ioToMainObservableScheduler()
                ).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {

                            getView()?.failCallUserInfor(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            if (handleError(response.result?.msgcode ?: EMPTY_STRING)){
                                return@subscribe
                            }
                            getView()?.failCallUserInfor(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.userEntity = response.result

                        this.userEntity?.birthday?.let {
                            this.dateSelect = it.convertStringToDate(FORMAT_DATE_API)
                        }
                        getView()?.succesUserInfor()
                        callProvince()
                    },
                    {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        if(err?.errorCode == 401 || err?.errorCode == 403) {
                            this.handleError("401")
                            return@subscribe
                        }
                        err?.let {
                            getView()?.failCallUserInfor(it.errorBody)
                        }
                    })
            )
        }
    }

    override fun callProvince() {
        interactor?.let {
            compositeDisposable.add(
                it.getProvince().compose(schedulerProvider.ioToMainObservableScheduler()).subscribe(
                    { response ->
                        getView()?.hideProgress()
                        if (response.result == null) {
                            getView()?.failCallUserInfor(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if (response?.result?.isshow == true) {
                            getView()?.failCallUserInfor(response?.result?.msg ?: EMPTY_STRING)
                            return@subscribe
                        }

                        this.provinceResult = response.result
                        getView()?.successProvince()
                    },
                    {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        err?.let {
                            getView()?.failCallUserInfor(it.errorBody)
                        }
                    })
            )
        }
    }

    override fun getUserData(): UserEntity? {
        return this.userEntity
    }

    override fun getListNameProvince(): List<String> {
        return provinceResult?.data?.map { it ->
            it.name ?: EMPTY_STRING
        } ?: arrayListOf()
    }

    override fun getIndexDefaultSelectProvince(): Int {
        val valueAndIndex = provinceResult?.data?.withIndex()?.filter { (i, value) ->
            value.id == userEntity?.provinceid
        }?.first()
        this.provinceId = valueAndIndex?.value?.id ?: 0

        return valueAndIndex?.index ?: 0
    }


    override fun setDateOfBirth(date: Date) {
        this.dateSelect = date
    }

    override fun provinceSelect(index: Int) {
        provinceResult?.data?.get(index)?.let {
            provinceId = it.id
        }
    }

    override fun updateData(
        fullName: String,
        nameDisplay: String,
        sex: Int,
        address: String,
        phone: String,
        email: String,
        avatar:String?
    ) {
        val dataRequest = UserRequest.ChangeInforRequest(
            userId = Utils.getUserId(),
            fullName = fullName,
            displayName = nameDisplay,
            phoneNumber = phone,
            birthDay = dateSelect?.toDateString("yyyyMMdd") ?: "20190101",
            address = address,
            sex = sex,
            provinceID = provinceId,
            email = email
        )

        interactor?.let {
            getView()?.showProgress()
            compositeDisposable.add(
                it.changeUserInfor(data = dataRequest).compose(
                    schedulerProvider.ioToMainObservableScheduler()
                ).subscribe(
                    { response ->

                        if (response.result == null) {
                            getView()?.hideProgress()
                            getView()?.failCallUserInfor(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if(!response?.result?.msg.isNullOrEmpty()) {
                            if (response?.result?.msg != "Cập nhật thành công.") {
                                getView()?.hideProgress()
                                getView()?.failCallUserInfor(response?.result?.msg ?: EMPTY_STRING)
                                return@subscribe
                            }
                        }

                        if (avatar.isNullOrEmpty()) {
                            getView()?.hideProgress()
                            this.getView()?.successUpdateData()
                            return@subscribe
                        }

                        updateAvatar(avatar)

                    },
                    {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        err?.let {
                            getView()?.failCallUserInfor(it.errorBody)
                        }
                    })
            )
        }
    }

    override fun setDataCamera(link: String) {
        interactor?.setDataCamera(link)
    }

    fun updateAvatar(avatar: String) {
        interactor?.let {
            compositeDisposable.add(
                it.changeAvatar(avatarBase64 = avatar).compose(
                    schedulerProvider.ioToMainObservableScheduler()
                ).subscribe(
                    { response ->

                        if (response.result == null) {
                            getView()?.hideProgress()
                            getView()?.failCallUserInfor(
                                AikidoKidsApplication.getAppResources().getString(
                                    R.string.please_check_connect_internet
                                )
                            )
                            return@subscribe
                        }

                        if(!response?.result?.msg.isNullOrEmpty()) {
                            if (response?.result?.msg != "Cập nhật thành công.") {
                                getView()?.hideProgress()
                                getView()?.failCallUserInfor(response?.result?.msg ?: EMPTY_STRING)
                                return@subscribe
                            }
                        }

                        this.getView()?.successUpdateData()
                        getView()?.hideProgress()
                    },
                    {
                        getView()?.hideProgress()
                        val err = it as? ANError
                        err?.let {
                            getView()?.failCallUserInfor(it.errorBody)
                        }
                    })
            )
        }
    }
}