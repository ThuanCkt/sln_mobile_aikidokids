package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor

import com.vn.akidokids.data.network.model.MethodPaymentResponse
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface MethodPaymentInteractor:MVPInteractor {

    fun getMethodPayment(): Observable<MethodPaymentResponse>
}