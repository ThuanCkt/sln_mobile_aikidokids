package com.vn.akidokids.ui.splash.view

import android.content.Intent
import android.os.Bundle
import com.vn.akidokids.R
import com.vn.akidokids.ui.LoginAndRegister.view.LoginAndRegisterActivity
import com.vn.akidokids.ui.base.view.BaseActivity
import com.vn.akidokids.ui.main.MainActivity.view.MainActivity
import com.vn.akidokids.ui.splash.interactor.SplashInteractor
import com.vn.akidokids.ui.splash.presenter.SplashPresenter
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashView {

    @Inject
    lateinit var presenter: SplashPresenter<SplashView, SplashInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }


    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun showSuccessToast() {
    }

    override fun showErrorToast() {
    }

    override fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun openLoginActivity() {
        val intent = Intent(this, LoginAndRegisterActivity::class.java)
        startActivity(intent)
        finish()
    }
}
