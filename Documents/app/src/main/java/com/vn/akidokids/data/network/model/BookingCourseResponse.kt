package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BookingCourseResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: BookingCourseResult? = null
)

data class BookingCourseResult (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("isshow")
    val isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    val msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    val msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    val msgtitle: String,

    @Expose
    @SerializedName("msgdetail")
    val msgdetail: String? = null,

    @Expose
    @SerializedName("data")
    val data: Any? = null
)