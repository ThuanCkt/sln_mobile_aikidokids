package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MessageTransactionResponse (
    @Expose
    @SerializedName("isok")
    val isok: Boolean,

    @Expose
    @SerializedName("statuscode")
    val statuscode: String? = null,

    @Expose
    @SerializedName("result")
    val result: MessageTransactionResult? = null
)

data class MessageTransactionResult (
    @Expose
    @SerializedName("isok")
    var isok: Boolean,

    @Expose
    @SerializedName("isshow")
    var isshow: Boolean,

    @Expose
    @SerializedName("msgcode")
    var msgcode: String? = null,

    @Expose
    @SerializedName("msg")
    var msg: String? = null,

    @Expose
    @SerializedName("msgtitle")
    var msgtitle: String? = null,

    @Expose
    @SerializedName("page")
    val page: Page? = null,

    @Expose
    @SerializedName("data")
    val data: List<MessageTransactionData>? = null
)

data class MessageTransactionData (
    @Expose
    @SerializedName("msg_id")
    val msg_id: Long? = null,

    @Expose
    @SerializedName("user_id")
    val user_id: String? = null,

    @Expose
    @SerializedName("msg_type")
    val msg_type: String? = null,

    @Expose
    @SerializedName("msg")
    val msg: String? = null,

    @Expose
    @SerializedName("msg_time")
    val msg_time: String? = null
)
