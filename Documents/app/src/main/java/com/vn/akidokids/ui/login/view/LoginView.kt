package com.vn.akidokids.ui.login.view

import com.vn.akidokids.ui.base.view.MVPView

interface LoginView:MVPView {

    fun showValidationMessage(message: String?)
    fun openMainActivity()
}