package com.vn.akidokids.ui.login.interactor

import com.vn.akidokids.data.network.model.LoginResponse
import com.vn.akidokids.data.network.model.ResultUser
import com.vn.akidokids.ui.base.interactor.MVPInteractor
import com.vn.akidokids.util.AppConstants
import io.reactivex.Observable

interface LoginInteractor:MVPInteractor {
    fun doServerLoginApiCall(email: String, password: String): Observable<LoginResponse>
    fun updateUserInSharedPref(userResult: ResultUser, loggedInMode: AppConstants.LoggedInMode, isRemember:Boolean)
}