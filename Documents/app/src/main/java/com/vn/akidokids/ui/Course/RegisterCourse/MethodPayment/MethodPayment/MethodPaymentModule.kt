package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment

import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor.MethodPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.interactor.MethodPaymentInteractorImplement
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.presenter.MethodPaymentPresenter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.presenter.MethodPaymentPresenterImplement
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentDialog
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodPayment.view.MethodPaymentView
import dagger.Module
import dagger.Provides

@Module
class MethodPaymentModule {

    @Provides
    internal fun provideInteractor(interactor: MethodPaymentInteractorImplement): MethodPaymentInteractor =
        interactor

    @Provides
    internal fun providePresenter(presenter: MethodPaymentPresenterImplement<MethodPaymentView, MethodPaymentInteractor>): MethodPaymentPresenter<MethodPaymentView, MethodPaymentInteractor> = presenter

    @Provides
    internal fun providePageAdapter(fragment:MethodPaymentDialog):MethodPagerAdapter = MethodPagerAdapter(fragment)
}