package com.vn.akidokids.ui.main.Category.presenter

import com.vn.akidokids.ui.base.presenter.MVPPresenter
import com.vn.akidokids.ui.main.Category.interactor.CategoryInteractor
import com.vn.akidokids.ui.main.Category.view.CategoryView

interface CategoryPresenter<V:CategoryView, I:CategoryInteractor>:MVPPresenter<V, I> {
    fun setCID(cid:Int)
    fun loadMore()
    fun pullToRefresh()
}