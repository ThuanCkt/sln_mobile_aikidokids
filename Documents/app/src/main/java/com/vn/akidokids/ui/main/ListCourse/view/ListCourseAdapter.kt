package com.vn.akidokids.ui.main.ListCourse.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vn.akidokids.R
import com.vn.akidokids.data.network.model.DataCourse
import com.vn.akidokids.util.FORMAT_DATE_APEARANCE
import com.vn.akidokids.util.FORMAT_DATE_API
import com.vn.akidokids.util.extension.convertStringToDate
import com.vn.akidokids.util.extension.loadImage
import com.vn.akidokids.util.extension.toDateString
import kotlinx.android.synthetic.main.item_cause.view.*
import java.util.*


class ListCourseAdapter(private val delegate: CourseDelegate, private var listCourse:List<DataCourse>): RecyclerView.Adapter<ListCourseAdapter.ListCourseHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListCourseHolder {
        return ListCourseHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_cause, parent,false))
    }

    override fun getItemCount(): Int {
        return listCourse.count()
    }

    fun setListCourse(list: List<DataCourse>) {
        this.listCourse = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ListCourseHolder, position: Int) {
        holder.let {
            it.setItemClickListener(index = position, delegate = delegate)
        }
        holder.bindingData(position)
    }

   inner class ListCourseHolder(view:View):RecyclerView.ViewHolder(view) {

        fun setItemClickListener(index:Int, delegate: CourseDelegate) {
            itemView.setOnClickListener {
                val course = listCourse.get(index = index)
                delegate.clickItemCourse(id = course.groupID)
            }
        }

       fun bindingData(position: Int) {
           val course = listCourse.get(index = position)
            val dateCreated:Date? = course.createdDate?.convertStringToDate(FORMAT_DATE_API)
            itemView.lbDate?.let {
                it.text = dateCreated?.toDateString(FORMAT_DATE_APEARANCE)
            }
            itemView.lbTitle?.let {
                it.text = course.name
            }
           itemView.lblDescription?.let {
               it.text = course.description
           }
           course.img?.let {
               itemView.imgThumbnail.loadImage(it)
           }
       }

    }
}
