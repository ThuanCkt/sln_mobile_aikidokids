package com.vn.akidokids.ui.AccountDetail

import com.vn.akidokids.ui.AccountDetail.interactor.AccountDetailInteractor
import com.vn.akidokids.ui.AccountDetail.interactor.AccountDetailInteractorImplement
import com.vn.akidokids.ui.AccountDetail.presenter.AccountDetailPresenter
import com.vn.akidokids.ui.AccountDetail.presenter.AccountDetailPresenterImplement
import com.vn.akidokids.ui.AccountDetail.view.AccountDetailView
import dagger.Module
import dagger.Provides

@Module
class AccountDetailModule {

    @Provides
    fun provideAccountDetailInteractor(interactor: AccountDetailInteractorImplement):AccountDetailInteractor = interactor

    @Provides
    fun providerPresenter(presenter:AccountDetailPresenterImplement<AccountDetailView, AccountDetailInteractor>):AccountDetailPresenter<AccountDetailView, AccountDetailInteractor> = presenter
}