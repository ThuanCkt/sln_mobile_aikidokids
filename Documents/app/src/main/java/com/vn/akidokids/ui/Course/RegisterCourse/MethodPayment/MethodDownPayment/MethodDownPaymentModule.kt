package com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment

import androidx.recyclerview.widget.LinearLayoutManager
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.interactor.MethodDownPaymentInteractor
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.interactor.MethodDownPaymentInteractorImplement
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.presenter.MethodDownPaymentPresenter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.presenter.MethodDownPaymentPresenterImplement
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentAdapter
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentFragment
import com.vn.akidokids.ui.Course.RegisterCourse.MethodPayment.MethodDownPayment.view.MethodDownPaymentView
import dagger.Module
import dagger.Provides

@Module
class MethodDownPaymentModule {

    @Provides
    internal fun provideInteractor(interactor: MethodDownPaymentInteractorImplement): MethodDownPaymentInteractor = interactor

    @Provides
    internal fun providePresenter(presenter: MethodDownPaymentPresenterImplement<MethodDownPaymentView, MethodDownPaymentInteractor>): MethodDownPaymentPresenter<MethodDownPaymentView, MethodDownPaymentInteractor> = presenter

    @Provides
    internal fun provideLayoutManager(fragment: MethodDownPaymentFragment) = LinearLayoutManager(fragment.context)

    @Provides
    internal fun provideAdapter(): MethodDownPaymentAdapter = MethodDownPaymentAdapter()
}