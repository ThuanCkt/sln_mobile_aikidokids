package com.vn.akidokids.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseUserId(
    @Expose
    @SerializedName("isok")
    var isok: Boolean? = false,

    @Expose
    @SerializedName("statuscode")
    var statuscode: String? = null,

    @Expose
    @SerializedName("result")
    var result: UserEntity? = null
)